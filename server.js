var dev = process.env.NODE_ENV == "development"

const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const logger = require('morgan')

require('./server/configs/db')

const app = new express()


app.use(cors())
app.use(logger('dev'))
app.set('port', process.env.PORT || 3000)


app.use(bodyParser.json({ limit: '100mb' }))
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))

app.use('/images/', express.static('assets/img'))
app.use('/userimages/', express.static('static/photos'))
app.use('/fonts/', express.static('assets/fonts'))
app.use('/styles/', express.static('assets/styles'))

app.use('/', express.static('assets/data'))

app.use('/api', require('./server/routes/index'))
// app.use('/api', index);
app.use('/departments', require('./server/routes/department'));
app.use('/employees', require('./server/routes/employees'));
app.use(require('./server/routes/admin'))
app.use('/dist/', (req, res, next) => {
	if (req.url === '/bundle_main.js') {
		req.url = req.url + '.gz'
	  	res.set('Content-Encoding', 'gzip')
	}
  	next()
}, express.static('dist'))


if (dev) {
	const front = process.env.FRONT_PART;
	const config = require('./front_' + front + '/webpack.' + front + '.dev')
	const compiler = webpack(config)
	app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }))
	app.use(webpackHotMiddleware(compiler))

	app.get('*', function (req, res) {
		var _url = req.url.split('/')
		// console.log(_url)
		if (_url[1]=='fa084902b382.html') {
			res.sendFile(__dirname + '/front_main/fa084902b382.html')
		} else if(_url[1]=='iitu') {
			res.sendFile(__dirname + '/front_main/it_practice.html')
		} else {
			res.sendFile(__dirname + '/front_main/dev_main_index.html')
		}

	})
} else {
	app.get('*', function (req, res) {
		var _url = req.url.split('/')
		// console.log(_url)

		if(_url[1]=='fa084902b382.html') {
			res.sendFile(__dirname + '/front_main/fa084902b382.html')
		} else if(_url[1]=='iitu') {
			res.sendFile(__dirname + '/front_main/it_practice.html')
		} else {
			res.sendFile(__dirname + '/front_main/prod_main_index.html')
		}		// else res.sendFile(__dirname + '/front_intro/prod_intro_index.html')
	})
}




app.listen(app.get('port'), function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info('Web server is listening on port %s. Go to http: //localhost:%s/ and use it. Thks!', app.get('port'), app.get('port'))
  }
})
