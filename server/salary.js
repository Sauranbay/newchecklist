// тут просчитывается зп

// эта функция создает дату например nd('23:35') - создаст сегодняшнюю дату с временем 23:35
function nd(a,b,c,d){
   var cur = new Date();
   if(!a){
     return cur;
   }
   if(typeof(a)=='string'){
    if(a.indexOf(':')>-1){
    var num = a.split(":");
    if(!b && (typeof(b*1)!="number")){
      var date = new Date(cur.getFullYear(), cur.getMonth(), cur.getDate(), num[0], num[1]);
    } else {
      var date = new Date(b.getFullYear(), b.getMonth(), b.getDate(), num[0], num[1]);
    }
    return date;
    }
  }
 if(a && (typeof(b*1)=="number") && c && d){
   var num = d.split(":");
   var date = new Date(a, b, c, num[0], num[1], '00');
   return date;
 }
}
// принимает дату и возвращает сегодняшняя эта дата или нет
function today(d){
  var tdy = new Date();
  if(d.getFullYear() == tdy.getFullYear()
  && d.getMonth() == tdy.getMonth()
  && d.getDate() == tdy.getDate()
  ) return true
  else return false
}
// возвращает дату больше или меньше текущей или указаной даты пример: Ago( 'days', -5) - возвращает дату которая была 5 дней назад
function Ago( what, count, initDate){
  var def = 'day';

  count = count ? count : ((typeof(initDate) == 'object') ? 1 : initDate*1 );
  initDate = initDate ? ((typeof(initDate) == 'object') ? initDate : new Date() ): new Date();
  what = what ? what : def;
  itr = Array.isArray(what) ? what.length : 1;
  for (var i = 0; i < itr; i++) {
    w = Array.isArray(what) ? what[i] : what;
    cnt = Array.isArray(count) ? ((count.length > i) ? count[i] : count[count.length-1]) : count;
    switch (w.toLowerCase()) {
      case 'years':
      var d =  (initDate.getFullYear()+cnt)
      initDate.setYear(d);
      break;
      case 'months':
      var d =  (initDate.getMonth()+cnt);
      initDate.setMonth(d);
      break;
      case 'days':
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
      break;
      case 'hours':
      var d =  (initDate.getHours()+cnt) ;
      initDate.setHours(d);
      break;
      case 'minutes':
      var d =  (initDate.getMinutes()+cnt);
      initDate.setMinutes(d);
      break;
      case 'seconds':
      var d =  (initDate.getSeconds()+cnt) ;
      initDate.setSeconds(d);
      break;
      default:
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
    }
  }
  return initDate;
}
// сравнивает две даты  по минутам, секундам, дням и тд
function CompareDate(Fdate, Sdate, returnBy, abs) {
  if(typeof(Sdate) == 'string') {
    var spl = Sdate.split(':');
    var Sdate = new Date(Fdate);
    Sdate.setHours(spl[0]);
    Sdate.setMinutes(spl[1]);
    // console.log(Sdate.toString(), Fdate.toString())
  }
  delta = Fdate.getTime() - Sdate.getTime();
  returnBy = returnBy ? returnBy : 'ms';
  var res = 0;
  switch (returnBy.toLowerCase()) {
    case 'year':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7 / 52);
      break;
    case 'week':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7);
      break;
    case 'day':
      res = Math.floor(delta / 1000 / 60 / 60 / 24);
      break;
    case 'hour':
      res = Math.floor(delta / 1000 / 60 / 60);
      break;
    case 'min':
      res = Math.floor(delta / 1000 / 60);
      break;
    case 'sec':
      res = Math.floor(delta / 1000 );
      break;
    case 'ms':
      res = Math.floor(delta);
      break;
    default:
      res = Math.floor( delta / 1000 / 60 );
  }
  return abs ? Math.abs(res) : res;
}
// возвращает количество дней в данном месяце
function daysInMonth(year, month){
  var date = new Date(year*1,month*1);
  date.setDate(32);
  return 32 - date.getDate();
}
// возвращает информацию по этому месяцу (выходные, будни, субботы). пример getMonthInfo(2017,9, [1,2]) - убирает из будней первое и второе число и приписывает их выходным
function getMonthInfo(year, month, prazd){
  var budni = [];
  var subbot = [];
  var vih = [];
  var pr = prazd || []; //<-------------------------
  var dim = daysInMonth(year*1,month*1);
  var nd = new Date(year*1,month*1,1);
  var d = 0;
  for (var i = 1; i < dim+1; i++) {
    nd.setDate(i);
    d = nd.getDay();
    if(d == 6 && pr.indexOf(i) == -1){
      subbot.push(i);//<-------------------------
    }else {
      if(d != 0 && pr.indexOf(i) == -1){
        budni.push(i);//<-------------------------
      }else{
        vih.push(i);//<-------------------------
      }
    }
  }
  console.log(vih);
  return {
    days: dim,
    budni: budni,
    subbot: subbot,
    vih: vih
  }
  // sd.setDate(sd.getDate())
}

// возврщает сколько минут должен отработать человек в этом месяе
function calcHrs(year, month, fixT, fixST, prazd){
  // calcHrs(2017(год),0(месяц),7(часы в будни),5(часы в субботу),[](праздники))
  var mon = getMonthInfo(year,month,prazd);
  var mins = mon.budni.length*570;
  // (mon.budni.length*(fixT*1))+(mon.subbot.length*(fixST*1)); //  <------с учетом выходных
  // console.log(mon.budni.length,'+',fixT,';',mon.subbot.length,'+',(fixST*1),';',mon, mins);
  return mins;
}
// принимает 24.12.2017 и создает из этого дату
function rawToDate(raw) {
  var d = raw.split(/\.|\-|\ /);
  if((d[0]*1 > 31) || (d[0]*1 < 1) || (d[1]*1 > 12) || (d[1]*1 < 1)){
    return new Date(d[2],d[1]-1,d[0]);
  }else{
    return false;
  }
}
// принимает дату чекина и дату чекаута, разбивает их по времени и возвращает
/*
console.log(splitByTime(nd("09:54"),nd("18:00"),['08:30','18:00']));

{ late: true, // опаздал ли
  lateMin: 84, // на сколько опаздал
  result:  //  разделенные даты
   [ [ 2017-10-17T03:54:00.000Z, 2017-10-17T12:00:00.000Z ],
     [ 2017-10-17T12:00:00.000Z, 2017-10-17T12:00:00.000Z ] ],
  splInd: 2, // тип разбиения
  splited: true }
*/
function splitByTime (fdate, sdate, time) {
  console.log(fdate.toString(),sdate.toString());
  fdate = new Date(fdate);
  sdate = new Date(sdate);
  function chk (f,s,h,m){
    // var td = new Date(f);
    // td.setHours(h*1);
    // td.setMinutes(m*1);
    var td = nd(new Date(f).getFullYear(),new Date(f).getMonth(),new Date(f).getDate() ,h+":"+m);
    // console.log(td.toString(),'======= < TD ['+h,':',m+']');
    // console.log('F['+fdate.toString()+']','S['+sdate.toString()+']','['+h,':',m+']',CompareDate(td, s),'< s compare f >',CompareDate(td, f));
    if(CompareDate(td, s)<=0){
      if(CompareDate(td, f)>=0) {return true;} else {return false}
    } else {
      if(CompareDate(td, f)<=0) {return true;} else {return false}
    }
  }
  var t1 = time[0].split(':');
  var t2 = time[1].split(':');
  var fa = [];
  var c = false
  var splInd = 0;
  var subDate = new Date(sdate)
  var late = true;
  var lateMin = 0;
  // console.log('CHK  t1 is ',chk(fdate,sdate,t1[0],t1[1]));
  if(chk(fdate,sdate,t1[0],t1[1])){
    console.log('1');
    splInd=1;
    late = false;
    subDate.setHours(t1[0]*1);
    subDate.setMinutes(t1[1]*1);
    fa.push([fdate, subDate])
    // console.log('pushed 1 >', fa[0])
    if(chk( subDate, sdate, t2[0], t2[1] )){
      // console.log('3');
      splInd=3;
      var subDate2 = new Date(subDate);
      subDate2.setHours(t2[0]*1);
      subDate2.setMinutes(t2[1]*1);
      fa.push([subDate, subDate2]);
      // console.log('pushed !2 >', fa[0])//fa[1][0].getHours(),fa[1][1].getHours())
      fa.push([subDate2, sdate]);
      // console.log('pushed !3 >', fa[0])//fa[2][0].getHours(),fa[2][1].getHours())
    } else {
      // console.log('4');
      fa.push([subDate, sdate]);
      // console.log('pushed 2 >', fa[0])//[1][0].getHours(),fa[1][1].getHours())

    }
    c = true;
  } else {
    // console.log('else t2' , chk(fdate,sdate,t2[0],t2[1]));
    if(chk(fdate,sdate,t2[0],t2[1])){
      // console.log('2');
      splInd=2;
      subDate.setHours(t2[0]*1);
      subDate.setMinutes(t2[1]*1);
      fa.push([fdate, subDate])
      fa.push([subDate, sdate]);
      var lat = CompareDate(fdate, time[0], 'min');
      // console.log('lat?! >',lat);
      if(lat > 0 ) {
        lateMin = Math.abs(lat);
      }
      c = true;
    }
  }
  if(!c){
    fa.push([fdate,sdate])
    var lat = CompareDate(fdate, time[0], 'min');
    if(lat > 0 ) {
      if(CompareDate(sdate, time[1], 'min') < 0){
        // console.log('passed!')
        lateMin = Math.abs(lat);
      }
    }
  }
  // console.log(fa[0][0].getHours(),fa[0][1].getHours())
  return {
    late,
    lateMin,
    result: fa,
    splInd,
    splited: c
  };
}
// дата ли это
function isDate(dt) {
  if(typeof(dt) === 'object'){
    return (typeof(dt.getMonth) === 'function')
  } else {
    return false;
  }
}
//  принимает чекины чекауты за текущий день и взвращает на сколько опаздал, на сколько раньше пришел на сколько перераотал сколько работал.
function splitDates(dArr, sTimes, returnBy) {
   var type = returnBy ? returnBy : 'minutes';
   console.log('===========Splitting ',new Date(dArr[0][0]).toString(),new Date(dArr[0][1]).toString(),'===============');
   //  var st = 0;
   var result = {
     before:0,
     lateMin:0,
     between:0,
     early:0,
     after:0
   };
   var tween = [];
   var n = false;
   var subd = [];
   var late = true;
   var pre = [];
   dArr.forEach(function (check, i) {
     if(!isDate(check[1])) {
      //  console.log(check)
       if(today(check[0])){
         check[1] = nd();
        //  console.log('setting now');
       } else {
        //  console.log('setting 23:59');
         check[1] = nd('23:59',check[0]);
       }
     }
     var spl = splitByTime(check[0], check[1], sTimes);
     console.log('spl - > ',spl, sTimes)
     if(spl.splited) {
       switch (spl.splInd) {
         case 1:
        //  console.log('1 > ',spl)
         if(spl.late == false) late == false;
         result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
         result.between += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
           break;
           case 2:
          //  console.log('2 > ',spl)
           if(spl.late){
             pre.push(spl.lateMin)
           }
           result.after += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
           result.between += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;

             break;
             case 3:
            //  console.log(spl)
            //  console.log('3 > ',spl)
             result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
             result.between += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
             result.after += CompareDate(spl.result[2][0],spl.result[2][1],'min',true)*1;
               break;
         default:
        //  console.log('default act')
       }
     } else {
       // если первая дата раньше чем вторая , то результат: "+"
       console.log('not splited',sTimes);
       var cmp = CompareDate(spl.result[0][0],sTimes[0])*1;
       var cmp2 =CompareDate(spl.result[0][1],sTimes[1])*1;
       if(spl.late){
         pre.push(spl.lateMin)
       }
      //  console.log('must be  > 0 ---',cmp,'must be < 0 --',cmp2)
       if(cmp > 0 && cmp2 < 0){
        //  console.log('><')
         result.between += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
       } else {
        //  console.log('<>')
         if(cmp > 0 && cmp2 > 0) result.after += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;//e
         if(cmp < 0 && cmp2 < 0) result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;//b
       }
     }
   })
   if(late){
     pre.reverse().forEach(function (l) {
       if(l>0){
         result.lateMin = l;
       }
     })
   }
  //  tween.forEach(function (t) {
  //    console.log(t[0].getHours()+':'+t[0].getMinutes(),t[1].getHours()+':'+t[1].getMinutes())
  //  })
  //  console.log(result)
   return result;
}
// принимает чекины за день и возвращает суммарное количество минут
function getSummary(checkArray, end) {
  var sum = 0;
  var result = {}
  checkArray.forEach(function (ch, i) {
    var chIn  = ch[0];
    var chOut = ch[1];
    if(!isDate(ch[1])) {
      if(today(ch[0])){
        chOut = nd();
      } else {
        chOut = nd((end || '23:59'),ch[0]);
      }
    }
    sum += CompareDate(new Date(chIn), new Date(chOut), 'min', true);
  })
  // result.
  return sum;
}

function summaryLateMins(checkArray, inTime){
  var result = 0;
  checkArray.forEach( (ch)=> {
    result+= getLateMins(ch, inTime);
  })
  return result;
  // var sum = 0;
  // // var result = {}
  // checkArray.forEach(function (ch, i) {
  //   var chIn  = ch[0];
  //   var chOut ;
  //
  //     if(today(ch[0])){
  //       chOut = nd('9:00');
  //     }
  //   sum += CompareDate(new Date(chOut), new Date(chIn), 'min', true);
  // })
  // // result.
  // return sum;
}
function getLateMins(ch, inTime){
  var sum = 0;
    var chIn  = ch[0];
    var mustIn = nd(chIn.getFullYear(), chIn.getMonth(), chIn.getDate(), inTime);
    var late = CompareDate(chIn, mustIn, 'min');
    if( late < 0)
    late = 0;
  // result.
  return late;
}


//                                %                  7     5    [12,7] [ check: [[date], [date]] ]
// console.log(calcHrs(2017,07,8,0,[]))
// соединить праздники личные и общие. ОБЯЗАТЕЛЬНО СЧИТАТЬ ПРАЗДНИКИ КАК ВОСКРЕСЕНЬЕ! А В ВОСКРЕСЕНЬЕ СЧИТАТЬ ПЕРЕРАБОТКИ!!
function SalaryInfo(totalSalary, bp, year, month, fixT, fixST, prazd, days, inTime, outTime, SinTime, SoutTime ,type){
  var bonus = ((totalSalary / 100) * bp);
  var fix = totalSalary - bonus; 
  var monthInfo = getMonthInfo(year, month, prazd);
  var monthMins = calcHrs(year, month, fixT, fixST, prazd);
  fixT = fixT / 60;
  fixST = fixST / 60;
  var monthHrs = monthMins / 60;
  var salPerHour = fix / monthHrs;
  // var salPerMin = fix / monthMins;
  var salPerMin = totalSalary / monthMins;
  var salMonth = (monthInfo.subbot.length*salPerHour*fixST) + (monthInfo.budni.length*salPerHour*fixT)  //<---------------я остановился тут
  var byDay = [];

  if(days) {
    var overMins = 0;
    var totalMonthMin = 0;
    var tempdata = 0;
    var totalLateMin = 0;

    // console.log(totalSalary, bp, year, month, fixT, fixST, prazd, monthInfo, monthMins, monthHrs, salPerMin, salMonth);
    days.forEach(function (day,i) {
      // console.log('============Это '+day.day+' день==============', day)
      if(day.check && day.year == year && day.month == month ){
        var thisDay = nd(day.year,day.month,day.day,'10:00').getDay();
        type = type || 'fixed';
        switch (type.toString().toLowerCase()) {
          case 'free':
            var t = getSummary(day.check);
            var late = summaryLateMins(day.check);          
            totalMonthMin += t;
            totalLateMin += late;            
            byDay.push({
              workedMins: t,
              day: day.day,
              month: day.month,
              year: day.year
            })
            break;
          default:
            var t = getSummary(day.check);
            var late = summaryLateMins(day.check, '9:00');            
            if(t>60) t-=60;
            totalMonthMin += t;
            totalLateMin += late;            
            byDay.push({
              workedMins: t,
              day: day.day,
              month: day.month,
              year: day.year
            })
            break;
        }
      }
    })
  }
  var nedorabotal = 0;
  if(totalMonthMin > monthMins){
    var o = totalMonthMin - monthMins;
    overMins += o;
    totalMonthMin -= o;
  }else if(totalMonthMin < monthMins) {
    nedorabotal = monthMins - totalMonthMin;
  }
  var hrsInMonth = totalMonthMin / 60;
  var trueFix  = totalMonthMin * salPerMin;
  var totalMinsOver = overMins;//(( totalMonthMin - monthMins ) >= 0) ? ( hrsInMonth * 60 - monthHrs * 60 ) : 0 ;
  var salCurr  = totalMonthMin * salPerMin;
  return {
    bonus,
    fix,
    monthHrs,
    monthMins, // сколько минут должен отработать в этом месяце
    totalMonthMin, // сколько минут отработал
    totalMinsOver, // сколько переработок в минутах
    salMonth,
    trueFix,
    salPerMin,
    byDay,
    nedorabotal,
    totalLateMin,
    totalSalary
    // monthInfo
  }
}
// console.log(Ago(['hours','minutes'],[1,-120]).toString())

// module.exports.fake = fakeDays;
// var testd  = [
//   {
//     day:1,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 1, '10:00'),nd(2017, 8, 1, '18:00')]
//     ]
//   },
//   {
//     day:2,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 2, '10:00'),nd(2017, 8, 2, '16:00')]
//     ]
//   },
//   {
//     day:4,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 4, '10:00'),nd(2017, 8, 4, '18:00')]
//     ]
//   },
//   {
//     day:5,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 5, '10:00'),nd(2017, 8, 5, '18:00')]
//     ]
//   },
//   {
//     day:6,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 6, '10:00'),nd(2017, 8, 6, '18:00')]
//     ]
//   },
//   {
//     day:7,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 7, '10:00'),nd(2017, 8, 7, '18:00')]
//     ]
//   },
//   {
//     day:8,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 8, '10:00'),nd(2017, 8, 8, '18:00')]
//     ]
//   },
//   {
//     day:9,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 9, '10:00'),nd(2017, 8, 9, '16:00')]
//     ]
//   },
//   {
//     day:11,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 11, '10:00'),nd(2017, 8, 11, '18:00')]
//     ]
//   },
//   {
//     day:12,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 12, '10:00'),nd(2017, 8, 12, '18:00')]
//     ]
//   },
//   {
//     day:13,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 13, '10:00'),nd(2017, 8, 13, '18:00')]
//     ]
//   },
//   {
//     day:14,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 14, '10:00'),nd(2017, 8, 14, '18:00')]
//     ]
//   },{
//     day:15,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 15, '10:00'),nd(2017, 8, 15, '18:00')]
//     ]
//   },
//   {
//     day:16,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 16, '10:00'),nd(2017, 8, 16, '16:00')]
//     ]
//   },
//   {
//     day:18,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 18, '10:00'),nd(2017, 8, 18, '18:00')]
//     ]
//   },
//   {
//     day:19,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 19, '10:00'),nd(2017, 8, 19, '18:00')]
//     ]
//   },
//   {
//     day:20,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 20, '10:00'),nd(2017, 8, 20, '18:00')]
//     ]
//   },
//   {
//     day:21,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 21, '10:00'),nd(2017, 8, 21, '18:00')]
//     ]
//   },
//   {
//     day:22,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 22, '10:00'),nd(2017, 8, 22, '18:00')]
//     ]
//   },
//   {
//     day:23,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 23, '10:00'),nd(2017, 8, 23, '16:00')]
//     ]
//   },
//   {
//     day:25,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 25, '10:00'),nd(2017, 8, 25, '18:00')]
//     ]
//   },
//   {
//     day:26,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 26, '10:00'),nd(2017, 8, 26, '18:00')]
//     ]
//   },
//   {
//     day:27,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 27, '10:00'),nd(2017, 8, 27, '18:00')]
//     ]
//   },
//   {
//     day:28,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 28, '10:00'),nd(2017, 8, 28, '18:00')]
//     ]
//   },
//   {
//     day:29,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 29, '10:00'),nd(2017, 8, 29, '18:00')]
//     ]
//   },
//   {
//     day:30,
//     month: 8,
//     year: 2017,
//     check: [
//       [nd(2017, 8, 30, '10:00'),nd(2017, 8, 30, '16:00')]
//     ]
//   }
// ]

// console.log(
//   SalaryInfo(80000, 75, 2017, 8, 420, 300, [], testd, '','','','','fixed' )
// );
module.exports.CompareDate = CompareDate;
module.exports.SalaryInfo = SalaryInfo;
