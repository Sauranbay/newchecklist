var BotHelper = require('./BotFunctions');
var lunchMenu = [//21
  {index: 21, // lunch
    qType: 'text',
    options: BotHelper.makeButtons(['Пойду поем','Уже наелся', '<-- Назад'],
                                   ['ToLunch','FromLunch','back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^ToLunch/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^FromLunch/i,
        next: 20 // индекс инфо по зар.плате admin
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
  ],
    data: 'Что у вас с обедом?',
    err: 'Нажмите сначала на кнопку.'
  }
]
var earlyMenu = [// 22 - 23
  {index: 22, //
    qType: 'text',
    options: BotHelper.makeButtons(['Сегодня','Завтра', '<-- Назад'],
                                   ['today','tomorrow','back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^today/i
      },
      {
        AnsType: 'inline',
        reg:/^tomorrow/i,
        next: 24
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ],
    data: 'Когда уходить собрались?',
    err: 'Сначала на кнопку нажмите.'
  },
  {index: 23, // time
    qType: 'text',
    data: 'На сколько раньше?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['1 час','2 часа', '3 и более', '<-- Назад'],
                                   ['1 час', '2 часа', '3 и более часов', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 22 // <-
      }
    ]
  },
  {index: 24, // time
    qType: 'text',
    data: 'На сколько раньше?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['1 час','2 часа', '3 и более', '<-- Назад'],
                                   ['1 час', '2 часа', '3 и более часов', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 22 // <-
      }
    ]
  }
]
var lateMenu = [//24
  {index: 26, // time
    qType: 'text',
    data: 'На сколько опаздываете?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['15 минут','1 час', '3 часа и более', '<-- Назад'],
                                   ['15 минут','1 час', '3 часа и более', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^[0-9]/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ]
  }

]
var willGo = [//25
  {index: 25, //
    qType: 'text',
    options: BotHelper.makeButtons(['Личные дела','Пошел в банк', 'Пошел на почту', 'Пошел в налоговый комитет', '<-- Назад'],
                                   ['self','bank', 'mail', 'nalog', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/came/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/self||bank||mail||nalog/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'text',
        next: 20
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
  ],
    data: 'Почему вы уходите? Нажмите на кнопку или напишите.',
    err: 'Сначала на кнопку нажмите или напишите.'
  },
  {index: 27, // time
    qType: 'text',
    data: 'Когда не придете?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['Сегодня','Завтра', '<-- Назад'],
                                   ['Сегодня','Завтра', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(.*)/i,
        next: 20 // индекс оповестть начальника
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 20 // <-
      }
    ]
  }
]
var alertMenu = [//21-25
  ...lunchMenu,
  ...earlyMenu,
  ...lateMenu,
  ...willGo
]
var salaryIndex = 37;
var salaryMenu = [
  {index: 37, // time
    qType: 'text',
    data: 'Чем могу помочь?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['ЗП за месяц', 'Прогноз ЗП', '<-- Назад'],
                                   ['sal_month','prediction', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(sal_month)/i,
        next: 38 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/(prediction)/i,
        next: 36 // aler
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 // <-
      }
    ]
  },
  {index: 38, // time
    qType: 'text',
    data: 'За какой месяц хотите отчёт? Напишите в формате: "ММ.ГГ"',
    err: 'Формат : ММ.ГГ (04.17).',
    options: BotHelper.makeButtons(['Прошедший месяц', '<-- Назад'], // generate
                                   ['lastmonth', 'back'],1,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(lastmonth)/i,
        next: 36 // aler
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 37 // <-
      }
    ]
  }
]
var alertsIndex = 39;
var alertsMenu = [
  {index: 39, // time
    qType: 'text',
    data: 'Выберите период.',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['За сегодня', 'За текущий месяц', 'За всё время', '<-- Назад'],
                                   ['today', 'thisMonth', 'allTime', 'back'],3,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^(today|thisMonth|allTime)/i,
        next: 36 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 36 // <-
      }
    ]
  }
]
var kosyakIndex = 40;
var kosyakMenu = [
  {index: 40, // time
    qType: 'text',
    data: 'Выберите период.',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['За текущий месяц', 'За прошлый месяц', '<-- Назад'],
                                   ['thisMonth', 'lastMonth', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^(lastMonth|thisMonth|allTime)/i,
        next: 36 // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 // <-
      }
    ]
  }
]
var basicInfo = [
  {index: 36, // time
    qType: 'text',
    data: 'Чем могу помочь?',
    err: 'Сначала на кнопку нажмите.',
    options: BotHelper.makeButtons(['Моя ЗП',
                                    // 'Мои оповещения',
                                    /*'Мои недоработки/опаздания',*/
                                               '<-- Назад'],
                                   ['salary',
                                   // 'alerts', /*'kosyaki',*/
                                     'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/(salary)/i,
        next: salaryIndex // индекс salary
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 // <-
      }
    ]
  }
]
var analMenu = [//35 - 45
  ...basicInfo,
  ...salaryMenu,
  ...alertsMenu
]
var hallMenu = [//60-80
  {index: 60, // user menu after rabota button
    qType:'text',
    options: {},
    data: 'Выберите зал или нажмите \"Назад\"',
    err: 'Выберите зал или нажмите \"Назад\"',
    answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 61
      }
    ]
  },
  {index: 61, // user menu after rabota button
    qType:'text',
		options: BotHelper.makeButtons(['Сегодня', 'Завтра', 'Послезавтра', 'Другой день', 'Назад'], ['today', 'tomorrow', 'aftertomorrow', 'other', 'back'], 3, true),
		data: 'Выберите день:',
		err: 'Выберите день:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 62
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 61
      }
		]
  },
  {index: 62,

		qType:'text',
		options: BotHelper.makeButtons(['До обеда', 'После обеда', 'Назад'], ['morning', 'evening', 'back'], 2, true),
		data: 'Выберите период:',
		err: 'Выберите период:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 63
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 61
      }
		]
	},
	{index: 63,

		qType:'text',
		options:{},
		data: 'Выберите время:',
		err: 'Выберите время:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 64
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 62
      }
		]
	},
	{index: 64,

		qType:'text',
		options: BotHelper.makeButtons(['1 час', '2 часа', '3 часа', 'Назад'], ['1', '2', '3', 'back'], 3, true),
		data: 'Определите продолжительность',
		err: 'Определите продолжительность',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 65
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 63
      }
		]
	},
	{index: 65,

		qType:'text',
		options: {},
		data: '....',
		err: '....',
		answers: [
      {
        AnsType: 'text',
        reg:/^(.*)$/i,
        next: 0
      }
		]
	},
	{index: 66,

		qType:'text',
		options: {},
		data: 'Напишите дату в формате "ДД.ММ.ГГГГ"',
		err: 'Напишите дату в формате "ДД.ММ.ГГГГ"',
		answers: [
      {
        AnsType: 'text',
        reg:/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/i,
        next: 60
      }
		]
	},
  {
    index: 70,

    qType:'text',
    options: {},
    data: 'Отмена брони',
    err: 'Отмена брони',
    answers: [
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 10
      },
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 71
      }
    ]
  },
  {index: 71,

    qType:'text',
    options: {},
    data: 'Отмена брони',
    err: 'Отмена брони',
    answers: [
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 70
      },
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 10
      }
    ]
  }

]
var menu = [//10,20-26
  {index: 10, // user menu after rabota button
    qType: 'text',
    options: BotHelper.makeButtons([
      // 'Предупредить администратора',
      'Информация о ЗП', 'Забронировать Зал', 'Прогноз погоды', 'Курсы валют', 'Отменить бронь', '<< В главное меню'],
                                   [
                                   // 'alert',
                                   'salaryInfo', 'зал', 'погода', 'курс', 'отмена', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^salaryInfo/i,
        next: salaryIndex // индекс инфо по зар.плате Юзера
      },
      {
        AnsType: 'inline',
        reg:/^зал/i,
        next: 60
      },
      {
        AnsType: 'inline',
        reg:/^погода/i,
        next: 2
      },
      {
        AnsType: 'inline',
        reg:/^курс/i,
        next: 2
      },
      {
        AnsType: 'inline',
        reg:/^отмена/i,
        next: 70
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 2 // <-
      }
    ],
    data: 'Выберите действие',
    err: 'Сначала на кнопку нажмите.'
  },
  {index: 20, // оповещание юзер
    qType: 'text',
    options: BotHelper.makeButtons(['Обед','Опоздаю', 'Ухожу по делам', 'Уйду пораньше', 'Не приду', '<-- вернуться'],
                                   ['lunch', 'will_late', 'willGo', 'early','not come', 'back'],2,true),
    answers:[
      {
        AnsType: 'inline',
        reg:/^lunch/i,
        next: 21 //
      },
      {
        AnsType: 'inline',
        reg:/^not come/i,
        next: 27
      },
      {
        AnsType: 'inline',
        reg:/^will_late/i,
        next: 26 //
      },
      {
        AnsType: 'inline',
        reg:/^willgo/i,
        next: 25 //
      },
      {
        AnsType: 'inline',
        reg:/^early/i,
        next: 22 //
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 10 //
      }
    ],
    data: 'Выберите действие',
    err: 'Сначала на кнопку нажмите.'
  },
  ...alertMenu,
  //
  ...analMenu,
  ...hallMenu
]


module.exports = menu
