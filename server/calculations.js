function nd(a,b,c,d){
   var cur = new Date();
   if(!a){
     return cur;
   }
   if(typeof(a)=='string'){
    if(a.indexOf(':')>-1){
    var num = a.split(":");
    if(!b && (typeof(b*1)!="number")){
      var date = new Date(cur.getFullYear(), cur.getMonth(), cur.getDate(), num[0], num[1]);
    } else {
      var date = new Date(b.getFullYear(), b.getMonth(), b.getDate(), num[0], num[1]);
    }
    return date;
    }
  }
 if(a && (typeof(b*1)=="number") && c && d){
   var num = d.split(":");
   var date = new Date(a, b, c, num[0], num[1], '00');
   return date;
 }
}
function today(d){
  var tdy = new Date();
  if(d.getFullYear() == tdy.getFullYear()
  && d.getMonth() == tdy.getMonth()
  && d.getDate() == tdy.getDate()
  ) return true
  else return false
}
function Ago( what, count, initDate){
  var def = 'day';

  count = count ? count : ((typeof(initDate) == 'object') ? 1 : initDate*1 );
  initDate = initDate ? ((typeof(initDate) == 'object') ? initDate : new Date() ): new Date();
  what = what ? what : def;
  itr = Array.isArray(what) ? what.length : 1;
  for (var i = 0; i < itr; i++) {
    w = Array.isArray(what) ? what[i] : what;
    cnt = Array.isArray(count) ? ((count.length > i) ? count[i] : count[count.length-1]) : count;
    switch (w.toLowerCase()) {
      case 'years':
      var d =  (initDate.getFullYear()+cnt)
      initDate.setYear(d);
      break;
      case 'months':
      var d =  (initDate.getMonth()+cnt);
      initDate.setMonth(d);
      break;
      case 'days':
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
      break;
      case 'hours':
      var d =  (initDate.getHours()+cnt) ;
      initDate.setHours(d);
      break;
      case 'minutes':
      var d =  (initDate.getMinutes()+cnt);
      initDate.setMinutes(d);
      break;
      case 'seconds':
      var d =  (initDate.getSeconds()+cnt) ;
      initDate.setSeconds(d);
      break;
      default:
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
    }
  }
  return initDate;
}
function CompareDate(Fdate, Sdate, returnBy, abs) {
  if(typeof(Sdate) == 'string') {
    var spl = Sdate.split(':');
    var Sdate = new Date(Fdate);
    Sdate.setHours(spl[0]);
    Sdate.setMinutes(spl[1]);
    // console.log(Sdate.toString(), Fdate.toString())
  }
  delta = Fdate.getTime() - Sdate.getTime();
  returnBy = returnBy ? returnBy : 'ms';
  var res = 0;
  switch (returnBy.toLowerCase()) {
    case 'year':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7 / 52);
      break;
    case 'week':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7);
      break;
    case 'day':
      res = Math.floor(delta / 1000 / 60 / 60 / 24);
      break;
    case 'hour':
      res = Math.floor(delta / 1000 / 60 / 60);
      break;
    case 'min':
      res = Math.floor(delta / 1000 / 60);
      break;
    case 'sec':
      res = Math.floor(delta / 1000 );
      break;
    case 'ms':
      res = Math.floor(delta);
      break;
    default:
      res = Math.floor( delta / 1000 / 60 );
  }
  return abs ? Math.abs(res) : res;
}
function daysInMonth(year, month){
  var date = new Date(year*1,month*1);
  date.setDate(32);
  return 32 - date.getDate();
}
function getMonthInfo(year, month, prazd){
  var budni = [];
  var subbot = [];
  var vih = [];
  var pr = prazd || [];
  var dim = daysInMonth(year*1,month*1);
  var nd = new Date(year*1,month*1,1);
  var d = 0;
  for (var i = 1; i < dim+1; i++) {
    nd.setDate(i);
    d = nd.getDay();
    if(d == 6 && pr.indexOf(i) == -1){
      subbot.push(i);
    }else {
      if(d != 0 && pr.indexOf(i) == -1){
        budni.push(i);
      }else{
        vih.push(i);
      }
    }
  }

  return {
    days: dim,
    budni: budni,
    subbot: subbot,
    vih: vih
  }
  // sd.setDate(sd.getDate())
}
function calcHrs(year, month, fixT, fixST, prazd){
  // calcHrs(2017(год),0(месяц),7(часы в будни),5(часы в субботу),[](праздники))
  var mon = getMonthInfo(year,month,prazd);
  var mins = (mon.budni.length*(fixT*1))+(mon.subbot.length*(fixST*1));
  console.log(mon.budni.length,'+',fixT,';',mon.subbot.length,'+',(fixST*1),';',mon, mins);
  return mins;
}
function rawToDate(raw) {
  var d = raw.split(/\.|\-|\ /);
  if((d[0]*1 > 31) || (d[0]*1 < 1) || (d[1]*1 > 12) || (d[1]*1 < 1)){
    return new Date(d[2],d[1]-1,d[0]);
  }else{
    return false;
  }
}
// console.log(splitByTime(nd("09:54"),nd("18:00"),['08:30','18:00']));
function splitByTime (fdate, sdate, time) {
  console.log(fdate.toString(),sdate.toString());
  fdate = new Date(fdate);
  sdate = new Date(sdate);
  function chk (f,s,h,m){
    // var td = new Date(f);
    // td.setHours(h*1);
    // td.setMinutes(m*1);
    var td = nd(new Date(f).getFullYear(),new Date(f).getMonth(),new Date(f).getDate() ,h+":"+m);
    // console.log(td.toString(),'======= < TD ['+h,':',m+']');
    console.log('F['+fdate.toString()+']','S['+sdate.toString()+']','['+h,':',m+']',CompareDate(td, s),'< s compare f >',CompareDate(td, f));
    if(CompareDate(td, s)<=0){
      if(CompareDate(td, f)>=0) {return true;} else {return false}
    } else {
      if(CompareDate(td, f)<=0) {return true;} else {return false}
    }
  }
  var t1 = time[0].split(':');
  var t2 = time[1].split(':');
  var fa = [];
  var c = false
  var splInd = 0;
  var subDate = new Date(sdate)
  var late = true;
  var lateMin = 0;
  console.log('CHK  t1 is ',chk(fdate,sdate,t1[0],t1[1]));
  if(chk(fdate,sdate,t1[0],t1[1])){
    console.log('1');
    splInd=1;
    late = false;
    subDate.setHours(t1[0]*1);
    subDate.setMinutes(t1[1]*1);
    fa.push([fdate, subDate])
    // console.log('pushed 1 >', fa[0])
    if(chk( subDate, sdate, t2[0], t2[1] )){
      console.log('3');
      splInd=3;
      var subDate2 = new Date(subDate);
      subDate2.setHours(t2[0]*1);
      subDate2.setMinutes(t2[1]*1);
      fa.push([subDate, subDate2]);
      // console.log('pushed !2 >', fa[0])//fa[1][0].getHours(),fa[1][1].getHours())
      fa.push([subDate2, sdate]);
      // console.log('pushed !3 >', fa[0])//fa[2][0].getHours(),fa[2][1].getHours())
    } else {
      console.log('4');
      fa.push([subDate, sdate]);
      // console.log('pushed 2 >', fa[0])//[1][0].getHours(),fa[1][1].getHours())

    }
    c = true;
  } else {
    console.log('else t2' , chk(fdate,sdate,t2[0],t2[1]));
    if(chk(fdate,sdate,t2[0],t2[1])){
      console.log('2');
      splInd=2;
      subDate.setHours(t2[0]*1);
      subDate.setMinutes(t2[1]*1);
      fa.push([fdate, subDate])
      fa.push([subDate, sdate]);
      var lat = CompareDate(fdate, time[0], 'min');
      console.log('lat?! >',lat);
      if(lat > 0 ) {
        lateMin = Math.abs(lat);
      }
      c = true;
    }
  }
  if(!c){
    fa.push([fdate,sdate])
    var lat = CompareDate(fdate, time[0], 'min');
    if(lat > 0 ) {
      if(CompareDate(sdate, time[1], 'min') < 0){
        // console.log('passed!')
        lateMin = Math.abs(lat);
      }
    }
  }
  // console.log(fa[0][0].getHours(),fa[0][1].getHours())
  return {
    late,
    lateMin,
    result: fa,
    splInd,
    splited: c
  };
}
function isDate(dt) {
  if(typeof(dt) === 'object'){
    return (typeof(dt.getMonth) === 'function')
  } else {
    return false;
  }
}
function splitDates(dArr, sTimes, returnBy) {
   var type = returnBy ? returnBy : 'minutes';
console.log('===========Splitting ',new Date(dArr[0][0]).toString(),new Date(dArr[0][1]).toString(),'===============');
   //  var st = 0;
   var result = {
     before:0,
     lateMin:0,
     between:0,
     early:0,
     after:0
   };
   var tween = [];
   var n = false;
   var subd = [];
   var late = true;
   var pre = [];
   dArr.forEach(function (check, i) {
     if(!isDate(check[1])) {
      //  console.log(check)
       if(today(check[0])){
         check[1] = nd();
        //  console.log('setting now');
       } else {
        //  console.log('setting 23:59');
         check[1] = nd('23:59',check[0]);
       }
     }
     var spl = splitByTime(check[0], check[1], sTimes);
     console.log('spl - > ',spl, sTimes)
     if(spl.splited) {
       switch (spl.splInd) {
         case 1:
        //  console.log('1 > ',spl)
         if(spl.late == false) late == false;
         result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
         result.between += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
           break;
           case 2:
          //  console.log('2 > ',spl)
           if(spl.late){
             pre.push(spl.lateMin)
           }
           result.after += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
           result.between += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;

             break;
             case 3:
            //  console.log(spl)
            //  console.log('3 > ',spl)
             result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
             result.between += CompareDate(spl.result[1][0],spl.result[1][1],'min',true)*1;
             result.after += CompareDate(spl.result[2][0],spl.result[2][1],'min',true)*1;
               break;
         default:
        //  console.log('default act')
       }
     } else {
       // если первая дата раньше чем вторая , то результат: "+"
       console.log('not splited',sTimes);
       var cmp = CompareDate(spl.result[0][0],sTimes[0])*1;
       var cmp2 =CompareDate(spl.result[0][1],sTimes[1])*1;
       if(spl.late){
         pre.push(spl.lateMin)
       }
      //  console.log('must be  > 0 ---',cmp,'must be < 0 --',cmp2)
       if(cmp > 0 && cmp2 < 0){
        //  console.log('><')
         result.between += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;
       } else {
        //  console.log('<>')
         if(cmp > 0 && cmp2 > 0) result.after += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;//e
         if(cmp < 0 && cmp2 < 0) result.before += CompareDate(spl.result[0][0],spl.result[0][1],'min',true)*1;//b
       }
     }
   })
   if(late){
     pre.reverse().forEach(function (l) {
       if(l>0){
         result.lateMin = l;
       }
     })
   }
  //  tween.forEach(function (t) {
  //    console.log(t[0].getHours()+':'+t[0].getMinutes(),t[1].getHours()+':'+t[1].getMinutes())
  //  })
  //  console.log(result)
   return result;
}
function getSummary(checkArray, end) {
  var sum = 0;
  var result = {}
  checkArray.forEach(function (ch, i) {
    var chIn  = ch[0];
    var chOut = ch[1];
    if(!isDate(ch[1])) {
      if(today(ch[0])){
        chOut = nd();
      } else {
        chOut = nd((end || '23:59'),ch[0]);
      }
    }
    sum += CompareDate(new Date(chIn), new Date(chOut), 'min', true);
  })
  // result.
  return sum;
}

//                               %                  7     5    [12,7] [ check: [[date], [date]] ]
// console.log(calcHrs(2017,07,8,0,[]))
// соединить праздники личные и общие. ОБЯЗАТЕЛЬНО СЧИТАТЬ ПРАЗДНИКИ КАК ВОСКРЕСЕНЬЕ! А В ВОСКРЕСЕНЬЕ СЧИТАТЬ ПЕРЕРАБОТКИ!!
function SalaryInfo(totalSalary, bp, year, month, fixT, fixST, prazd, days, inTime, outTime, SinTime, SoutTime ,type){
  var bonus = ((totalSalary / 100) * bp);
  var fix = totalSalary - bonus;
  var monthInfo = getMonthInfo(year, month, prazd);
  var monthMins = calcHrs(year, month, fixT, fixST, prazd);
  fixT = fixT / 60;
  fixST = fixST / 60;
  var monthHrs = monthMins / 60;
  var salPerHour = fix / monthHrs;
  var salPerMin = fix / monthMins;
  var salMonth = (monthInfo.subbot.length*salPerHour*fixST) + (monthInfo.budni.length*salPerHour*fixT)
  var byDay = [];
  // рассчитывает по ранним минутам
  function earlyMinusLate(sunday, earlyMins, todayMins, afterMins, warned) {
    if(sunday){
      var total = earlyMins+todayMins+afterMins;
      var emr = lateMins - total;
      console.log('SUNDAY emr', emr, total, lateMins);
      // console.log(Fulfilled,'<=== fulfilled now| from ==>' , lateMins);
      if ( emr < 0 ) { //если опаздания - всё время меньше нуля (опазданий меньше) emr это то что осталось после отнятия опазданий
        Fulfilled += lateMins;
        lateMins = 0;
        total = Math.abs(emr);
        emr = notedLateMins - total;
        if(emr < 0) { /// тоже самое
          Fulfilled += notedLateMins;
          notedLateMins = 0;
          total = Math.abs(emr);
          // if(warned){
            // if( CompareDate(warned, Ago('hours', -8, nd(outTime)), 'hour') >= 0 ) {
              // console.log('not warned so emr =', emr, Nedorabotal_weekend - Math.abs(emr));
              emr = Nedorabotal_weekend - total;
              if(emr < 0) { // если после отонятия остоается остаток
                Dorabotal += Nedorabotal_weekend;
                Nedorabotal_weekend = 0;
                total = Math.abs(emr);
                emr = Nedorabotal - total;
                if(emr < 0) { // если после отонятия остоается остаток
                  Dorabotal += Nedorabotal;
                  Nedorabotal = 0;
                  overMins += Math.abs(emr);     //////// тут идет переработка в воскресенье
                } else {
                  Dorabotal += total;
                  Nedorabotal = emr;
                }
              } else {
                Dorabotal += total;
                Nedorabotal_weekend = Math.abs(emr);;
              }
            // }
          // }
        } else {
          Fulfilled += total;
          notedLateMins = Math.abs(emr);
        }
      } else {
        Fulfilled += total// lateMins - total; // по тойж е логике дальше исправить
        lateMins = Math.abs(emr);
      }
      // earlyMins = 0;

    } else {// не выходной
      var emr = notedLateMins - earlyMins;
      //all++
      if(emr < 0) {
        Fulfilled += notedLateMins;
        notedLateMins = 0;

        emr = Nedorabotal - earlyMins;
        if(emr < 0) {
          Dorabotal += Nedorabotal;
          Nedorabotal = 0;
        } else {
          Dorabotal += earlyMins;
          Nedorabotal = emr;
        }

      } else {
        Fulfilled += earlyMins;
        notedLateMins = notedLateMins - earlyMins;
      }
      earlyMins = 0 //обнуляется раннее
    }
  }
  // рассчитывает по переработкам
  function overMinusLate(after, late){
    var omr = notedLateMins - after;
    // console.log('omr (1)')
    if(omr < 0) {
      Fulfilled += notedLateMins;
      after = after - notedLateMins;
      notedLateMins = 0;

      omr = Nedorabotal - after;
      // console.log(omr,'Nedorabotal - after')
      if(omr < 0) {
        Dorabotal += Nedorabotal;
        after = after - Nedorabotal;
        if(!late){
          overMins += after;
        }
        // console.log(Dorabotal,'< Dora Botal           ',after,'<- after' );
        Nedorabotal = 0;
      } else {
        Dorabotal += after;
        Nedorabotal = omr;
        after = 0;
      }

    } else {
      Fulfilled += after;
      notedLateMins = notedLateMins - after;
      after=0;
    }

  }
  if(days) {
    var Fulfilled = 0;
    var lateMins = 0;
    var notedLateMins = 0; // 60
    var overMins = 0;
    var totalMonthMin = 0;
    var todayMustWork = 0;
    var Nedorabotal = 0;
    var Nedorabotal_weekend = 0;
    var Dorabotal = 0;
    var workedDays = 0;
    var remaining = 0;
    var selfGo = 0;
    var tempdata = 0;
    var itogo = {
      late :0,
      after :0,
      between :0,
      nedorabotal:0
    }
    var total_Nedorabotki = 0;

    days.forEach(function (day,i) {
      console.log('============Это '+day.day+'й день==============')
      if(!day.notes) day.notes = {};
      if(day.check && day.year == year && day.month == month ){
        var thisDay = nd(day.year,day.month,day.day,'10:00').getDay();
        type = type || 'fixed';
        switch (type.toString().toLowerCase()) {
          case 'free':
            var t = getSummary(day.check);
            totalMonthMin += t;
            break;

          default:
            // console.log((thisDay == 6) ? [SinTime,SoutTime] : [inTime,outTime]);
            var dayResult = splitDates(day.check, (thisDay == 6) ? [SinTime,SoutTime] : [inTime,outTime]);
            itogo.late += dayResult.lateMin*1;
            itogo.after += dayResult.after*1;
            itogo.between += dayResult.between*1;
            console.log(dayResult);
            if(day.notes.zabil){
              console.log('Forgot!');
              dayResult.after = 0;
            }

            // if(dayResult.between > 180){
            //   workedDays++;
            // }
            // console.log(totalMonthMin,'+',dayResult.between)
            var weekEnd = (fixST == 0) ? ((thisDay == 6) ? true : ((thisDay == 0 || prazd.includes(day.day*1)) ? true : false) ) : (thisDay == 0 || prazd.includes(day.day*1)) ? true : false;
            // var weekEnd = (fixST == 0) ? ((thisDay == 6) ? true : ((thisDay == 0) ? true : false) ) : (thisDay == 0) ? true : false;
            console.log(dayResult, day.day, weekEnd? 'Выходной':".");
            var selfTemp = 0;
            if(day.notes.selfGo){
              selfTemp = getSummary(day.notes.selfGo, outTime)
              dayResult.between -= selfTemp;
              selfGo += selfTemp;
              console.log('Ушел по своим делам сегодня, отсутствовал (мин): '+selfTemp, 'отнял их от общего дня (',dayResult.between,')');
              if(dayResult.between < 0) dayResult.between = 0;
            }
            if(dayResult.between > 60) dayResult.between -= weekEnd ? 0 : 60; /////// тут я отнял час обеда
            console.log('Убрал из времени Обед:',dayResult.between);
            if(!weekEnd) {
              totalMonthMin += dayResult.between;
              if(day.notes.late){
                console.log('Предупредил об опаздании: ',dayResult.lateMin,'+', notedLateMins);
                notedLateMins += dayResult.lateMin;
              } else {
                console.log('Не предупредил об опаздании: ',dayResult.lateMin,'+', lateMins);
                lateMins += dayResult.lateMin;
              }
              console.log('Так как сегодня не выходной, сегодняшнее время сплюсовываю с общим и теперь общее время :', totalMonthMin);
            }
            todayMustWork = weekEnd ? 0 : ((thisDay == 6) ? (fixST * 60) : (fixT * 60))  - dayResult.lateMin //- 60
            console.log('Сегодня он должен отработать: ',todayMustWork);
            console.log('Но недоработал я :' , weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between));
            // console.log((fixT * 60), '60' , dayResult.lateMin,)
            // earlyMins = dayResult.before;

            earlyMinusLate(weekEnd,
            dayResult.before,dayResult.between,dayResult.after);

            // overMins += dayResult.after;
            // func  && !day.notes.zabil

            if((notedLateMins > 0||Nedorabotal > 0)){
              console.log('Так как есть предупреждённые опоздания, я буду их умертвлять. Было опазд:',notedLateMins,'было недор:',Nedorabotal);
              overMinusLate(dayResult.after, ((dayResult.lateMin > 0) ? true : false)  );
              console.log('Стало опазд:', notedLateMins,'стало недор:',Nedorabotal);
            } else {
              overMins += dayResult.after;
              console.log('Так как задолжностей нет, переработки сохраняем: ',overMins,'+',dayResult.after);
            }
            total_Nedorabotki+=weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)
            if(day.notes.nedorabotaiu){
              // console.log(((todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)*-1,'<<<<<')
              var n = CompareDate(day.notes.nedorabotaiu,
              Ago(['hours','minutes'],
              [-8,((todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)*-1],
              nd(day.year,day.month,day.day,outTime)), 'hour');
              console.log(n,'<<< - CompareDate');
              if( n >= 0 ) {
                remaining = weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)
                Nedorabotal_weekend += remaining;
              } else {
                remaining = weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)
                Nedorabotal += remaining;
              }
            }
            itogo.nedorabotal += (weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between))*1;
            var nedor = (weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between))*1;
            tempdata += todayMustWork
            //td2 += todayMustWork - dayResult.between
            console.log('На данный момент всего нужно работать: ',tempdata);
            // console.log(todayMustWork,'remaining - >',weekEnd ? 0 : ( (todayMustWork - dayResult.between < 0) ? 0 : todayMustWork - dayResult.between)/*dayResult*/, day.day, lateMins,notedLateMins)
            // if(Nedorabotal > 0){console.log('HERE--------------------------')}
            byDay.push({
              year: day.year,
              month: day.month,
              day: day.day,
              weekEnd,
              late: dayResult.lateMin,
              nedor,
              nedorabotal: itogo.nedorabotal,
              remaining,
              todayMustWork,
              Fulfilled,
              selfGo: selfTemp,
              beforeWorkTime: dayResult.before,
              workTime: dayResult.between,
              afterWorkTime: dayResult.after,
              check : day.check,
              notes: day.notes,
              salPerMin
            })
            console.log(itogo)//byDay[byDay.length-1]);
            console.log({total: totalMonthMin+'+'+Fulfilled+'+'+Dorabotal,notedLateMins,lateMins,totalMinsOver, total_Nedorabotki});
            // console.log(Nedorabotal,'<- Недоработал',Nedorabotal_weekend,'<- Недоработал_weekend',todayMustWork+'< сегодня должен отработать',lateMins+'<late',notedLateMins,'noted')
            break;
        }
      }
      console.log('-------------------------------------------------')
    })
  }
  if(totalMonthMin > monthMins){
    var o = totalMonthMin - monthMins;
    // console.log('Переполнение >',o);
    overMins += o;
    totalMonthMin -= o;
  }
  var hrsInMonth = totalMonthMin / 60;
  //НАЧАЛО рассчет заработной платы
  var trueFix  = totalMonthMin * salPerMin;
  // console.log(tota);
  // console.log((totalMonthMin-(60*workedDays)), workedDays)
  var FulfilledMoney = Fulfilled * salPerMin;
  var finalBonusSal=0;
  var spm = salPerMin /2;
  for (var i = 1; i < overMins+1; i++) {
    if(i % 60==0){
      spm=spm/2;
    }
    finalBonusSal += spm;
  }
  //КОНЕЦ рассчет заработной платы
  var totalMonthLates = ((lateMins + notedLateMins) >= 0) ? (lateMins + notedLateMins) : 0;
  // var hrsLeft  =(( monthHrs - hrsInMonth ) >= 0) ? (monthHrs - hrsInMonth) : 0;
  var minsLeft =(( monthMins+Fulfilled+Dorabotal - totalMonthMin ) >= 0) ? ( monthMins+Fulfilled+Dorabotal - totalMonthMin) : 0 ;
  // var hrsOver  =(( hrsInMonth - monthHrs ) >= 0) ? ( hrsInMonth - monthHrs ) : 0 ;
  var totalMinsOver = overMins;//(( totalMonthMin - monthMins ) >= 0) ? ( hrsInMonth * 60 - monthHrs * 60 ) : 0 ;
  var salCurr  = totalMonthMin * salPerMin;
  var salLeft  =  minsLeft * salPerMin ;
  // var salPerDay = salPerHour * ((condition) ? fixST:fixT)//fix / monthInfo.budni.length+monthInfo.subbot
  return {
    bonus,
    fix,
    monthHrs,
    monthMins, // сколько минут должэен отработать в этом месяце
    // Dorabotal,
    // Nedorabotal,
    lateMins,
    notedLateMins,
    totalMonthMin, // сколько минут отработал
    totalMonthLates, // сколько опазданий осталось в минутах
    totalMinsOver, // сколько переработок в минутах
    Fulfilled, // сколько отрабюотанных опазданий
    minsLeft, // сколько минут не доработал

    Nedorabotal,
    Nedorabotal_weekend,
    Dorabotal,
    salMonth,
    trueFix,
    finalBonusSal,
    salPerMin,
    FulfilledMoney,
    selfGo,
    byDay

    // monthInfo
  }
}


// console.log(Ago(['hours','minutes'],[1,-120]).toString())
module.exports.CompareDate = CompareDate;
module.exports.SalaryInfo = SalaryInfo;
// module.exports.fake = fakeDays;
