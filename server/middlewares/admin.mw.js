const Configs    = require('../configs/configs')
const jwt        = require('jsonwebtoken')
const Employee       = require('../models/employee')


function verifyJWT_MW(req, res, next){
    // let token = (req.method === 'POST') ? req.body.token : req.query.token
    var token = req.headers['authorization'];
    // console.log(jwt.decode(token, Configs.auth.JWT_SECRET));
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, Configs.auth.JWT_SECRET, function(err, employee) {
            if (err) {
            	console.log('error cannot verify token!');
                return res.status(403).send({err: err, data: null}).end();    
            } else {
            	// console.log('test2');
                Employee.findOne({employee_id:employee._id.toUpperCase(), admin:employee.admin}).exec((err, result) => {
                    if(err) res.status(403).send({err: 'Failed to authenticate token.', data: null}).end();   
                    else if(result && (employee.admin == true)) {
                        // if everything is good, save to request for use in other routes
                        // console.log('test3');
                        req.employee = employee;    
                        next();
                    } else {
                        res.status(403).send({err: 'Failed to authenticate token. Maybe it is already expired. Or user is not exists.', data: null}).end();
                    }
                })

            }
        });

    } else {
    	console.log('не увидел токен');
        // if there is no token
        // return an error
        return res.status(403).send({err: 'No token provided.', data: null}).end();

    }

}



module.exports =  {
  verifyJWT_MW,
}
