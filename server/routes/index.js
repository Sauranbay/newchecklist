var express = require('express');
var router = express.Router();
var Employee = require('../models/employee');
var adminAccess = require('../models/adminAccess');
var BotHelper = require('../BotFunctions');
var fs = require('fs');
var CheckListBot = require('node-telegram-bot-api');
var token ="575820373:AAGR9MJ_gPJL6iKqr0glHO8CljXkZSEFFhg"; //checklist_automato_bot
// var token = "482832370:AAG8UruzzifdXd28Vhw4M2G4Za29kpEui3c" //kanakma bot
// var token = "519957309:AAFwx9C5-7_OqFmb2a3fSrHo-i_L5G2NWO4"
var bot = new CheckListBot(token, {
  polling: true
});
var path = require('path');
var imgPath = path.join(__dirname, '../../images/');
var lastPhoto = path.join(__dirname, '../../static/photos/');
var existImg = [];
var cron = require('node-cron');
var calc = require('../calculations');
const file = require('../files');

const jwt = require('jsonwebtoken');
const Config = require('../configs/configs');

///////////////////////////////////////////////////////////////////////////////////////////////////////
// находит индекс элемента в массиве объектов
function IndInObjArr(objArray, subj, inkey, sensetive) {
  var sens = ((typeof inkey) === "boolean") ? inkey : false;
  var found = false;
  var result = [];
  if (objArray.length > 0) {
    objArray.forEach(function(obj, ind) {
      if (!sens && inkey) {
        var sub1 = sensetive ? obj[inkey] : obj[inkey].toString().toLowerCase();
        var sub2 = sensetive ? subj : subj.toString().toLowerCase();
        if (sub1 == sub2) {
          found = true;
          result.push(ind);
        }
      } else {
        for (var key in obj) {
          if (obj.hasOwnProperty(key)) {
            var sub1 = sens ? obj[key] : obj[key].toString().toLowerCase();
            var sub2 = sens ? subj : subj.toString().toLowerCase();
            if (sub1 == sub2) {
              found = true;
              result.push(ind);
            }
          }
        }
      }
    })
  }

  if (found) {
    return result;
  } else {
    return false;
  }

}
// находит индексЫ элемента в массиве объектов
function matchInObjArr(objArray, subjarr, keysArr, sensetive) {
  var res = [];
  if (subjarr.length != keysArr.length) {
    return false;
  }
  function getAll(arr) {
    var res = [];
    var was = [];
    var raw = [];
    arr.forEach(function(a, i) {
      a.forEach(function(f, i2) {
        if (was.indexOf(f) == -1) {
          was.push(f);
          raw.push(1);
        } else {
          raw[was.indexOf(f)]++;
          if (raw[was.indexOf(f)] == arr.length)
            res.push(f);
          }
        })
    })
    return res;
  }
  var preResult = [];
  subjarr.forEach(function(subj, si) {
    if (typeof(subj) == 'object') {
      //
      subj.forEach(function(sb) {

        if (typeof(keysArr[si]) == 'object') {
          //
          keysArr[si].forEach(function(ka) {
            var R = IndInObjArr(objArray, sb, ka, sensetive);
            if (R) {
              preResult.push(R);
            }
          })
        } else {
          var R = IndInObjArr(objArray, sb, keysArr[si], sensetive);
          if (R) {
            preResult.push(R);
          }
        }
      })
    } else {
      if (typeof(keysArr[si]) == 'object') {
        //
        keysArr[si].forEach(function(ka) {
          var R = IndInObjArr(objArray, subj, ka, sensetive);
          if (R) {
            preResult.push(R);
          }
        })
      } else {
        var R = IndInObjArr(objArray, subj, keysArr[si], sensetive);
        if (R) {
          preResult.push(R);
        }
      }
    }
  })
  ////
  if (preResult.length = subjarr.length) {
    return getAll(preResult);
  } else {
    return [];
  }

}
///////////////////////////////////////////////////////////////////////////////////////////////////////\
var fn = require('../AllFunctions');
// делает из минут читаемую строку.
function fm(mins) {
  function l(num) {
    num += '';
    switch (num[num.length - 1]) {
      case "1":
        return {h: 'час', m: 'мин.'}
        break;
      case "2":
      case "3":
      case "4":
        return {h: 'часа', m: 'минуты'}
        break;
      default:
        return {h: 'часов', m: 'минут'}
    }
  }
  var hr = Math.floor(mins / 60);
  var mn = (mins % 60);
  return hr + ' ' + l(hr).h + ' ' + mn + ' ' + l(mn).m
}
// делает рандомный набор букв и цифр
function makeHash(){
  var alp = ['a','0','b','c','1','d','e','f','g','2','3','4','h','i','5','j','6','7','k','l','m','n','8','o','p','q','r','s','t','u','v','w','x','y','9','z']
  var r = ''
  for (var i = 0; i < 21; i++) {
    var q = Math.round(Math.random() * alp.length-1)
    if(q<0)q=0;
    r = r+alp[q]
  }
  return r;
}
// принадлежность объекта массиву
function incl(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] == obj) return true;
  }
  return false;
}
// берет значение из массива обхектов
function ValInObjArr(objArray, subj, inkey, sensetive) {
  var sens = ((typeof inkey) === "boolean") ? inkey : false;
  var found = false;
  var result = {};
  if (objArray.length > 0) {

    objArray.forEach(function(obj, ind) {
      if (!sens && inkey) {
        var sub1 = sensetive ? obj[inkey] : obj[inkey].toLowerCase();
        var sub2 = sensetive ? subj : subj.toLowerCase();
        if (sub1 == sub2) {
          found = true;
          result = ind;
        }
      } else {
        for (var key in obj) {
          if (obj.hasOwnProperty(key)) {
            var sub1 = sens ? obj[key] : obj[key].toString().toLowerCase();
            var sub2 = sens ? subj : subj.toString().toLowerCase();
            if (sub1 == sub2) {
              found = true;
              result = ind;
            }
          }
        }
      }
    })
  }

  if (found) {
    return result;
  } else {
    return false;
  }

}
//берет объект по id
function GetObjById(Source, id) {
  var e = false;
  var fnd = {};
  Source.forEach(function(src) {

    if (src.id == id) {
      e = true;
      fnd = src;
    }
  })
  if (e) {
    return fnd;
  } else {
    return e;
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////


// начал ли чекиниться этот id
function ExistId(Source, id) {
  var e = false;
  Source.forEach(function(src) {
    if (src.id == id) {
      e = true;
    }
  })
  return e;
}
var step = [{
  id: 'test',
  step: 0,
  code: '',
  status: 'in',
  photo: '',
  firstname: '',
  lastname: ''
}];
// проверка
function ch(v) {
  if (typeof (v) == 'number') {
    return true
  } else {
    return v;
  }
}
// получает текущий шаг того кто чекинится
function getStep(id) {
  var st = GetObjById(step, id);
  if (!ch(st)) {
    return false
  }
  return st.step;
}
// делает следующий шаг чекина
function nextStep(id) {
  var st = ValInObjArr(step, id, 'id');
  if (st > -1) {
    step[st].step++;
    return true;
  }
  return false
}
// генерирует админский хэш
function genAdminHash(id){
  var hash = makeHash();
  adminAccess.findOneAndRemove({id},function (er,w) {
    var newAccess = new adminAccess({
      id,
      hash
    })
    newAccess.save();
  })
  return hash;
}
// удаляет
function removeAdminHash(id) {
  adminAccess.remove({id}, function (err,removed) {
    if(!err){
      console.log('Admin logged Out. Access hash removed.');
    } else {
      console.log(id+' is not admin. So i`ll ingore him.');
    }
  })
}
// отправляет сообщение всем админам
function SendMsgToAdmins(msg) {
  Employee.find({admin:true},function (err,admins) {
    if(admins){
      admins.forEach(function (admin) {
        var adm = admin.botId;
        //console.log('Sending Error to Boss',photo,adm,msg)
          bot.sendMessage(adm,msg)
      })
    }
  })
}
// отправляет фотку админам( сейчас то не фотка а просто ссылка на фотку)
function SendPhotoToAdmins(photo,msg,then) {
  Employee.find({admin:true},function (err,admins) {
    if(admins){
      admins.forEach(function (admin) {
        var adm = admin.botId;
        console.log('Sending Otchet to Boss',photo,adm,msg)
        // bot.sendPhoto(adm, photo).then(function (a) {
        bot.sendMessage(adm, photo+ '?r=' + new Date().getTime()).then(function (a) {
          bot.sendMessage(adm,msg)
          then();
        }).catch(function (te) {
          console.log('Cant send a Photo->',te)
          then();
        });
      })
    }
  })
}
// зачекинен ли?
function isChecked(id, cb) {
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, function(e, f) {
    if (!e) {
      if (f) {
        date = (f.checked ? f.checkedIn : f.checkedOut);
        cb(null, {
          checked: f.checked,
          date
        });
      } else {
        cb({
          error: true,
          msg: 'User Not Found'
        }, null)
      }
    } else {
      cb({
        error: true,
        msg: 'Mongoose Error'
      }, null)
    }
  })
}
// принимает ([date/"", date/""], date , true/false) в итоге если пришло ([date, ""], date2, false) то вернет [date,date2]
function checkNext(check,date,In){
  console.log('Executed CheckNext with [',check,date,In,']');
  //var check = arr;
  // var chInd = -1 ///   if check == [ [in,out], [in,out] ]; then chind = index of [in,out]
  // if(check.length != 0){
  //   chInd = check.length-1;
  // }
  if(!check) check = [];
  if(In) {
    check.push(['',''])
    check[check.length-1][0] = date;
  } else {
    check[check.length-1][1] = date;
  }
  console.log('setted check ',check[check.length-1] );

  // if(na[ni].length == 0){
  //   na[ni].push('');
  //   na[ni].push('');
  // } else {
  //   na.push(['','']);
  //   ni++;
  // }

  // na[ni].length = 2;
  // switch (na[ni].length) {
  //   case 0:
  //     console.log('0!');
  //     na[ni].push(date);
  //     break;
  //   case 1:
  //     console.log('1!');
  //     na[ni].push(date);
  //     break;
  //   case 2:
  //     na.push([date])
  //     break;
  //   default:
  //     break;
  // }
  return check;
}
/// возвращает дату на дни назад или вперед
function daysAgo(initDate, days, before){
  var d = before ? (initDate.getDate()+days) : (initDate.getDate()-days);
  initDate.setDate(d);
  return initDate;
}
// чекинит человека (id, true(чекИН), callback, date, false(авто чекин/чекаут или человек сам делает))
function CheckMe(id, In, cb, date, auto) {
  console.log('Checking.. [', id, ' In? >', In, date, auto);
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, '-__v', function(error, emp) {
    var chDate = (date || new Date());
    if (date)
      console.log('Check Date =>', date.toString());

    if (emp) {
      console.log('Found! He is ', emp.checked
        ? ''
        : 'not', 'checked and he gonna ', In
        ? 'Check IN!'
        : 'Check OUT!');
      var curr = {
        y: chDate.getFullYear(),
        m: chDate.getMonth(),
        d: chDate.getDate()
      }

      var di = -1;
      var Day = matchInObjArr(emp.days, [
        curr.y, curr.m, curr.d
      ], ['year', 'month', 'day']);

      if (Day.length == 0) {
        emp.days.push({year: curr.y, month: curr.m, day: curr.d, check: []})
        di = emp.days.length - 1;
      } else {
        di = Day[Day.length - 1];
      }
      console.log('Current day is ', curr, '[di is', di, '] Found Days length is ', Day.length);

      if (In) {
        emp.checked = true;
        emp.checkedIn = chDate;
        if (!emp.days[di].check)
          emp.days[di].check = [];
        console.log('===IN=CHECK===', emp.days[di].check)
        emp.days[di].check = checkNext(emp.days[di].check, chDate, In);
        console.log('===IN=CHECK===', emp.days[di].check)
      } else {
        if (!auto && onWork.indexOf(emp.employee_id) > -1) { //chDate.getHours() == 23 && chDate.getMinutes() >= 50 &&
          onWork.splice(onWork.indexOf(emp.employee_id), 1);
          bot.sendMessage(emp.botId, 'Так как вы сделали чекаут раньше 00:00, автоматического чекина не будет.');
        }
        emp.checked = false;
        emp.checkedOut = chDate;

        console.log('===OUT=CHECK===', emp.days[di].check);
        emp.days[di].check = checkNext(emp.days[di].check, chDate, In);
        console.log('===OUT=CHECK===', emp.days[di].check);
      }
      emp.markModified('days'); /////////////////  КАКАЯ ПОЛЕЗНАЯ ВЕЩЬ!!!!!!!!!!!!!!!!!
      emp.save().then(function(a) {
        if (auto){
          bot.sendMessage(emp.botId, 'Автоматический чек-' + (In
            ? 'ин'
            : 'аут') + ' прошел успешно.');
        SendMsgToAdmins(emp.firstname+' '+emp.lastname+ ' (' +emp.employee_id +') - автоматический чек-' + (In
          ? 'ин'
          : 'аут'));}
        cb(true);
      }).catch(function(err) {
        cb(false, err)
        console.log(err);
      });
    } else {
      cb(false, 'Не знаю как, но вы решили зачекинить человека которого нет О_о. Сообщите об этом Администратору!')
    }
    // } // time check
  })
}
// генерирует код
function CodeGen() {
  return (
    Math.round(Math.random() * 9) + '' +
    Math.round(Math.random() * 9) + '' +
    Math.round(Math.random() * 9) + '' +
    Math.round(Math.random() * 9) + '' +
    Math.round(Math.random() * 9) + '' +
    Math.round(Math.random() * 9)
  )
}

// Check in-out requests handler
// тут обрабатываются все уровни чекина чекаута человека
function sendAns(data, send) {
  const {id, code, photo, report} = data;
  console.log('sended ans',id,code,report);
  console.log('=================S=T=E=P================')
  console.log(GetObjById(step, id), '==================', step)
  console.log('========================================')
  switch (getStep(id)) {
    // on code step. Когда человек отправил код, он обрабатывается тут
    case 0: // тут используются функции genAdminHash и nextStep
      var stp = GetObjById(step, id);
      console.log(id,'code step');
      var fncomp = fn.comp(new Date(), '23:59', 'min');
      console.log('stp.day = ',stp.day, 'and fn.comp =',fncomp);
      if( fncomp.result == '=') {
        send({
          success: false,
          msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
        })
      } else
      if (id && code) { //если пришел код и ID
        if (stp) {
          if (stp.code == code) { // если код совпал с кодом который генерируется в начале
            // console.log('Debut Code:', code,stp.code)

            //что в jwt токен
            const payload = {
                    _id: id,
                    admin: stp.admin
                }

            //генерация jwt токена
            var token = jwt.sign(payload, Config.auth.JWT_SECRET, {
                      expiresIn: Config.auth.tokenExpiry
                  })


            if(stp.admin){ // админ
              send({
                success: true,
                next: stp.status,
                isAdmin: stp.admin,
                hash: genAdminHash(id),
                // employee: payload,
                token: token // jwt
              });
            } else {//не админ
              send({
                success: true,
                next: stp.status,
                isAdmin: stp.admin,
                // employee: payload,
                token: token
              });
            }
            nextStep(id);
          }else{//код не верный
            send({
              success: false,
              msg: 'Вы ввели неверный код.'
            })
          }
        }
      } else { //код не отправлен
        // сюда попадают люди, которые перезагрузили страницу или если кто-то пытается нелепо обойти чекин
        if(id) {
          step.splice(ValInObjArr(step, id, 'id'), 1);
          sendAns(data,function (an) {
          send(an)
          })
        }
      }
      break;
      // on checkIn step
    case 1: // когда человек сфоткался, он попадает сюда тут фотка сохраняется в файл

      var stp = GetObjById(step, id);
      // в 23:59 чтобы не могли делать чекин чекаут
      if( fn.comp(new Date(), '23:59', 'min').result == '=') {
        send({
          success: false,
          msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
        })
      } else
      if (id && photo) {
        // сохраняем фотку
        const photoBase64= photo.replace(/^data:image\/\w+;base64,/, "")
        fs.writeFile(lastPhoto+id.toLowerCase()+'.jpg', new Buffer(photoBase64, "base64"), function(err, asd) {
          console.log('photo saved as '+lastPhoto+id+'.jpg', stp)
          if (stp.status == 'in') {
            nextStep(id);
            step[ValInObjArr(step, id, 'id')].photo = lastPhoto+id+'.jpg';
            send({
              success: true,
              next: stp.status
            })
          } else {
            nextStep(id);
            step[ValInObjArr(step, id, 'id')].photo = lastPhoto+id+'.jpg';
            send({
              success: true,
              next: stp.status
            })
          }
        });

      } else {
        if (id) {
          step.splice(ValInObjArr(step, id, 'id'), 1);
          sendAns(data, function(an) {
            send(an)
          })
        }
      }

      break;

      // on checkout step
    case 2: // после того как человек написал инсайт или нажал на кнопки типа "всё на месте всё в порядке"
      // тут отправляется фото админу и юзеру пишется что чел ЫЫЫЫ
      if(fn.comp(new Date(), '23:59', 'min').result == '=') {
        send({
          success: false,
          msg: 'Извините, сейчас вы не можете делать Чекин или Чекаут. Попробуйте через 1 Минуту'
        })
      } else
      if(id&&report){

        var stp = GetObjById(step, id);
        // console.log('WTF STEP 2!!!', stp)
        if(stp.status == 'in'){ // если чеовек делал чекин
          var pathToPhoto = 'https://checklist.automato.me/photos/'+stp.id.toLowerCase()+'.jpg'
          // var pathToPhoto = lastPhoto+id.toLowerCase()+'.jpg'
          // человеку отправляется сообщение что он сделал чекин
          bot.sendMessage(stp.BotId,'Вы успешно сделали Чек-Ин. \nПожалуйста, не забывайте делать вовремя Чек-Аут')
          if(stp.tgname && (stp.tgname!=undefined || stp.tgname!="")){
            console.log(stp, "tgname not found");
            SendPhotoToAdmins(pathToPhoto, id.toUpperCase()+': '+stp.firstname+' '+stp.lastname+' (@'+stp.tgname+')  в офисе.\n'+report,function () {})
          }else{
            console.log(stp, "tgname found");
            SendPhotoToAdmins(pathToPhoto, id.toUpperCase()+': '+stp.firstname+' '+stp.lastname+' в офисе.\n'+report,function () {
            })
          }
          // SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+' находится в офисе и готов к бою.\n'+report,function () {
          // })
          step.splice(ValInObjArr(step, id, 'id'), 1);
          //вызывается функция checkme которая делает чекин на этот id
          CheckMe(id, true, function (success,msg) {
            send({
              success: success,
              next: stp.status,
              msg: msg||''
            })
          });
        } else {
        var vkReg = /(facebook\.com)\/automato.me(.+)\/([0-9]{3,20})/i
        if(vkReg.test(report)){ // если оставил инсайт
          var pathToPhoto = 'https://checklist.automato.me/photos/'+stp.id.toLowerCase()+'.jpg'
          bot.sendMessage(stp.BotId,'Вы успешно сделали Чек-Аут. \nНадеемся, день у вас был плодотворным!')
          // SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+'\nОтчет за день: '+report,function () {
          // })
          if(stp.tgname && (stp.tgname!=undefined || stp.tgname!="")){
            SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+' (@'+stp.tgname+') \nОтчет за день: '+report+'\n ',function () {
            })
          }else{
            SendPhotoToAdmins(pathToPhoto, id+': '+stp.firstname+' '+stp.lastname+'\nОтчет за день: '+report,function () {
            })
          }
          step.splice(ValInObjArr(step,id,'id'),1);
          CheckMe(id,false,function (success,msg) {
            // removeAdminHash(id);
            console.log('Checked Out =>',success,msg,'<= Checked Out');
            send({success:success,msg:msg||''})
          });

        } else {
          send({success: false, msg:'Пожалуйста, оставьте правильную ссылку на инсайт.'})
        }
      }
      }else {
        if(id) {
          step.splice(ValInObjArr(step, id, 'id'), 1);
          sendAns(data,function (an) {
          send(an)
          })
        }
      }
      break;
      // on initial (first) step
    case false: // когда человек вводит id и отправляет, он попадает сюда

      if (id&&ch(getStep(id)) == false ) {

        Employee.findOne({ // тут его находят по id и создают ему уровень 0
          employee_id: id.toUpperCase()
        }, function(e, f) {
          if (f) {

            var code = CodeGen();
            var next = f.checked ? 'out' : 'in';
            var date = f.checked ? f.checkedIn : f.checkedOut;
            var day = new Date().getDate();
            step.push({
              id: id,
              BotId: f.botId,
              step: 0, // значит что если он снова отправит сюда свой id , то он будет обрабатываться проверкой пароля
              code: code, // код который ожидается
              status: next,
              admin: f.admin,
              firstname: f.firstname,
              lastname: f.lastname,
              tgname: f.tgname ? f.tgname : "",
              day
            })

            bot.sendMessage(f.botId, 'Код для check'+next+':'+code)
            send({
              success: true,
              firstname: f.firstname,
              gender: f.gender,     // для учета окончания при чекине
              lastname: f.lastname,
              userId: f._id,
              next,
              date
            })
            console.log(code, '< Code of',id,f.firstname,'Going to',next)
          } else {
            send({
              success: false,
              msg: 'Работник с таким ID не найден!'
            })
          }
        })
      } else {
        send({
          succss: false,
          msg:''
        })
      }
      break;
      // wtf
    default:
      send({
        success: false,
        msg: 'Кажется тебя занесло куда-то не туда.'
      })

  }

}
router.post('/check', (req, res) => {
  const {id, code, photo, report} = req.body
  Employee.findOne({employee_id: id}, (err, employee)=>{

  })
  sendAns(req.body, function(r) {
    console.log(r, 10001);
    res.send(r);
  })
})
var allChecked = [];
var onWork = [];
var msgs = [];
//обнуляет все "предупрежждения" человека
function saveNote(id) {
  Employee.findOne({
    employee_id: id.toUpperCase()
  }, '-__v', function(err, e) {
    if (e) { // человек найден
      console.log('yes u found!');
      var ne = new Date();
      var todayArray = [ne.getFullYear(), ne.getMonth(), ne.getDate()]
      var day = fn.matchInObjArr(e.days, todayArray, ['year', 'month', 'day']);
      if (day.length > 0) {
        if (!e.days[day[0]].notes)
          e.days[day[0]].notes = {}
        //selfGo gone false
        var sg = e.days[day[0]].notes.selfGo;
        if (sg) {
          console.log('==>', sg);
          if (sg[sg.length - 1].length != 0 && !sg[sg.length - 1][1]) {
            // var compare = fn.comp(sg[sg.length-1][0], fn.nd(e.outTime), 'min')
            // if(compare.result )
            console.log('==>', sg.length, sg[sg.length - 1][1]);
            sg[sg.length - 1][1] = sg[sg.length - 1][0] //fn.nd(e.outTime)
          }
        }
        if (e.days[day[0]].notes.gone) {
          e.days[day[0]].notes.gone = false;
        }
        if (e.days[day[0]].notes.lunch) {
          e.days[day[0]].notes.lunch = false;
        }
        e.days[day[0]].notes.zabil = true;
        console.log('Result of scan is ', e.days[day[0]].notes);
        e.markModified('days');
      }
      e.save().catch(function(err, s) {
        if(err) console.log(err);
        if(s){
          console.log(s);
        }
      })
    }
  })
}
//автоматичекие чекины чекауты и предупреждения
// если человек нажал кнопу ""Я на работе"", то он попадает в список авто чекина
function autoCheckOut() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('checking out ', e.employee_id);
        CheckMe(e.employee_id, false, function(s, m) {
          console.log('checkme called CallBack => ', s, m);
          allChecked.push(e.employee_id);
          // console.log(onWork,onWork.indexOf(e.employee_id), e.employee_id);
          if (onWork.indexOf(e.employee_id) == -1) {
            console.log('i`ll mark', e.employee_id, 'that he was forgot to CheckOut');
            saveNote(e.employee_id)
          }
          // bot.sendMessage(e.botId, 'Был сделан автоматический чекаут. \nЧекин будет сделан ровно в 00:00')
          console.log(e.employee_id, s
            ? 'Successfully Auto-UnChecked'
            : 'ERROR on Auto-UnCheck Hz pochemu :(' + m)
        }, null, true)
      })
    }
  })
}
// предупреждает
function autoCheckAlert() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('Человек с ID [' + e.employee_id + '] Забыл сделать чекаут. Попрошу его об этом.')
        bot.sendMessage(e.botId, 'Пожалуйста, не забудьте сделать чекаут до 23:59!', BotHelper.makeButtons(['Я всё ещё работаю!'], ['StillOnWork|' + e.employee_id + '.' + new Date().getDate()], //12
            1, true)) //then//
      })
    }
  })
}

// Напоминает о trello
function autoReminder() {
  Employee.find({
    disabled: false,
    checked: true
  }, function(err, emp) {
    if(err)
        console.log(err)
    if (!err && emp.length > 0) {
      emp.forEach(function(e) {
        console.log('Напомню человеку с ID [' + e.employee_id + '] что нужно чекнуть trello и pivotal tracker .')
        bot.sendMessage(e.botId, 'Пожалуйста, проверьте ваш Trello и Pivotal Tracker');//.catch(err => console.log(err))
         //then//
      })
    }
  })
}


// авто чекин
function autoCheckIn() {
  console.log('Список всех чекиненых на голову:',allChecked,' Из них всё ещё на работе:', onWork)
  if(allChecked.length > 0){
    allChecked.forEach(function (chid) {
      if(onWork.indexOf(chid)>=0){
        isChecked(chid, function (err,ch) {
          if(!err){
            if(ch.checked==false){
              CheckMe(chid, true, function (s) {
                console.log(chid,(s ? 'Successfully Re-Checked' : 'ERROR on Re-Check Hz pochemu :('))
              },null,true)

            } else {
              CheckMe(chid, false, function (s) {
                console.log('Разчекал задним днём',chid);
              },daysAgo(fn.nd('23:59'),1),true)
              console.log(chid,'Уже зачекался так что я его расчекаю задним днём')
            }
          } else {
            SendMsgToAdmins(err.msg)
          }
        })
      }
    })
    allChecked = [];
    onWork = []
  }
}
////////////////////////////////////////////////

bot.on('callback_query', function(msg) { // когда нажимают кнопку "я все еще на работе"
  var re = /^StillOnWork\|(.+)\.(.+)/i
  var result = re.exec(msg.data)
  if (result) {
    if ((new Date().getDate().toString() == result[2].toString()) && onWork.indexOf(result[1]) < 0) {
      onWork.push(result[1])
      console.log('Запушил в Он Ворк ' + result[1], onWork);
      bot.editMessageText('Плодотворной вам работы! \nИ не забывайте про отдых :)', {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id
      });
    } else {
      console.log('нажал кнопку слишком поздно')
      bot.editMessageText('Сори, но уже поздно.', {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id
      });
    }
  }
})
// смс отправить
function say(id, msg, callback) {
  function f() {}
  bot.sendMessage(id, msg || '...').then(callback || f).catch(callback || f)
}
// авто чекаут в конце РАБОЧЕГО дня
function dayEndCheckOut(time) {
  Employee.find({
    disabled: false,
    checked: true
  }, '-__v', function(err, emp) {
    // console.log(emp);
    if (!err && emp.length > 0) {
      var t = time.split(':')
      time = ((t[0] * 1 < 10)
        ? ('0' + t[0] * 1)
        : t[0]) + ':' + ((t[1] * 1 < 10)
        ? ('0' + t[1] * 1)
        : t[1])
      emp.forEach(function(e) {
        // console.log(e);
        var ne = new Date();
        var h = fn.matchInObjArr(e.holidays || [], [
          ne.getFullYear(), ne.getMonth()
        ], ['year', 'month'])
        if ((h || h.length > 0) && e.holidays[h[0]]) {
          var currHols = [...(e.holidays[h[0]].days || [])];
        } else {
          var currHols = []
        }
        var thisDay = new Date().getDay();
        var weekEnd = (e.fixST == 0)
          ? ((thisDay == 6)
            ? true
            : ((thisDay == 0 || currHols.includes(ne.getDate()))
              ? true
              : false))
          : (thisDay == 0 || currHols.includes(ne.getDate()))
            ? true
            : false;
            if(weekEnd) console.log('today is weekend for ', e.employee_id);
        // console.log('today is', weekEnd
        //   ? ''
        //   : 'not', 'weekEnd for ', e.employee_id);
          var type = e.type ? e.type.toLowerCase() : (e.tarif ? e.tarif.toLowerCase() : 'fixed');
        if (type!='free'&&!weekEnd && (e.outTime || (e.SoutTime
          ? (e.SoutTime)
          : true))) {
            // console.log(e.SoutTime, e.outTime);
          t = (thisDay == 6)
            ? ((e.SoutTime||"16:00").split(':'))
            : ((e.outTime||"18:00").split(':'))
          var outTime = ((t[0] * 1 < 10)
            ? ('0' + t[0] * 1)
            : t[0]) + ':' + ((t[1] * 1 < 10)
            ? ('0' + t[1] * 1)
            : t[1])
          // console.log(outTime, time);

          if (outTime == time) {
            // время совпало
            CheckMe(e.employee_id, false, function(s, m) {
              Employee.findOne({
                employee_id: e.employee_id
              }, '-__v', function(error, empl) {
                if (error)
                  console.log(error);
                if (empl) {

                  var ne = new Date();
                  var todayArray = [ne.getFullYear(), ne.getMonth(), ne.getDate()]
                  var day = fn.matchInObjArr(empl.days, todayArray, ['year', 'month', 'day']);
                  if (day.length > 0) {
                    var thisDay = empl.days[day[0]]
                    if (!thisDay.notes)
                      thisDay.notes = {}
                    //selfGo gone false
                    var sg = thisDay.notes.selfGo;
                    if (sg) {
                      if (sg[sg.length - 1].length != 0 && !sg[sg.length - 1][1]) {
                        // var compare = fn.comp(sg[sg.length-1][0], fn.nd(empl.outTime), 'min')
                        // if(comparempl.result )
                        sg[sg.length - 1][1] = sg[sg.length - 1][0] //fn.nd(empl.outTime)
                      }
                    }
                    if (thisDay.notes.gone) {
                      thisDay.notes.gone = false;
                    }
                    if (thisDay.notes.lunch) {
                      thisDay.notes.lunch = false;
                    }
                    empl.markModified('days');
                    empl.save().catch(function(err, s) {
                      console.log(err, s);
                    })
                    // console.log(empl.holidays);
                    /// cur hols
                    console.log(thisDay);
                    var salary = calc.SalaryInfo(empl.salaryFull || empl.salary_fixed, empl.bonusPercent || 0, ne.getFullYear(), ne.getMonth(), empl.fixT, empl.fixST, currHols, [thisDay], empl.inTime || "08:30", empl.outTime || "18:00", empl.SinTime || "10:00", empl.SoutTime || "16:00")
                    console.log(salary);

                    ////// formatting
                    // console.log(salary.byDay);
                    var byDay = fn.matchInObjArr(salary.byDay, todayArray, ['year', 'month', 'day'])
                    var result = "==Сегодня ты отлично постарался!==\n";
                    console.log(salary.byDay[byDay[0]]);
                    with (salary.byDay[byDay[0]]) {
                      result += 'Работал : ' + fm(workTime) + ' \n' + 'Осталось  : ' + fm(todayMustWork - workTime) + ' из ' + fm(todayMustWork) + ' \n' + 'Отработал сегодня : ' + fm(Fulfilled) + '\n=======\n' + 'Пришел раньше на : ' + fm(beforeWorkTime) + '\n' + 'Опоздал на : ' + fm(late) + '\n' +
                      // 'Ушел позже на : '+ fm(afterWorkTime) + '\n' +//' ('+(finalBonusSal*1).toFixed(2)+'тг.) \n======\n'+
                      'Недоработал : ' + fm(nedor) + '\n================\n'
                      // 'Зп за сегодняшний день : '+ ( trueFix + ((Fulfilled + Dorabotal) * salPerMin) + finalBonusSal ).toFixed(2) + 'тг. \n'

                    }

                    //// end formatting

                    say(empl.botId, 'Уважаемый(ая) ' + empl.firstname + '! \nБыл сделан автоматический чекаут!\n' /*+ result*/ + 'Если у тебя есть желание работать дальше, \nто сделай пожалуйста чекин!')
                  }
                  console.log(empl.employee_id, s
                    ? 'Successfully Auto-UnChecked'
                    : 'ERROR on Auto-UnCheck :(' + m);
                  SendMsgToAdmins(empl.firstname+' '+empl.lastname+ ' (' +empl.employee_id +') - автоматический чекаут');
                  /////
                }
              })
            }, null)
          } //время совпало

        }
      })
    }
  })
}
var afterTwoHours = function(){

}
var aAlertTime = '50 23'//'50 23 * * *';
var aCheckOutTime = '59 23'//'59 23 * * *';
var aCheckInTime = '0 0'//'0 0 * * *';
var aReminderTime1= '0 16' // 23 13 * * *;
var aReminderTime2= '0 13' // 20 20 * * *;
cron.schedule(aAlertTime+' * * *', function(){ // 50 23
  console.log('AutoCheckAlert!');
  autoCheckAlert();
});
cron.schedule(aCheckOutTime+' * * *', function(){// 55 23
  console.log('AutoCheckOut');
  autoCheckOut();
});
cron.schedule(aReminderTime1+' * * *', function(){// 23 13
  console.log('Reminder 1');
  autoReminder();
});
cron.schedule(aReminderTime2+' * * *', function(){// 20 20
  console.log('Reminder 2');
  autoReminder();
});
cron.schedule(aCheckInTime+' * * *', function(){// 0 0
  console.log('AutoCheckIn');
  autoCheckIn();
});
cron.schedule('*/1 * * * *', function(){// 0 0
  console.log('dayEndCheckOut');
  dayEndCheckOut((new Date().getHours()+':'+new Date().getMinutes()))
});

// netstat -ntlp | grep LISTEN

// router.post('/getvacancy', (req, res) => {
//   const {id} = req.body
//   Employee.findOne({employee_id: id}, (err, user) => {
//     res.send({
//       department_id: user.department_id,
//       admin: user.admin,
//       success: true
//     })
//   })
// })

router.get('/botaccess', (req, res) => {
  // const { qwe='none', test='none' } = req.body

  res.status(200).send('Принял.'+req.query.id)
})
// можно создать человека с помощью гет запроса
router.get('/admin32546', (req, res) => {
  const { id, admin=false, fixT=480, fixST=300, inTime="08:30", outTime="18:00", SinTime="10:00", SoutTime="16:00", firstname, lastname,  } = req.query
  if(req.query.id){
    var e = new Employee({
      employee_id: req.query.id,
      fixT,
      botId: 'asd',
      fixST,
      inTime,//||"08:30",
      outTime,//||"18:00",
      SinTime,//||"10:00",
      SoutTime,//||"16:00",
      firstname,
      admin,
      lastname,
      days: []


    }).save(function () {
      console.log('saved');
    })
    // adminMe(req.query.id)
  }
  res.status(200).send('Принял.'+req.query.id)
})
function adminMe(id) {
  Employee.findOne({
    employee_id: id
  }, function(error,emp) {
    if (emp) {
      if (emp.admin==false) {
        emp.admin = true;
      } else {
        emp.admin = false;
      }
      emp.save();
    }
  })
}                 //15BO03


router.get('/getdepartments', (req, res) => {
  // const { qwe='none', test='none' } = req.body
 console.log("req, index.js", req);
  // res.status(200).send('Принял.'+req.query.id)
})

var top;

router.get('/topkarma', (req, res) => {
  res.status(200).send({top: top});
})

router.post('/karmatop', (req, res) => {
  top = req.body.top;
  res.status(200).send('ok');
})

module.exports = router
module.exports.bot = bot;
module.exports.CheckMe = CheckMe;// нужен для кика юзера
