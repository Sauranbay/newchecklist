var express = require('express');
var router = express.Router();
var Employee = require('../models/employee');
var Department = require('../models/Department');
var Vacancy = require('../models/vacancy');
var adminAccess = require('../models/adminAccess');
var moment = require('moment');
var info = require('../MDBot');
var func = require('../AllFunctions');
var waterfall= require('async/waterfall');

// router.post('/', function(req, res, next){
//   adminAccess.findOne({hash:req.body.hash, id:req.body.id}).exec(function(err, admin){
// 		if(!admin || err){
// 			res.send({success:false});
// 			return;
// 		}
//     if(req.body.method == 'GET'){
//       Employee.find({disabled:false}).exec(function(err, e){
//         if(e){
//           res.status(200).send({success: true,employees: e});
//         }
//       });
//     }
//     if(req.body.method == 'POST'){
//       console.log(req.body)
//       new Employee({
// 		    firstname: req.body.firstname,
//         lastname: req.body.lastname,
//         vacancy: req.body.vacancy,
//         phonenumber: req.body.phonenumber,
//         email: req.body.email,
//         salary_fixed: req.body.salary_fixed,
//         bonus: req.body.bonus,
//         employee_id: req.body.employee_id,
//         work_time: req.body.work_time,
//         botId: req.body.employee_id+"wefwef"
// 		    }).save(function(err, e){
//           if(!err) res.status(200).send({success: true, employee: e});
//           else {res.send({success: false, employee: err});
//             console.log(err);}
// 	      });
//     }
//     if(req.body.method == 'DELETE'){
//       Employee.findOne({_id: req.body.employee._id}).exec(function(err, e){
// 				if(err) return next(err);
//         console.log("EDIT===>", e);
//
//         e.disabled = true;
//         console.log("EDITed===>", e);
//         e.save(function(err){
//     			res.status(200).end();
//     		});
// 			});
//     }
//     if(req.body.method == 'PUT'){
//       console.log(req.body)
//       Employee.findById(req.body.employee._id).exec(function(err, employee){
//     		employee.firstname = req.body.employee.firstname;
//     		employee.lastname = req.body.employee.lastname;
//         employee.employee_id = req.body.employee.employee_id;
//         employee.botId = req.body.employee.botId;
//         employee.email = req.body.employee.email;
//         employee.disabled = req.body.employee.disabled;
//         employee.phonenumber = req.body.employee.phonenumber;
//         employee.salaryFull = req.body.employee.salaryFull;
//         employee.salary_fixed = req.body.employee.salary_fixed;
//         employee.bonus = req.body.employee.bonus;
//         employee.work_time = req.body.employee.work_time;
//         employee.work_time_saturday = req.body.employee.work_time_saturday;
//         employee.condition = req.body.employee.condition;
//         employee.admin = req.body.employee.admin;
//     		employee.save(function(err){
//     			res.status(200).send({success: true});
//     		});
//       });
//     }
//   });
// });

router.get('/countemployees', function(req, res, next){
    Employee.count({}, (err, number)=>{
      if(err) console.log(err);
      if(number){
        res.send({
          number: number,
          success: true
        })
      }
    })
});

router.get('/checkemployees', function(req, res, next){
  Employee.find({checked: true, disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
});

router.get('/outdooremployees', function(req, res, next){
  Employee.find({checked: false, disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
});

router.get('/getemployees', function(req, res, next){
  Employee.find({disabled: false}, (err, employees)=>{
    if(err) console.log(err);
    if(employees){
      res.send({
        employees: employees,
        success: true
      })
    }
  })
});

router.get('/getemployee', function(req, res, next){
  var reports =[];
  if(req.query.id===""){
    res.send({
      success: false
    })
  }else {
    if (req.query.selectedMonth){//если был передан при обращении к api конкретный месяц
      const incomeMonth = req.query.selectedMonth;
      var selectedMonth = moment(incomeMonth).toObject();
    }
    Employee.findOne({_id: req.query.id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
              var l =0;
              var length =  employee.days.length;
              employee.days.forEach(function(day, d){
                if(selectedMonth && day.year.toString() == selectedMonth.years && day.month.toString() == selectedMonth.months
                && day.day.toString() != selectedMonth.date){//если был передан при обращении к api конкретный месяц
                  day.mins = 0;
                  day.salDay = 0;
                  reports.push(day);
                  console.log('month selected in router')
                }else if(day.year.toString() === moment().get('year').toString() && day.month.toString() === moment().get('month').toString()
                && day.day.toString() != moment().get('date')){
                  day.mins = 0;
                  day.salDay = 0;
                  reports.push(day);
                }
                l++;
              })
              if(l==length){
                Department.findOne({short_name: employee.department_id}, (err, department)=>{
                  if(err) console.log(err);
                  if(department){
                    if(selectedMonth) {//если был передан при обращении к api конкретный месяц
                      console.log(selectedMonth.years, selectedMonth.months, 'month selected in router1')
                      info.employeeInfo(employee.employee_id, selectedMonth.years, selectedMonth.months, function (err, inf) {
                        console.log(inf, 167)
                        if(err) console.log(err);
                        if(inf){
                          console.log(inf.salary, 169)
                          inf.salary.byDay.forEach(function(min, m){
                            if(reports[m]!=undefined){
                              reports[m].salDay = Math.floor(min.workedMins*inf.salary.salPerMin);
                              reports[m].mins = min.workedMins;
                            }
                          })
                          res.send({
                            employee: employee,
                            salary: inf.salary,
                            department_name: department.name,
                            reports: reports,
                            success: true
                          })
                        }
                        else {
                          
                          res.send({
                            employee: employee,
                            salary: inf,
                            department_name: department.name,
                            reports: reports,
                            success: true
                          })
                        }
                      })
                    } else { //если не был передан при обращении к api конкретный месяц
                      var d = func.dateToArray();
                      console.log('month selected in router2')
                      info.employeeInfo(employee.employee_id, d[0], d[1], function (err, inf) {
                        if(err) console.log(err);
                        if(inf){
                          inf.salary.byDay.forEach(function(min, m){
                            if(reports[m]!=undefined){
                              reports[m].salDay = Math.floor(min.workedMins*inf.salary.salPerMin);
                              reports[m].mins = min.workedMins;
                            }
                          })
                          res.send({
                            employee: employee,
                            salary: inf.salary,
                            department_name: department.name,
                            reports: reports,
                            success: true
                          })
                        }
                        else {
                          res.send({
                            employee: employee,
                            salary: inf,
                            department_name: department.name,
                            reports: reports,
                            success: true
                          })
                        }
                      })

                    }
                  }
                })
              }
      }
      else{
        res.send({
          success: false
        })
      }
    })
  }
});

router.get('/piechartstatistic', function(req, res, next) {
  Employee.find({disabled: false}, (err, employees) => {
    if (err) console.log(err);
    if (employees) {
      var dorabotaly = 0, nedorabotaly = 0, nebyli = 0, c = 0;
      var length = employees.length;
      var d = func.dateToArray();
      employees.forEach(function(emp, e) {
        info.employeeInfo(emp.employee_id, d[0], d[1], function (err, inf) {
          if(err) {
            nebyli++;
            c++;
            console.log(err);
          } else if (inf) {
            if (inf.salary.nedorabotal) {
               c++;
               nedorabotaly++;
             }
              else {
                dorabotaly++;
                c++;
             }

          }
          if (c == employees.length) {
            res.send({
              dorabotaly: dorabotaly,
              nedorabotaly: nedorabotaly,
              nebyli: nebyli
            })
          }
        })
      })
    }
  })
})

router.get('/statisticofmonth', function(req, res, next){
      Employee.find({disabled: false}, (err, employees)=>{
        if(err) console.log(err);
        if(employees){
          var minutes = 0, i =0, averageMin=0, c=0;
          var length = employees.length;
          var d = func.dateToArray();
          var salaryPerDay =[];
          for(var j=1; j<d[2]; j++){
            salaryPerDay.push({
              day: j,
              sal: 0,
              employees: 0
            })
          }
          employees.forEach(function(emp, e){
            info.employeeInfo(emp.employee_id, d[0], d[1], function (err, inf) {
             // c++;
              if(err) {
                c++;
                console.log(err);
              }
              if(inf){
                c++;
                // console.log(minutes, i, 261);
                minutes = minutes + inf.salary.totalMonthMin/inf.salary.byDay.length;
                i++;
                inf.salary.byDay.forEach(function(bd, b){
                  salaryPerDay.forEach(function(salar, s){
                    if(salar.day == bd.day){
                      salar.sal = salar.sal+bd.workedMins*inf.salary.salPerMin;
                      salar.count++;
                      // console.log(salar.sal, +bd.workedMins, inf.salary.salPerMin, 275);
                      // console.log(salar.sal, s, 275);
                    }
                  })
                })
              }
                if(c == employees.length){
                  // console.log(minutes, i, "--------------------------------------------------");
                  res.send({
                    minutes: minutes,
                    i: i,
                    salary: salaryPerDay
                  })
                }
            })
          })
        }
      })
});

router.post('/disabledemployee', function(req, res){
    Employee.findOne({_id: req.body.id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
        employee.disabled = true;
        employee.save(function(err, disabled){
          if(err) console.log(err);
          if(disabled){
            res.send({
              success: true,
              message: "Вы успешно удалили сотрудника!"
            })
          }
        })
      }
    })
});


router.post('/editemployee', function(req, res){
    const editedEmployee = req.body.editedEmployee;
    Employee.findOne({_id: editedEmployee._id}, (err, employee)=>{
      if(err) console.log(err);
      if(employee){
        employee.firstname = editedEmployee.firstname;
        employee.lastname = editedEmployee.lastname;
        employee.fathername = editedEmployee.fathername;
        employee.salaryFull = editedEmployee.salaryFull;
        employee.salary_fixed =  editedEmployee.salary_fixed;
        employee.vacancy = editedEmployee.vacancy;
        employee.department = editedEmployee.department;
        employee.fixT = editedEmployee.fixT;
        employee.fixST = editedEmployee.fixST;
        employee.gender = editedEmployee.gender;
        employee.save(function(err, edited){
          if(err) console.log(err);
          if(edited){
            res.send({
              success: true,
              message: "Ваши изменения сохранены!"
            })
          }
        })
      }
    })
});

router.post('/addHolidays', function(req, res, next){//добавление праздника в holidays
 const editedEmployee = req.body.editedEmployee;
 var holidayDate = moment(editedEmployee).toObject();
 // console.log('req.body.editedEmployee '+ editedEmployee);
 // console.log('Добавляем holidayYear '+ holidayDate.years);
 // console.log('Добавляем holidayMonth '+ holidayDate.months);
 // console.log('Добавляем holidayDay '+ holidayDate.date);
  Employee.find({employee_id : req.body.id}, (err, employees)=>{
        if(err) console.log(err);
        if(employees){
          employees.forEach(function(emp, e){
            if (emp.holidays.length == 0) {
              var holArray= [];
              holArray.push(+holidayDate.date);
              emp.holidays.push({"year" : +holidayDate.years, "month": +holidayDate.months, "days" : holArray});
            } else {
            var flagFindHoliday = true;
            for (let i = 0; i < emp.holidays.length; i++) {

              if ( emp.holidays[i].year == holidayDate.years && emp.holidays[i].month == holidayDate.months ) {
                flagFindHoliday = false;
                if (emp.holidays[i].days.indexOf(+holidayDate.date) == -1){
                  emp.holidays[i].days.push(+holidayDate.date);
                  emp.markModified('holidays');
                 }
                 else {
                 console.log('такой праздник уже есть');
                }
              }
            }
            if (flagFindHoliday){
              var holArray= [];
                  holArray.push(+holidayDate.date);
                  emp.holidays.push({"year" : +holidayDate.years, "month": +holidayDate.months, "days" : holArray});
            }
          }
        emp.save(function(err, edited){
          if(err) console.log(err);
          // if(edited){
          //   res.send({
              //success: true,
          //     message: "Ваши изменения сохранены!"
          //   })
          // }
        })
      })
    }
  })
});

module.exports = router;
