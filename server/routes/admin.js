var express = require('express');
var router = express.Router();
var Employee = require('../models/employee');
var adminAccess = require('../models/adminAccess');

router.use('/api/department', require('./department'));
router.use('/api/holiday', require('./holiday'));
router.use('/api/employees', require('./employees'));

// import axios from 'axios'
const { verifyJWT_MW } = require('../middlewares/admin.mw')

router.post('/api/admin',
	verifyJWT_MW,
	function(req, res, next){
	// console.log(req.body);
  adminAccess.findOne({id:req.body.id}).exec(function(err, admin){
		if(!admin || err){
			res.send({success:false});
			return;
		}
		var a = {
			success: true,
			info: admin
		};
		res.status(200).send(a);
	});

});

module.exports = router;
