var express = require('express');
var router = express.Router();
var Employee = require('../models/employee');
var Department = require('../models/Department');
var Vacancy = require('../models/vacancy');
var adminAccess = require('../models/adminAccess');
//
// router.post('/', function(req, res, next){
//   console.log("deps OPENED, 9");
//   console.log(req.body);
//   adminAccess.findOne({hash:req.body.hash, id:req.body.id}).exec(function(err, admin){
// 		if(!admin || err){
// 			res.send({success:false});
// 			return;
// 		}
//
//     if(req.body.method == 'POST'){
//       new Department({
// 		    name: req.body.dep
// 		    }).save(function(err, e){
// 			    res.status(200).send({success: true, dep: e});
// 	      });
//     }
//     if(req.body.method == 'DELETE'){
//       Department.remove({_id: req.body.dep._id}).exec(function(err, e){
// 				if(err) return next(err);
// 				res.status(200).end();
// 			});
//     }
// 	});
// });

router.get('/departments', function(req, res, next){
  Department.find({}, (err, departments)=>{
    if(err) console.log(err);
    else{
      res.send({
        departments: departments,
        success: true
      })
    }
  })
});

router.get('/countdepartments', function(req, res, next){
  Department.count({}, (err, departments)=>{
    if(err) console.log(err);
    else{
      res.send({
        number: departments,
        success: true
      })
    }
  })
});


router.post('/adddepartment', function(req, res, next){
  new Department({
    name: req.body.department_name,
    short_name: req.body.short_name
  }).save(function(err, savedDepartment){
    if(err) {
      console.log(err);
      res.send({
        success: false
      })}
    else{
      res.send({
        success: true
      })
    }
    if (savedDepartment) {
      console.log(savedDepartment);
    }
  });
});


router.get('/editdepartments', function(req, res, next){
  Department.find({}, (err, departments)=>{
    if(err) console.log(err);
    else{
      departments.forEach(function(department, d){

      })
      res.send({
        number: departments,
        success: true
      })
    }
  })
});

router.post('/deletedepartments', function(req, res){
   Department.find({short_name: req.body.short_name}, (err, departments)=>{
    if(err) console.log(err);
    else{
      var flagDeleteDepartment = false;
      var depsWithEmployee = 0;
      foundDeps = [];
      departments.forEach(function(department, d){//сначала проверяем есть ли отмеченные отделы с сотрудниками внутри
        if (department.employees.length === 0){
          // department.remove({_id: department._id});
          foundDeps.push(department._id);
          flagDeleteDepartment = true;          
        } else {
          console.log("Выбран отдел где есть сотрудники!");
          depsWithEmployee++;//увеличиваем счетчик если сотрудники в каком-то отделе есть 
          flagDeleteDepartment = false;
        }
      });
      if (flagDeleteDepartment && depsWithEmployee === 0 ) {
        for (let i = 0; i < foundDeps.length; i++) {
          departments.forEach(function(department, d){//удаляем отдел только если не были отмечены отделы с сотрудниками
            department.remove({_id: foundDeps[i]});
          })
        }
        res.send({
          success: true
        })
      } else {
        res.send({
          success: false
        })
      }      
    }
  })
});

module.exports = router;
