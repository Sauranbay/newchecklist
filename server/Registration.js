var BotHelper = require('./BotFunctions');
var que = [
  {index: 50,
    data: 'Здравствуйте! Пожалуйста, введите ваше Имя:',
    options: {},
    answers:[
      {
        AnsType: 'text',
        reg:/(.*)/i
      }
    ],
    qType: 'text',
  }, //имя
  {index: 51,
    data: 'Введите теперь, пожалуйста, вашу Фамилию:',
    options: BotHelper.makeButtons(['Назад'],['back'],1,true),
    answers:[
      {
        AnsType: 'text'
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 50
      }
    ],
    qType: 'text'
  },
  {index: 52,
    data: 'И ваше Отчество:',
    options: BotHelper.makeButtons(['Назад'],['back'],1,true),
    answers:[
      {
        AnsType: 'text'
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 51
      }
    ],
    qType: 'text'
  },
  {index: 53,
    data: '',
    options: {},
    answers:[
      {
        AnsType: 'inline',
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 52
      }
    ],
    qType: 'text'
  },
  {index: 54,
    data: '',
    options: BotHelper.makeButtons(['Назад'],['back'],1,true),
    answers:[
      {
        AnsType: 'text'
        // next: 57
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 53
      }
    ],
    qType: 'text'
  },
  {index: 55,
    data: 'С какого года вы работаете в этой компании? Напишите, пожалуйста, точный полный год \n Например: 2017 или 17 ',
    options: BotHelper.makeButtons(['Назад'],['back'],1,true),
    answers:[
      {
        AnsType: 'text',
        reg: /^([0-9]{2,4})$/,
        next: 57
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 54
      }
    ],
    qType: 'text'
  },
  {index: 57,
    data: '',
    options: {},
    answers:[
      {
        AnsType: 'inline',
        next: 56
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 55
      }
    ],
    qType: 'text'
  },
  {index: 56,
    data: 'Напишите вашу официальную заработную плату',
    options: BotHelper.makeButtons(['Назад'],['back'],1,true),
    answers:[
      {
        AnsType: 'text'
        // reg: /^([0-9])$/
      },
      {
        AnsType: 'inline',
        reg: /^back$/i,
        next: 56
      }
    ],
    qType: 'text'
  }
  // ...require('./UserMenu'),

]

module.exports = que;
