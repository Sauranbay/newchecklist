var Hall = require('../models/hall.js')
var Schedule = require('../models/schedule.js')
var Weather = require('../models/weather.js')
var Currency = require('../models/currency.js')
var Ideas = require('../models/Idea.js')

var DamFunc = require('../AllFunctions')

// Morning hour for creating buttons

var morning = [
	'8:00',
  '9:00',
  '10:00',
  '11:00',
  '12:00',
  '13:00',
	'14:00'
]

// Evening hours for creating buttons

var evening = [
  '15:00',
  '16:00',
  '17:00',
  '18:00'
]


var sendSchedule = function (day, hallId, other, callBack){
 var today = 0;
 var result = "Кстати, не забудьте проверить расписание бронирования:\n";
   switch(day){
     case 'today':
       today = new Date().setHours(0, 0, 0, 0)
       break
     case 'tomorrow':
       today = new Date().setHours(0, 0, 0, 0) + 86400000
       break
     case 'aftertomorrow':
       today = new Date().setHours(0, 0, 0, 0) + 86400000*2
       break
     case 'other':
       var array = other.split('.');
       today = new Date(array[2], array[1]*1-1, array[0]*1).getTime();
       break
   }
 Schedule.find({hallId: hallId, date: today}).populate('user hallId').exec((err, schedules)=>{
   if(err) console.log(err);
   else{
     if(schedules.length==0){
       result="На этот день зал свободен";
       callBack(result)
     }else {
       schedules.forEach(function(item, i){
         result+=item.stringFrom+"-"+item.stringFor+" "+item.user.firstname + " "+item.user.lastname+"\n";
       })
			 callBack(result)
     }
   }
 })
}


function getButtons(_hall_id, _period, when, callback) {
  switch (when) {
    case 'today':
      var date = DamFunc.dateToArray(new Date())
      break;
    case 'tomorrow':
      var date = DamFunc.dateToArray(DamFunc.Ago('days', 1))
      break;
    case 'aftertomorrow':
      var date = DamFunc.dateToArray(DamFunc.Ago('days', 2))
      break;
		case 'other':
      var date = DamFunc.dateToArray(DamFunc.Ago('days', 2))
      break;
    default:

  }
  Schedule.find({
    hallId: _hall_id
  }, function(err, halls) {
    if (err) console.log(err)
    if (halls) {
    	if(when == 'today'){
    		var todayDate = new Date().getHours()
    		var timeButtons = (_period == 'morning') ? morning : evening
      	var buttons = timeButtons.filter(function (butts){
      		if(Number(butts.split(':')[0]) > todayDate){
      			return butts
      		}
      	})
      	var finalButtons = []
      	buttons.map(function(time) {
        var allowed = true
        halls.map(function(hall) {
          if (chk(new Date(hall.timeFrom), new Date(hall.timeFor), DamFunc.nd(date[0], date[1], date[2], time))) {
            allowed = false
          }
        })
        if (allowed) {
          finalButtons.push(time)
        }
      })
      callback(finalButtons)
    	} else{
				var buttons = (_period == 'morning') ? morning : evening
	      var finalButtons = []
	      buttons.map(function(time) {
        var allowed = true
        halls.map(function(hall) {
          if (chk(new Date(hall.timeFrom), new Date(hall.timeFor), DamFunc.nd(date[0], date[1], date[2], time))) {
            allowed = false
          }
        })
        if (allowed) {
          finalButtons.push(time)
        }
      })
      callback(finalButtons)
     	}
    }
  })
}

function getPeriod( _hall_id, _when, _time, callback ) {
	switch (_when) {
    case 'today':
      var date = DamFunc.dateToArray(nd(_time))
      break;
    case 'tomorrow':
      var date = DamFunc.dateToArray(Ago('days', 1))
      break;
    case 'aftertomorrow':
      var date = DamFunc.dateToArray(Ago('days', 2))
      break;
		case 'other':
      var date = DamFunc.dateToArray(Ago('days', 2))
      break;
  }

  Schedule.find({
    hallId: _hall_id
  }, function(err, halls) {
    if (err) console.log(err)
    if (halls) {
      var buttons = [0,1,2]
			var finalButtons = []
			var finalQueries = []
			var enough = false;
      buttons.map(function(hour) {
        var allowed = true
        halls.map(function(hall) {
          if (chk(new Date(hall.timeFrom), new Date(hall.timeFor), Ago('hours',hour, nd(date[0], date[1], date[2], _time))) ) {
            allowed = false
          }
        })
				if(allowed&&!enough){
					finalButtons.push( (hour+1) + ( (hour+1==1) ? ' час':' часа'))
					finalQueries.push( (hour+1).toString() )
				} else {
					enough = true
				}
      })
      callback({text: finalButtons, query: finalQueries});
    }
  })
}

function dateFormat(date){
    var fDate = new Date(date);
    var m = ((fDate.getMonth() * 1 + 1) < 10) ? ("0" + (fDate.getMonth() * 1 + 1)) : (fDate.getMonth() * 1 + 1);
    var d = ((fDate.getDate() * 1) < 10) ? ("0" + (fDate.getDate() * 1)) : (fDate.getDate() * 1);
    return d + "/" + m + "/" + fDate.getFullYear()
}

function chk (f,s,t){
  var td = new Date( (typeof t == 'string') ? f : t )
  if((typeof t == 'string')) {
		console.log(t, 184);
    mh = t.split(':')
    m=mh[1]
    h=mh[0]
    td.setHours(h*1)
    td.setMinutes(m*1)
    td.setSeconds(0)
    td.setMilliseconds(0)
  }
  if(CompareDate(td, s, 'min')<0){
    if(CompareDate(td, f, 'min')>=0) {return true} else {return false}
  } else {
    if(CompareDate(td, f, 'min')<=0) {return true} else {return false}
  }
}

function CompareDate(Fdate, Sdate, returnBy, abs) {
  if(typeof(Sdate) == 'string') {
    var spl = Sdate.split(':');
    var Sdate = new Date(Fdate);
    Sdate.setHours(spl[0]);
    Sdate.setMinutes(spl[1]);
    // console.log(Sdate.toString(), Fdate.toString())
  }
  delta = Fdate.getTime() - Sdate.getTime();
  returnBy = returnBy ? returnBy : 'ms';
  var res = 0;
  switch (returnBy.toLowerCase()) {
    case 'year':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7 / 52);
      break;
    case 'week':
      res = Math.floor(delta / 1000 / 60 / 60 / 24 / 7);
      break;
    case 'day':
      res = Math.floor(delta / 1000 / 60 / 60 / 24);
      break;
    case 'hour':
      res = Math.floor(delta / 1000 / 60 / 60);
      break;
    case 'min':
      res = Math.floor(delta / 1000 / 60);
      break;
    case 'sec':
      res = Math.floor(delta / 1000 );
      break;
    case 'ms':
      res = Math.floor(delta);
      break;
    default:
      res = Math.floor( delta / 1000 / 60 );
  }
  return abs ? Math.abs(res) : res;
}


function nd(a,b,c,d){
   var cur = new Date();
   if(!a){
     return cur;
   }
   if(typeof(a)=='string'){
    if(a.indexOf(':')>-1){
    var num = a.split(":");
    if(!b){
      var date = new Date(cur.getFullYear(), cur.getMonth(), cur.getDate(), num[0], num[1]);
    } else {
      var date = new Date(b.getFullYear(), b.getMonth(), b.getDate(), num[0], num[1]);
    }
    return date;
    }
  }
 if(a && b && c && d){
   var num = d.split(":");
   var date = new Date(a, b, c, num[0], num[1], '00');
   return date;
 }
}

function Ago( what, count, initDate){
  var def = 'days';

  count = count ? count : ((typeof(initDate) == 'object') ? 0 : initDate*1 );
  initDate = initDate ? ((typeof(initDate) == 'object') ? initDate : new Date() ): new Date();
  what = what ? what : def;
  itr = Array.isArray(what) ? what.length : 1;
  for (var i = 0; i < itr; i++) {
    w = Array.isArray(what) ? what[i] : what;
    cnt = Array.isArray(count) ? ((count.length > i) ? count[i] : count[count.length-1]) : count;
    switch (w.toLowerCase()) {
      case 'years':
      var d =  (initDate.getFullYear()+cnt)
      initDate.setYear(d);
      break;
      case 'months':
      var d =  (initDate.getMonth()+cnt);
      initDate.setMonth(d);
      break;
      case 'days':
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
      break;
      case 'hours':
      var d =  (initDate.getHours()+cnt) ;
      initDate.setHours(d);
      break;
      case 'minutes':
      var d =  (initDate.getMinutes()+cnt);
      initDate.setMinutes(d);
      break;
      case 'seconds':
      var d =  (initDate.getSeconds()+cnt) ;
      initDate.setSeconds(d);
      break;
      default:
      var d =  (initDate.getDate()+cnt) ;
      initDate.setDate(d);
    }
  }
  return initDate;
}

module.exports.sendSchedule = sendSchedule
module.exports.getButtons = getButtons
module.exports.getPeriod = getPeriod
module.exports.dateFormat = dateFormat
