// variables for server
// require('./modules/cron.js')

// models in DB
// var Users = require('./models/user.js')
var Hall = require('../models/hall.js')
var Schedule = require('../models/schedule.js')
var Weather = require('../models/weather.js')
var Currency = require('../models/currency.js')
var Ideas = require('../models/Idea.js')
// var Question = require('./models/question.js')

var mongoose = require('mongoose')
// var matcher = require('./matcher').commandHandler
//variables for bot

var BotHelper = require('../BotFunctions')


//==========================================================>>>>>>>>>>>> BOT <<<<<<<<<<<<========================================

// -------------------------> bot functions
// function SendMessage(){
//   Users.find({}, function(err, users){
//     if(err) cb(true, 100)
//     if(users){
//       users.map(function(user){
//         bot.sendMessage(user.telegramID, 'Привет! У меня для вас хорошие новости\n' +
//           'Теперь я принимаю задачи не только текстом, но и кнопками. \n' +
//           'Набери /menu и увидишь доступные функции \n' +
//           'А ещё я теперь знаю погоду в Астане и курсы валют \n ' +
//           'Если вы случайно забронировали конференц зал, то просто \n ' +
//           'отмените бронь нажатием одной кнопки. \n ' +
//           'В общем я становлюсь умнее\)')
//       })
//     }
//   })
// }
// SendMessage()

var channel_id = -1001110590767
var newIdea;
var pages = {};
var choose = {
  reply_markup: JSON.stringify({
    inline_keyboard:  [
      [{text: '\u{2764} Love - 0', callback_data:'love'},
      {text:'\u{1F44D} Like - 0 ', callback_data:'like'},
      {text:'\u{1F44E} Dislike - 0', callback_data:'dislike'}]
    ]
  }),
  resize_keyboard: false,
  parse_mode : "HTML"
}





function getIndex(index) {
  return DamFunc.IndInObjArr(questions, index, 'index')[0]
}



// Questions and trigers

var questions = [
	{index: 0,

		qType:'text',
		options: BotHelper.makeButtons(['Забронировать зал', 'Не хочу'], ['zakaz', 'back'], 2, true),
		data: 'Слушаю',
		err: 'Слушаю',
		answers: [
      {
        AnsType: 'inline',
        reg:/^zakaz$/i,
        next: 1
      },
      {
        AnsType: 'inline',
        reg:/^back/i,
        next: 100
      }
		]
	},
	{index: 1,

		qType:'text',
		options: {},
		data: 'Выбери зал или нажми \"Назад\"',
		err: 'Выбери зал или нажми \"Назад\"',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 2
      }
		]
	},
	{index: 2,

		qType:'text',
		options: BotHelper.makeButtons(['Сегодня', 'Завтра', 'Послезавтра', 'Другой день', 'Назад'], ['today', 'tomorrow', 'aftertomorrow', 'other', 'back'], 3, true),
		data: 'Выберите день:',
		err: 'Выберите день:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 3
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 1
      }
		]
	},
	{index: 3,

		qType:'text',
		options: BotHelper.makeButtons(['До обеда', 'После обеда', 'Назад'], ['morning', 'evening', 'back'], 2, true),
		data: 'Выберите период:',
		err: 'Выберите период:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 4
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 2
      }
		]
	},
	{index: 4,

		qType:'text',
		options:{},
		data: 'Выберите время:',
		err: 'Выберите время:',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 5
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 3
      }
		]
	},
	{index: 5,

		qType:'text',
		options: BotHelper.makeButtons(['1 час', '2 часа', '3 часа', 'Назад'], ['1', '2', '3', 'back'], 3, true),
		data: 'Определите продолжительность',
		err: 'Определите продолжительность',
		answers: [
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 6
      },
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 4
      }
		]
	},
	{index: 6,

		qType:'text',
		options: {},
		data: '....',
		err: '....',
		answers: [
      {
        AnsType: 'text',
        reg:/^(.*)$/i,
        next: 0
      }
		]
	},
	{index: 7,

		qType:'text',
		options: {},
		data: 'Напишите дату в формате "ДД.ММ.ГГГГ"',
		err: 'Напишите дату в формате "ДД.ММ.ГГГГ"',
		answers: [
      {
        AnsType: 'text',
        reg:/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/i,
        next: 0
      }
		]
	},
	{index: 10,

		qType:'text',
		options: BotHelper.makeButtons(['Да', 'Нет'], ['yes', 'no'], 2, true),
		data: 'Правильно я к вам обращаюсь?',
		err: 'Правильно я к вам обращаюсь?',
		answers: [
      {
        AnsType: 'inline',
        reg:/^yes$/i,
        next: 13
      },
      {
        AnsType: 'inline',
        reg:/^no$/i,
        next: 11
      }
		]
	},
	{index: 11,

		qType:'text',
		options: {},
		data: 'Круто! А какая у вас фамилия?',
		err: 'Круто! А какая у вас фамилия?',
		answers: [
      {
        AnsType: 'text'
      }
		]
	},
	{index: 12,

		qType:'text',
		options: {},
		data: 'А кем вы работаете в НПП \"Атамекен\"?',
		err: 'А кем вы работаете в НПП \"Атамекен\"?',
		answers: [
      {
        AnsType: 'text'
      }
		]
	},
	{index: 13,

		qType:'text',
		options: {},
		data: 'О! Классная должность! А какой департамент?',
		err: 'О! Классная должность! А какой департамент?',
		answers: [
      {
        AnsType: 'text'
      }
		]
	},
	{index: 14,

		qType:'text',
		options: {},
		data: 'Приятно познакомиться',
		err: 'Приятно познакомиться',
		answers: [
      {
        AnsType: 'text'
      }
		]
	},
	{index: 15,

		qType:'text',
		options: {},
		data: 'Читайте внимательней',
		err: 'Читайте внимательней',
		answers: [
      {
        AnsType: 'inline',
        reg:/^yes$/i
      },
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 15
      }
		]
	},
  {index: 20,

    qType:'text',
    options: {},
    data: 'Отмена брони',
    err: 'Отмена брони',
    answers: [
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 100
      },
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 21
      }
    ]
  },
  {index: 21,

    qType:'text',
    options: {},
    data: 'Отмена брони',
    err: 'Отмена брони',
    answers: [
      {
        AnsType: 'inline',
        reg:/^back$/i,
        next: 20
      },
      {
        AnsType: 'inline',
        reg:/^(.*)$/i,
        next: 100
      }
    ]
  },
	{index: 100,

		qType:'text',
		options: {},
		data:'Чем я могу вам помочь?',
		err: 'Чем я могу вам помочь?',
		answers: [
			{
        AnsType: 'text',
        reg: /^(.*)$/i
      },
      {
        AnsType: 'inline',
        reg:/^зал/i,
      },
      {
        AnsType: 'inline',
        reg:/^погода/i,
      },
      {
        AnsType: 'inline',
        reg:/^курс/i
      },
      {
        AnsType: 'inline',
        reg:/^отмена/i
      }
		]
	},
	{index: 101,

		qType:'text',
		options: {},
		data: '=====================End ',
		err: '=====================End ',
		answers: [
			{
        AnsType: 'text',
        reg: /^(.*)$/i
      }
		]
	}
]

var trigger = {
    stopOn: {
      force: [],
      data: '/stop',
      AnsType: 'text',
      reg: /\/jgj7079048961/i
    },
    startOn: {
      data: '/start',
      AnsType: 'text',
      reg: /^(.*)$/i
    }
}

// Names

var vips = [
	'Шотанов',
	'Куандыкова',
	'Альтаев',
	'Амангельдинова',
	'Якупбаева',
	'Исагалиева',
	'Журсунов',
	'Смагулова',
	'Рамазанов',
	'Жумагазиев',
	'Альжанова',
	'Жунусова',
	'Кожахметова',
	'Kanatova'
]

var badNames = [
	'пидор',
	'дурак',
	'педик',
	'гандон',
	'котак',
	'қотақ',
	'/start',
	'/stop'
]


function firstCallBack (user_Id,username,msg,cb) {
  Users.findOne({telegramID: user_Id}, function (err, found) {
  	if(err) {
  		bot.sendMessage(msg.from.id, 'Произошла ошибка, попробуйте снова.')
  	}
    if(found) {
    	cb(true, 100)
    } else{
     	var match = false
    	vips.map(function(vip){
    		if(msg.from.last_name && msg.from.last_name.toLowerCase() == vip.toLowerCase()){
    			match=true
    		}
    	})
    	if(match){
    		var user = new Users({
						telegramID:msg.from.id,
						username:msg.from.username,
						firstName:msg.from.first_name,
						lastName:msg.from.last_name,
						vip:true
					})
					user.save(function(){
						var i = getIndex(10)
						questions[i].data= 'Приветик ' + msg.from.first_name + '!\nМеня зовут Анжелика.\nПравильно ли я обращаюсь к вам, '
						+ msg.from.first_name + ' ' + (msg.from.last_name?msg.from.last_name:'') + '?'
						cb(true, 10)
					})
    	} else{
    		var user = new Users({
					telegramID:msg.from.id,
					username:msg.from.username,
					firstName:msg.from.first_name,
					lastName:msg.from.last_name,
					vip:false
				})
		      user.save(function(){
					var i = getIndex(10)
		    	questions[i].data= 'Приветик ' + msg.from.first_name + '!\nМеня зовут Анжелика.\nПравильно ли я обращаюсь к вам, '
		    	+ msg.from.first_name + ' ' + (msg.from.last_name?msg.from.last_name:'') + '?'
					cb(true, 10)
				})
    	}
    }
  })
}

function midCallback (user_Id, answers, ind, cb, msg) {
  switch (ind) {
    case 0:
	    if(answers[ind] == 'back'){
	    	cb(true, 100)
	    	return
	    }
      Users.findOne({telegramID: user_Id},function (err, user) {
        if(user){
          var i = getIndex(1)
					var buttonName=[]
					var buttonId=[]
          Hall.find({}, function(err, halls){
          	if(err){
          		console.log(err)
          		cb(true, 0)
          	} else if(halls.length>0){
          		halls.map(function(hall, index){
          			if(user.vip==true){
									buttonName.push(hall.name)
	          			buttonId.push(hall._id)
          			} else{
          				if(!hall.vip){
									buttonName.push(hall.name)
	          			buttonId.push(hall._id)
	          			}
          			}
          		})
							questions[i].options = BotHelper.makeButtons([...buttonName,'Назад'],[...buttonId, 'back'], 3, true)
			      	questions[i].data = msg.from.first_name + ', выберите пожалуйста зал:'
              cb(true,1)
           	}
          })
        } else{
          cb(true, 1)
        }
      })
      break
    case 1:
    	if(answers[ind] == 'back'){
    	cb(true, 100)
    	} else{
    	var i = getIndex(2)
     	questions[i].data = msg.from.first_name + ', выберите пожалуйста день:'
      cb(true, 2)
    	}
      break
    case 2:
      	if(answers[ind] == 'back'){
        	cb(true, 1)
      	} else if(answers[ind]=='other'){
					cb(true, 7)
				} else{
	      	var i = getIndex(3)
					sendSchedule( answers[ind], answers[ind-1], null, function (result) {
					  bot.sendMessage(user_Id, result)
					})
	      	questions[i].data = 'А теперь определите пожалуйста период:'
         	cb(true, 3)
      	}
      	break
    case 3:
      	if(answers[ind] == 'back'){
		      cb(true, 2)
		    }  else{
					getButtons(answers[1], answers[3], answers[2], function (buttons) {
						var i = getIndex(4)
            if(buttons.length==0){
              bot.sendMessage(user_Id, 'Забронировать зал в это время уже не получится. Sorry :-(')
              .then(function(){
                cb(true, 3, true)
              })
              .catch(function(){
                cb(true, 3, true)
              })
            } else{
              questions[i].options = BotHelper.makeButtons([...buttons, 'Назад'], [...buttons, 'back'], buttons.length==0?1:3, true)
              cb(true)
            }
					})
		    }
		    break
    case 4:
			if(answers[ind] == 'back'){
				cb(true, 3)
      } else{
	   		var i = getIndex(5)
	    	questions[i].data = 'С временем определились, ' + msg.from.first_name
	    	+ ', осталось выбрать продолжительность. Определите пожалуйста продолжительность:'
				getPeriod(answers[1], answers[2], answers[4], function (buttons) {
					questions[i].options = BotHelper.makeButtons([...buttons.text, 'Назад'], [...buttons.query, 'back'], buttons.text.length==0?1:3, true)
					cb(true, 5)
				})
      }
      break
    case 5:
	    if(answers[ind] == 'back'){
	    	cb(true, 4)
	    } else{
	    	async.waterfall([
	    		function(done){
	        	switch(answers[2]){
	      			case 'today':
	        			var today = new Date().setHours(0, 0, 0, 0)
		      			break
		      		case 'tomorrow':
	        			var today = new Date().setHours(0, 0, 0, 0) + 86400000
		      			break
		      		case 'aftertomorrow':
	        			var today = new Date().setHours(0, 0, 0, 0) + 86400000*2
		      			break
							case 'other':
								var array = answers[7].split('.');
								var today = new Date(array[2], array[1]*1-1, array[0]*1).getTime();
		      			break
	      		}
	      		done(null, today)
	     		},
	     		function(today, done){
	     			var time_from = today + Number(answers[4].split(':')[0] * 3600000)
	     			var time_for = time_from + (Number(answers[5])) * 3600000
	     			var _times = []
	     			for(var i = 0; i < Number(answers[5]); i++){
	     				_times.push((Number(answers[4].split(':')[0]) + i) + ':00')
	     			}
	     			Schedule.findOne({hallId:answers[1]})
	     			.where('timeFrom').lte(time_from)
	     			.where('timeFor').gt(time_from)
	     			.populate('hallId user')
	     			.exec(function(err, schedule){
	     				if(err) console.log(err)
	     				if(schedule){
								bot.sendMessage(msg.from.id, 'Зал ' + schedule.hallId.name + ' занят ' + (schedule.user.lastName?schedule.user.lastName:schedule.user.username))
								cb(true, 1)
	     				} else{
	     					Users.findOne({telegramID: user_Id}, function(err, user){
	     						if(err) console.log(err)
	     						if(user){
	     							if(answers[3]=='morning' && time_from>=today + Number('12' * 3600000) && Number(answers[5]) > 1){
				     					var newSchedule = new Schedule({
				     						hallId: answers[1],
				     						timeFrom: time_from,
				     						timeFor: time_for,
				     						duration: answers[5],
				     						period: answers[3],
				     						secondPeriod:'evening',
				     						stringFrom: answers[4],
				     						stringFor: new Date(time_for).getHours() + ':00',
				     						times:_times,
				     						date: today,
				     						user: user._id
				     					})
				     					newSchedule.save(function(err, saved){
				     						Hall.findOne({_id:saved.hallId}, function(err, hall){
				     							if(err){
				     								console.log(err)
				     							} else{
		    										bot.sendMessage(msg.from.id, 'Вроде все. Давайте проверим: вы забронировали зал '
		    										 + hall.name + ' на ' + dateFormat(saved.timeFrom)
		    										 + ' с ' + new Date(saved.timeFrom).getHours()
		    										 + ':00 до ' + new Date(saved.timeFor).getHours() + ':00')
		    										.then( function(){
								   					var i = getIndex(100)
								   					questions[i].options = {}
		   											cb(true, 100, true)
		    										})
				     							}
				     						})
				     					})
			     					} else{
											var newSchedule = new Schedule({
				     						hallId: answers[1],
				     						timeFrom: time_from,
				     						timeFor: time_for,
				     						duration: answers[5],
				     						period: answers[3],
				     						stringFrom: answers[4],
				     						stringFor: new Date(time_for).getHours() + ':00',
				     						times:_times,
				     						date: today,
				     						user: user._id
				     					})
				     					newSchedule.save(function(err, saved){
												if(err) console.log(err);
												if(saved){
													Hall.findOne({_id:saved.hallId}, function(err, hall){
														if(err){
															console.log(err)
														} else{
															bot.sendMessage(msg.from.id, msg.from.first_name
																+ ', вроде все. Давайте проверим: вы забронировали зал '
																+ hall.name + ' на ' + dateFormat(saved.timeFrom)
																+ ' с ' + new Date(saved.timeFrom).getHours()
																+ ':00 до ' + new Date(saved.timeFor).getHours() + ':00')
																.then(function(){
																	var i = getIndex(100)
																	questions[i].options = {}
																	cb(true, 100, true)
																})
															}
														})
												}
				     					})
			     					}
	     						}
	     					})
	     				}
	     			})
	     		}
	    	])
	    }
			break
		case 7:
			var array = answers[ind].split('.');
			var tod = new Date().setHours(0, 0, 0, 0);
			var today = new Date(array[2], array[1]*1-1, array[0]*1).getTime();
			if(today>tod){
				sendSchedule( answers[2], answers[1], answers[ind], function (result) {
					bot.sendMessage(user_Id, result)
				})
				var i = getIndex(3)
				questions[i].data = 'А теперь определите пожалуйста период:'
				cb(true, 3)
			}else {
				cb(true, 7)
			}
		break
//=================>>>>>>>>>>>>>>>>>> Greetings
		case 10:
			if(answers[ind] == 'yes'){
				var i = getIndex(15)
				questions[i].data = 'Приветик, ' + msg.from.first_name + '!\n'
				+ 'Рада знакомству! Я Анжелика, бот-ассистент НПП «Атамекен»! Я помогу вам '
				+ 'бронировать конференц-залы, '
				+ 'напоминать, что сегодня чудесная погода, узнавать курсы валют, записывать ваши идеи и отправлять их на голосование.\n'
				+ '\nЯ такая умница, что всегда стремлюсь к знаниям. Поэтому мои разработчики постоянно учат меня новым фишкам.\n'
				+	'— Можете написать «Анжелика, забронируй мне зал», чтобы я заняла зал;\n'
				+	'— Для того, чтобы я показала прогноз погоды, напишите «Анжелика, покажи погоду»;\n'
				+	'— Красивые девушки тянутся к богатым, поэтому не забывайте писать «Анжелика, покажи курс валют»;\n'
				+	'— Напишите свою идею с хэштегом #idea, чтобы я запомнила ее;\n'
				+	'— Напишите свою идею с хэштегом #idealike, чтобы отправить идею на голосование;\n'
				+	'— Хотите узнать все идеи? Напишите комманду /idea_all;\n'
				+	'В общем, не стесняйтесь писать, я всегда рада поговорить : )\n'
				+ 'Вроде все рассказала. Надеюсь скоро увидимся!\n'
				+ 'Все ли понятно?'
				questions[i].options = BotHelper.makeButtons(['Да', 'Нет'], ['yes', 'no'], 2, true)
				cb(true, 15)
			} else{
				var i = getIndex(11)
				questions[i].data = 'Давайте познакомимся! Как вас зовут?'
				cb(true, 11)
			}
			break
		case 11:
			if(answers[ind].toLowerCase() == '/start'|| answers[ind].toLowerCase() == '/stop'){
				var i = getIndex(11)
				questions[i].data = 'Попробуйте еще раз!'
				cb(true, 11)
			} else{
				var i = getIndex(14)
				questions[i].data = answers[11] + '! Круто! А какая у вас фамилия?'
				cb(true, 14)
			}
			break
		case 12:
			if(answers[ind].toLowerCase() == '/start'|| answers[ind].toLowerCase() == '/stop'){
				var i = getIndex(12)
				questions[i].data = 'Попробуйте еще раз!'
				cb(true, 12)
			} else{
				var i = getIndex(13)
				questions[i].data = 'А кем вы работаете в НПП \"Атамекен\"?'
				cb(true, 13)
			}
			break
		case 13:
			if(answers[ind].toLowerCase() == '/start'|| answers[ind].toLowerCase() == '/stop'){
				var i = getIndex(13)
				questions[i].data = 'Попробуйте еще раз!'
				cb(true, 13)
			} else{
				var i = getIndex(14)
				questions[i].data = 'О! Классная должность! А какой департамент?'
				cb(true, 14)
			}
			break
		case 14:
			if(answers[ind].toLowerCase() == '/start'|| answers[ind].toLowerCase() == '/stop'){
				var i = getIndex(14)
				questions[i].data = 'Попробуйте еще раз!'
				cb(true, 14)
			} else{
				Users.findOne({telegramID:msg.from.id}, function(err, user){
					if(err) console.log(err)
					if(user){
							user.trueFirstName=answers[11]
							user.trueLastName=answers[14]
							user.save(function(err){
								if(err) console.log(err)
							})
					} else{
						var newUser = new User({
							trueFirstName: answers[11],
							trueLastName: answers[14],
							telegramID:msg.from.id,
							username:msg.from.username,
							firstName:msg.from.first_name,
							lastName:msg.from.last_name
						})
						newUser.save(function(err){
							if(err) console.log(err)
						})
					}
				var i = getIndex(15)
				questions[i].data = 'Приветик, ' + answers[11] + '!\n'
				+ 'Рада знакомству! Я Анжелика, бот-ассистент Атамекен! Я помогу вам '
				+ 'бронировать конференц-залы, '
				+ 'напоминать, что сегодня чудесная погода и узнавать курс валют,\n записывать ваши идеи и отправлять их на голосование.\n'
				+ 'Я такая умница, что всегда стремлюсь к знаниям. Поэтому мои разработчики постоянно учат меня новым фишкам.\n'
				+	'- Можешь написать «Анжелика, забронируй мне зал», чтобы я заняла зал;\n'
				+	'- Для того, чтобы я показала прогноз погоды, напиши «Анжелика, покажи погоду»;\n'
				+	'- Красивые девушки тянутся к богатым, поэтому не забывай писать «Анжелика, покажи курс валют»;\n'
				+	'- Интеллект - это сексуально, напиши свою идею с хэштегом #idea, чтобы я запомнила ее;\n'
				+	'- Напиши свою идею с хэштегом #idealike, чтобы отправить идею на голосование;\n'
				+	'- Хочешь узнать все идеи, напиши комманду /idea_all;\n'
				+	'В общем, не стесняйся писать, я всегда рада поговорить : )\n'
				+ 'Вроде все рассказала. Надеюсь скоро увидимся!\n'
				+ 'Все ли понятно?'
				questions[i].options = BotHelper.makeButtons(['Да', 'Нет'], ['yes', 'no'], 2, true)
				cb(true, 15)
				})
			}
			break
		case 15:
			if(answers[ind] == 'yes'){
				// var i = getIndex(100)
				// questions[i].options = BotHelper.makeButtons(['Забронировать Зал', 'Прогноз погоды', 'Курсы валют'], ['зал', 'погода', 'курс'], 2, true)
				cb(true, 100)
			} else{
				var i = getIndex(100)
				questions[i].data = 'Ну ладно, давайте сделаем так: вы пишете #question и задаете свой вопрос, а я передаю его своим разработчикам. Они ответят вам в течении 5 минут.'
				// questions[i].options = BotHelper.makeButtons(['Да', 'Нет'], ['yes', 'no'], 2, true)
				cb(true, 100)
			}
			break
    case 20:
      if(answers[ind] == 'back'){
        cb(true, 100)
      } else{
        Users.findOne({telegramID: user_Id}, function(err, user){
          if(err) console.log(err)
          if(user){
            Schedule.find({user:user._id, hallId:answers[ind]}).populate('hallId user').exec(function(err, schedules){
              if(err){
               console.log(err)
               cb(true, 100)
              } else if(schedules.length>0){
                var i = getIndex(21)
                var buttonName=[]
                var buttonId=[]
                schedules.map(function(schedule){
                  var lastItem = schedule.times.length -1
                  buttonName.push(dateFormat(schedule.date) + ' - ' + schedule.times[0] + ' - ' + schedule.times[lastItem])
                  buttonId.push(schedule._id)
                })
                questions[i].options = BotHelper.makeButtons([...buttonName,'Назад'],[...buttonId, 'back'], 2, true)
                questions[i].data = msg.from.first_name + ', выберите пожалуйста время, бронь на которую хотите отменить:'
                cb(true,21)
              }
            })
          }
        })
      }

      break
    case 21:
      if(answers[ind] == 'back'){
        cb(true, 20)
      } else{
        Schedule.findOneAndRemove({_id:answers[ind]}, function(err){
          if(err){
            console.log(err)
          } else{
            bot.sendMessage(user_Id, 'Отмена брони прошла успешно')
            .then(function(){
              cb(true, 100, true)
            })
            .catch(function(){
              cb(true, 100, true)
            })
          }
        })
      }

      break
//=================>>>>>>>>>>>>>>>>>> HubPlace
		case 100:
			var commands = [
			    {
			      id:'first',
			      command: 'Анжел заброн зал zal zabron',
			      callback: getHalls
			    },
			    {
			      id:'second',
			      command: 'Анжел покаж прогноз погод pogod progno',
			      callback: getWeather
			    },
			    {
			      id:'third',
			      command: 'Анжел покаж курс валют kurs valu vali',
			      callback: getCurrency
			    },
			    {
			      id:'fifth',
			      command: 'menu меню',
			      callback: function(id, p){
			      	if(!id) return
			      		var i = getIndex(100)
			      		questions[i].options = BotHelper.makeButtons(['Забронировать Зал', 'Прогноз погоды', 'Курсы валют', 'Отменить бронь'], ['зал', 'погода', 'курс', 'отмена'], 2, true)
			      		cb(true, 100)
			      }
			    },
			    {
			      id:'sixth',
			      command: 'хуй пизда пидор пидар сука сучк коза коров телк шалав педер',
			      callback: function(id, p){
			      	if(!id) return
			      		bot.sendMessage(user_Id, 'Фу-фу-фу! У меня болят уши, когда так ругаются :-(')
			      		.then(function(){
									var i = getIndex(100)
				      		questions[i].options = {}
				      		cb(true, 100, true)
			      		})
			      		.catch(function(){
									var i = getIndex(100)
				      		questions[i].options = {}
				      		cb(true, 100, true)
			      		})
			      }
			    },
			    {
			      id:'seventh',
			      command: 'Привет Helo hello hi Здравствуй здрав ',
			      callback: function(id, p){
			      	if(!id) return
		      		bot.sendMessage(user_Id, 'Приветик!')
		      		.then(function(){
								var i = getIndex(100)
			      		questions[i].options = {}
			      		cb(true, 100, true)
		      		})
							.catch(function(){
								var i = getIndex(100)
			      		questions[i].options = {}
			      		cb(true, 100, true)
		      		})
            }
			    },
          {
            id:'eighth',
            command: 'cancel удалить delete del отмен',
            callback: function(id, p){
              if(!id) return
              Users.findOne({telegramID: user_Id}, function(err, user){
                if(err) console.log(err)
                if(user){
                  Schedule.find({user:user._id}).populate('hallId user').exec(function(err, schedules){
                    if(err){
                     console.log(err)
                     cb(true, 100)
                    }else if(schedules.length>0){
                      var buttonName=[]
                      var buttonId=[]
                      schedules.map(function(schedule){
                        buttonName.push(schedule.hallId.name)
                        buttonId.push(schedule.hallId._id)
                      })
                      buttonName = buttonName.filter(function(item, pos) {
                          return buttonName.indexOf(item) == pos;
                      })
                      buttonId = buttonId.filter(function(item, pos) {
                          return buttonId.indexOf(item) == pos;
                      })
                      var i = getIndex(20)
                      questions[i].options = BotHelper.makeButtons([...buttonName,'Назад'],[...buttonId, 'back'], 3, true)
                      questions[i].data = msg.from.first_name + ', выберите пожалуйста зал:'
                      cb(true,20)
                    } else if(schedules.length==0){
                      bot.sendMessage(user_Id, 'У вас нет забронированных залов, может сначала забронируете хоть один? ;-)')
                      .then(function(){
                        cb(true, 100, true)
                      })
                      .catch(function(){
                        cb(true, 100, true)
                      })
                    }
                  })
                }
              })
            }
          },
					{
					 id:'nineth',
					 command: '#idea',
					 callback: saveIdea
				 },
				 {
					id:'tenth',
					command: '/idea_all',
					callback: allIdea
				},
				{
				 id:'eleventh',
				 command: '/idea_file',
				 callback: ideaFile
			 },
			 {
				id:'twelve',
				command: '#question',
				callback: saveQuestion
			}
			//  ,
			//  {
			// 	id:'eleventh',
			// 	command: '/idea_file',
			// 	callback: ideaFile
			//  }
			]
			function getCurrency ( id, percent ) {
			  if(!id) return
				var today = new Date().setHours(0,0,0,0)
					Currency.find({})
					.where('date').gte(today).exec(function(err, currencies){
						if(err) console.log(err)
						if(currencies.length>0){
							var nearest = 0
		 					var max = 0
		 					currencies.map(function(currency, i){
		 						if(currency.date>max){
		 							max=currency.date
		 							nearest = i
		 						}
		 					})
		 					with (currencies[nearest]) {
		   						var cours = 'Курсы валют:'
		   												+'\n >USD/KZT: ' + USDKZT.toFixed(2) + ' ₸'
		   												+'\n >EUR/KZT: ' + (USDKZT/USDEUR).toFixed(2) + ' ₸'
		   												+'\n >BTC/KZT: ' + (USDKZT/USDBTC).toFixed(2) + ' ₸'
		   												+'\n >RUB/KZT: ' + (USDKZT/USDRUB).toFixed(2) + ' ₸'

		 					}
		 					bot.sendMessage(user_Id, cours)
		 					.then(function(){
									var i = getIndex(100)
			   					questions[i].options = {}
									cb(true, 100, true)
		 					})
		 					.catch(function(){
									var i = getIndex(100)
			   					questions[i].options = {}
									cb(true, 100, true)
		 					})
						}else{
		   					bot.sendMessage(user_Id, 'Нет данных о курсах валют, sorry ;-)')
		   					.then(function(){
			   					var i = getIndex(100)
			   					questions[i].options = {}
			   					cb(true, 100, true)
		   					})
		   					.catch(function(){
			   					var i = getIndex(100)
			   					questions[i].options = {}
			   					cb(true, 100, true)
		   					})
		   				}
					})
			}
			function getWeather ( id, percent ) {
				if(!id) return
				var today = new Date().setHours(0,0,0,0)
		  			Weather.find({city:'Astana'})
		   			.where('date').gte(today).exec(function(err, weathers){
		   				if(err) console.log(err)
		   				if(weathers.length>0){
		   					var nearest = 0
		   					var max = 0
		   					weathers.map(function(weather, i){
		   						if(weather.date>max){
		   							max=weather.date
		   							nearest = i
		   						}
		   					})
		   					with (weathers[nearest]) {
			   						var pogoda = 'Погода в Астане:'
			   												+'\n > ' + main + ' <'
			   												+'\n >Температура: ' + temp + '°C'
			   												+'\n >Скорость ветра: ' + wind_speed + 'м/с'
		   					}

		   					bot.sendMessage(user_Id, pogoda)
		   					.then(function(){
		   					var i = getIndex(100)
		   					questions[i].options = {}
								cb(true, 100, true)
		   					})
		   					.catch(function(){
		   					var i = getIndex(100)
		   					questions[i].options = {}
		   					cb(true, 100, true)
		   					})
		   				} else{
		   					bot.sendMessage(user_Id, 'Нет данных о погоде, sorry ;-)')
		   					.then(function(){
		   					var i = getIndex(100)
		   					questions[i].options = {}
								cb(true, 100, true)
		   					})
		   					.catch(function(){
		   					var i = getIndex(100)
		   					questions[i].options = {}
								cb(true, 100, true)
		   					})
		   				}
		   			})
			}
			function getHalls ( id, percent ) {
				if(!id) return
					 Users.findOne({telegramID: user_Id},function (err, user) {
		        if(user){
		          var i = getIndex(1)
							var buttonName=[]
							var buttonId=[]
		          Hall.find({}, function(err, halls){
		          	if(err){
		          		console.log(err)
		          		cb(true, 1)
		          	} else if(halls.length>0){
		          		halls.map(function(hall, index){
		          			if(user.vip==true){
											buttonName.push(hall.name)
			          			buttonId.push(hall._id)
		          			} else{
		          				if(!hall.vip){
											buttonName.push(hall.name)
			          			buttonId.push(hall._id)
			          			}
		          			}
		          		})
									questions[i].options = BotHelper.makeButtons([...buttonName,'Назад'],[...buttonId, 'back'], 3, true)
					      	questions[i].data = msg.from.first_name + ', выберите зал или нажми \"Назад\":'
		              cb(true,1)
		           	}
		          })
		        } else{
		          cb(true, 1)
		        }
		      })
			}
		 	matcher(commands, answers[ind], function (id, percent) {
		 		if(!id){
		 			bot.sendMessage(user_Id, 'Извините, я вас не понимаю. Попробуйте еще раз.')
		 			.then(function (argument) {
			 			var i = getIndex(100)
						questions[i].options = {}
						cb(true, 100, true)
		 			})
		 			.catch(function (argument) {
		 				var i = getIndex(100)
						questions[i].options = {}
						cb(true, 100, true)
					})
		 		}
		 	})
				break
    default:
    	cb(true)
  }
}


function endCallBack (user_Id,answers,cb,msg) {
  bot.sendMessage(user_Id, 'С нетерпением жду нашей следующей встречи! Пока!')
  cb(false)
}

function stopCallback(user_Id,force,answers) {}

// BotHelper.askFor (
// 	bot,
// 	questions,
// 	trigger,
// 	firstCallBack,
// 	midCallback,
// 	endCallBack,
// 	stopCallback
// )

var saveIdea = function(){
	bot.onText(/(.+|)\#(idea|идея)(.+|)/, (msg) => {
	  var idea
	  if (msg.reply_to_message !== undefined) {
	    idea = msg.reply_to_message.text.replace('#idealike', '').replace('#idea', '').replace('#идея', '').replace('  ', ' ').trim()
	    if (msg.text.includes('#idealike') && !msg.reply_to_message.text.includes('#idealike')) {
	      if (msg.chat.title !== undefined) {
	        var message = "Пользователь " + '<b>' + msg.from.first_name + '</b>' + " из группы " + '<i>' + msg.chat.title + '</i>'  + " предложил(а) идею. \n"
	      } else {
	        var message = "Пользователь " + '<b>' + msg.from.first_name + '</b>' + " предложил(а) идею. \n"
	      }
	      bot.sendMessage(channel_id, message + idea, choose)
	    }
	  } else  {
	    if (msg.text.includes('#idealike')) {
	        idea = msg.text.replace('#idealike', '').replace('  ', ' ').trim()
	        if (msg.chat.title !== undefined) {
	          var message = "Пользователь " + '<b>' + msg.from.first_name + '</b>' + " из группы " + '<i>' + msg.chat.title + '</i>'  + " предложил(а) идею. \n"
	        } else {
	          var message = "Пользователь " + '<b>' + msg.from.first_name + '</b>' + " предложил(а) идею. \n"
	        }
	        bot.sendMessage(channel_id, message + idea, choose)
	    } else {
	        idea = msg.text.replace('#idea', '').replace('#идея', '').replace('  ', ' ').trim()
	    }
	  }

	  if (idea !== '') {
	    newIdea = new Ideas({
	        text : idea,
	        chat_id : msg.chat.id.toString(),
	        username : msg.from.first_name,
	        from_id: msg.from.id.toString(),
	        chat_name: msg.chat.title,
	        love: 0,
	        like: 0,
	        dislike: 0,
	        voters: []
	    })
	    newIdea.save().then(function (idea) {
	      bot.sendMessage(msg.chat.id, 'Спасибо, ' + msg.from.first_name + '. Ваша идея успешно сохранена!')
	    }).catch(function (err) {
	      bot.sendMessage(msg.chat.id,'Данная идея уже предлагалась.')
	    });
	  } else {
	    bot.sendMessage(msg.chat.id,'Сообщение пустое. Пожалуйста, попробуйте снова.')
	  }
	});
}

var saveQuestion = function(){
	bot.onText(/(.+|)\#(question)/, (msg) => {
		// console.log(msg);
	  var question = msg.text.replace('#question', '').replace('  ', ' ').trim()
	  if (question !== '') {
	    newQuestion = new Question({
	        text : question,
	        chat_id : msg.chat.id.toString(),
	        username : msg.from.first_name,
	        from_id: msg.from.id.toString(),
	        chat_name: msg.chat.title
	    })
	    newQuestion.save(function(err, saved){
				if(err) console.log(err);
				if(saved){
					console.log("вопрос сохранен");
				}
			}).then(function (quest) {
	      bot.sendMessage(msg.chat.id, 'Отлично, ' + msg.from.first_name + '! Мои разработчики уже размышляют над вашим вопросом.')
				var i = getIndex(100);
				questions[i].data = "Чем я могу вам помочь?";
				bot.sendMessage(332040108, 'Вопрос от @'+msg.from.username+' :\n'+ question)
	    }).catch(function (err) {
	      // bot.sendMessage(msg.chat.id,'Данная идея уже предлагалась.')
	    });
	  } else {
			console.log("question are empty");
	    bot.sendMessage(msg.chat.id,'Сообщение пустое. Пожалуйста, попробуйте снова.')
	  }
	});
}


var allIdea = function(){
	bot.onText(/\/idea_all/, function (msg) {
	  var ideas = ''
	  Ideas.find({}).exec(function (err, idea) {
	    if (err) {
	      bot.sendMessage(msg.chat.id, 'Извините, ничего не нашлось.')
	    } else if (idea.length !== 0) {
	        var pgs = [];
	        idea.forEach(function (ide,i) {
	          ideas += (i*1+1)+') '+ '<b>' + ide.username + '</b>' + '\n' + ide.text + '\n' +
	          'Love: ' + ide.love + '\n' + 'Like: ' + ide.like + '\n' +
	          'Dislike: ' + ide.dislike + '\n\n'
	          if ((i*1+1) % 5 == 0){
	            pgs.push(ideas)
	            ideas = '';
	          }
	        });
	        if(ideas != '') pgs.push(ideas)
	        var opts = {
	          reply_markup: JSON.stringify({
	            inline_keyboard:  [
	              [{text: 'Next \u{25B6}', callback_data: 'next' }]
	            ]
	          }),
	          resize_keyboard: false,
	          parse_mode : "HTML"
	        }
	        pages = {
	          pgs,
	          id: msg.chat.id,
	          index: 0
	        }

	        bot.sendMessage(msg.chat.id, pages.pgs[pages.index],(pgs.length > 1) ? opts : {parse_mode : "HTML"})
	    } else {
	        bot.sendMessage(msg.chat.id, 'Извините, ничего не нашлось.')
	    }
	  })
	})
}


var ideaFile = function(){
	bot.onText(/\/idea_file/, function (msg) {
	  var ideas = ''
	  Ideas.find({}).exec(function (err, idea) {
	    if (err) {
	      bot.sendMessage(msg.chat.id, 'Извините, ничего не нашлось.')
	    }
	    else if (idea.length !== 0) {
	      var stream = fs.createWriteStream("file" + msg.chat.id + ".csv");
	      stream.once('open', function(fd) {
	        stream.write('"' + "Пользователь" + '"' + ';' + '"' + "Идея" +
	        '"' + ';' + '"' + "Количество love" + '"' + ';' + '"' + "Количество like"  + '"'  + ';'
	        + '"' + "Количество dislike" + '"' + ';\n')
	        idea.forEach(function (ide,i) {
	          ideas = '"' + ide.username + '"' + ';' + '"' + ide.text +
	          '"' + ';' + '"' + ide.love + '"' + ';' + '"' + ide.like  + '"'  + ';'
	          + '"' + ide.dislike + '"' + ';\n'
	          stream.write(ideas)
	          ideas = ''
	        })
	        stream.end(function (q,w,e) {
	          bot.sendDocument(msg.chat.id, "./file" + msg.chat.id + ".csv")
	        });
	      });
	    }
	    else {
	        bot.sendMessage(msg.chat.id, 'Извините, ничего не нашлось.')
	    }
	  })
	})
}

bot.on('callback_query', function (msg) {
  switch (msg.data) {
    case 'next':
      pages.index++;
      var opts = {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id,
        reply_markup: JSON.stringify({
          inline_keyboard:  [
            (pages.pgs.length-1==pages.index ) ? [{text: '\u{25C0} Back', callback_data: 'prev' }]: [{text: '\u{25C0} Back', callback_data: 'prev' },
            {text: 'Next \u{25B6}', callback_data: 'next' }]
          ]
        }),
        resize_keyboard: false,
        parse_mode : "HTML"
      }
      bot.editMessageText(pages.pgs[pages.index],opts)
      break

    case 'prev':
      var opts = {
        message_id: msg.message.message_id,
        chat_id: msg.message.chat.id,
        reply_markup: JSON.stringify({
          inline_keyboard:  [
            (pages.index - 1 == 0 ) ? [{text: 'Next \u{25B6}', callback_data: 'next' }]: [{text: '\u{25C0} Back', callback_data: 'prev' },
            {text: 'Next \u{25B6}', callback_data: 'next' }]
          ]
        }),
        resize_keyboard: false,
        parse_mode : "HTML"
      }
      pages.index--;
      bot.editMessageText(pages.pgs[pages.index], opts)
      break

    case 'like':
      var text = msg.message.text.split('\n')
      Ideas.findOne({text: text[1]}).exec(function (err, idea) {
        if (idea && !idea.voters.includes(msg.from.id)) {
          idea.like++
          idea.voters.push(msg.from.id);
          idea.markModified('voters')
          idea.save()

          var opt = {
            message_id: msg.message.message_id,
            chat_id: msg.message.chat.id,
            reply_markup: JSON.stringify({
              inline_keyboard:  [
                [{text: '\u{2764} Love - ' + idea.love, callback_data:'love'},
                {text:'\u{1F44D} Like - ' + idea.like, callback_data:'like'},
                {text:'\u{1F44E} Dislike - ' + idea.dislike, callback_data:'dislike'}]
              ],
              resize_keyboard: false,
            })
          }
          bot.editMessageText(
            msg.message.text,
            opt
          );
        }
        else if (idea && idea.voters.includes(msg.from.id)) {
            bot.answerCallbackQuery(msg.id, 'Вы больше не можете голосовать за эту идею!')
        }
      })
      break
    case 'love':
      var text = msg.message.text.split('\n')
      Ideas.findOne({text: text[1]}).exec(function (err, idea) {
        if (idea && !idea.voters.includes(msg.from.id)) {
          idea.love++
          idea.voters.push(msg.from.id);
          idea.markModified('voters')
          idea.save()
          var opt = {
            message_id: msg.message.message_id,
            chat_id: msg.message.chat.id,
            reply_markup: JSON.stringify({
              inline_keyboard:  [
                [{text: '\u{2764} Love - ' + idea.love, callback_data:'love'},
                {text:'\u{1F44D} Like - ' + idea.like, callback_data:'like'},
                {text:'\u{1F44E} Dislike - ' + idea.dislike, callback_data:'dislike'}]
              ],
              resize_keyboard: false,
            })
          }
          bot.editMessageText(
            msg.message.text,
            opt
          );
        }
        else if (idea && idea.voters.includes(msg.from.id)) {
            bot.answerCallbackQuery(msg.id, 'Вы больше не можете голосовать за эту идею!')
        }
      })
      break
    case 'dislike':
        var text = msg.message.text.split('\n')
        Ideas.findOne({text: text[1]}).exec(function (err, idea) {
          if (idea && !idea.voters.includes(msg.from.id)) {
            idea.dislike++
            idea.voters.push(msg.from.id);
            idea.markModified('voters')
            idea.save()
            var opt = {
              message_id: msg.message.message_id,
              chat_id: msg.message.chat.id,
              reply_markup: JSON.stringify({
                inline_keyboard:  [
                  [{text: '\u{2764} Love - ' + idea.love, callback_data:'love'},
                  {text:'\u{1F44D} Like - ' + idea.like, callback_data:'like'},
                  {text:'\u{1F44E} Dislike - ' + idea.dislike, callback_data:'dislike'}]
                ],
                resize_keyboard: false,
              })
            }
            bot.editMessageText(
              msg.message.text,
              opt
            );
          }
          else if (idea && idea.voters.includes(msg.from.id)) {
              bot.answerCallbackQuery(msg.id, 'Вы больше не можете голосовать за эту идею!')
          }
        })
        break
  }
})
// Creating the admin-user

// bot.onText(/\/getmeAdmin/, function(msg, match){
// 	var userData = {
// 		telegramID:msg.from.id,
// 		username: msg.from.username.toString(),
// 		first_name: msg.from.first_name.toString(),
// 		last_name: msg.from.last_name.toString(),
// 		vip: true,
// 		admin:true
// 	}
// 	Users.findOne({telegramID:msg.from.id}, function(err, admin){
// 		if(err) bot.sendMessage(msg.from.id, 'Произошла ошибка: '+ err)
// 		if(admin){
// 			bot.sendMessage(msg.from.id, "Вы уже администратор этого бота")
// 		} else{

// 			var newAdmin = new Users(userData)
// 			newAdmin.save(function(err, saved){
// 				if(err) bot.sendMessage(msg.from.id, 'Произошла ошибка: '+ err)
// 				if(saved) bot.sendMessage(msg.from.id, 'Вы зарегистрированы как новый админ')
// 			})
// 		}
// 	})
// })

// bot.onText(/\/createhall/, function(msg, match){
// var halls = [
// 			{
// 				name:'Мугалжар',
// 				level:'10',
// 				number:'1',
// 				vip:'false'
// 			},
// 			{
// 				name:'Алтай',
// 				level:'25',
// 				number:'2',
// 				vip:'false'
// 			},
// 			{
// 				name:'Туркестан',
// 				level:'26',
// 				number:'3',
// 				vip:'false'
// 			},
// 			{
// 				name:'Coffice (Кофис)',
// 				level:'26',
// 				number:'4',
// 				vip:'false'
// 			},
// 			{
// 				name:'Жетысу',
// 				level:'27',
// 				number:'5',
// 				vip:'false'
// 			},
// 			{
// 				name:'Проектор EPSON переносной',
// 				number:'6',
// 				vip:'false'
// 			},
// 			{
// 				name:'Сары-Арка',
// 				level:'26',
// 				number:'7',
// 				vip:'true'
// 			}
// 		]
// 		halls.map(function(hall){
// 			Hall.findOne({name: hall.name}, function(err, found){
// 				if(err){
// 					bot.sendMessage(msg.from.id, 'Произошла ошибка: '+ err)
// 				}else if(found) {
// 					bot.sendMessage(msg.from.id, 'Залы уже существуют! Пожалуйста наберите /start')
// 				} else{
// 					var newHall = new Hall(hall)
// 					newHall.save(function(err){
// 						if(err) bot.sendMessage(msg.from.id, 'Произошла ошибка: '+ err)
// 					})
// 				}
// 			})
// 		})
// 		bot.sendMessage(msg.from.id, 'Пожалуйста наберите /start 127')
// })

// Creating users

bot.onText(/\/createvip/, function(msg, match){
	var user = {
		telegramID:msg.from.id,
		username:msg.from.username,
		firstName:msg.from.first_name,
		lastName:msg.from.last_name,
		vip:true
	}
	var newUsers = new Users(user)
	newUsers.save(function(err, saved){
		if(err){
			bot.sendMessage(msg.from.id, 'Произошла ошибка: '+ err)
		} else if(saved){
			bot.sendMessage(msg.from.id, 'Вы удачно зарегистрировались!')
		}
	})
})
