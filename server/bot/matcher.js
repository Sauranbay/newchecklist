  function commandHandler ( commands, text, callback) {
    var max = {
      id: null,
      percent: -1,
      callback: null
    }
    commands.forEach(function (cmd, i) {
      var compared = isMatching(text, cmd.command)
      if( compared.percent >= max.percent ) {
        max.percent = compared.percent;
        max.callback = cmd.callback;
        max.id = cmd.id;
      }
    })
    if(max.percent == 0){
      if(callback)
      callback(null, max.percent);
    } else {
      if(max.callback)
      max.callback(max.id, max.percent);
      if(callback)
      callback(max.id, max.percent);
    }
  }

  function isMatching (rawText, target) {
    if(target.length >= rawText.length){
      var targ = rawText
      var subjects = target.split(/\s/i);
    } else {
      var targ = target
      var subjects = rawText.split(/\s/i);
    }

    var total = subjects.length;
    var current = 0;
    subjects.forEach(function (subj) {
      subj.replace(/\P{L}/i, '')
      if( subj != '' &&  new RegExp(subj, 'i').test(targ) ) {
        current++;
      } else if( subj != '' &&  new RegExp(targ, 'i').test(subj) ) {
        current++;

      }
    })
    return { percent: current*100/total, matched: current, total }
  }
  /*
  var commands = [
    {
      id:'first',
      command: 'Привет',
      callback: cb
    },
    {
      id:'second',
      command: 'Привет пидр',
      callback: cb
    },
    {
      id:'third',
      command: 'чтоб ты сдох, урод',
      callback: cb
    }
]
function cb ( id, percent ) {
  console.log('Yay! id',id,'is matched by ', percent);
}
 commandHandler(commands, 'эй, ебаный пидрила, че, привет чтоли?')
 */
module.exports.commandHandler = commandHandler
