var cron = require('node-cron')
var request  = require('request')
var Weather = require('../models/weather.js')
var Currency = require('../models/currency.js')

function gets(){
    var url = "http://api.openweathermap.org/data/2.5/group?id=1526273,1526384&units=metric&lang=ru&appid=e29f5af5749c6b22b71aeaeb32330fce"
    request({
      method: "GET",
      json:true,
      url: url
    }, function(err, response){
      response.body.list.map(function(city){
        var weatherData = {
          city: city.name,
          date: new Date().getTime(),
          dateString:new Date(),
          wind_speed:city.wind.speed,
          temp: city.main.temp,
          pressure: city.main.pressure,
          temp_min: city.main.temp_min,
          temp_max: city.main.temp_max,
          humidity:city.main.humidity,
          main: city.weather[0].description
        }
        var newWeather = new Weather(weatherData)
        newWeather.save(function(err, saved){
          if(err) console.log(err)
        })
      })
    })
  var url = "http://apilayer.net/api/live?access_key=7e01d869bd0b06eaeab427317fd28973&currencies=KZT,CNY,EUR,GBP,KGS,BTC,RUB"
    request({
      method: "GET",
      json:true,
      url: url
    }, function(err, response){
      var currencyData = {
        date: new Date().getTime(),
        dateString:new Date(),
        USDKZT:response.body.quotes.USDKZT,
        USDEUR:response.body.quotes.USDEUR,
        USDGBP:response.body.quotes.USDGBP,
        USDKGS:response.body.quotes.USDKGS,
        USDBTC:response.body.quotes.USDBTC,
        USDRUB:response.body.quotes.USDRUB
      }
      var newCurrency = new Currency(currencyData)
      newCurrency.save(function(err){
        if(err) console.log(err)
      })
    })
}
   // gets()

cron.schedule('0 0,18 * * *', gets)

cron.schedule('0 0,18 * * *', function(){
  Weather.find({})
  .where('date').lte(new Date().getTime() - 86400000).remove()
  Currency.find({})
  .where('date').lte(new Date().getTime() - 86400000).remove()
})
