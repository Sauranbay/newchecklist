var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var underRegion = new Schema({
    name: String,
    short_name: {type: String, unique: true},
    head: {type: Boolean, default: false},
    region: {type: mongoose.Schema.Types.ObjectId, ref: 'Region'},
    departments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Department'}]
},{
    timestamp:true
    // usePushEach: true
});

module.exports = mongoose.model('underRegion', underRegion);
