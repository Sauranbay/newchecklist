var mongoose = require('mongoose');
var scheduleSchemas = new mongoose.Schema({
    hallId:{type: mongoose.Schema.Types.ObjectId, ref: 'Hall'},
    duration:Number,
    period:String,
    secondPeriod:String,
    timeFrom:Number,
    timeFor:Number,

    stringFrom:String,
    stringFor:String,
    times: Array,
    date:Number,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}
})
module.exports = mongoose.model('Schedule', scheduleSchemas);
