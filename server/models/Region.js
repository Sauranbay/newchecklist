var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var Region = new Schema({
    name: String,
    short_name: {type: String, unique: true},
    u_regions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'underRegion'}]
},{
    timestamp:true
    // usePushEach: true
});

module.exports = mongoose.model('Region', Region);
