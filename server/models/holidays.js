'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var holidays = new Schema({
    year: { type: Number },
    month: { type: Number },
    holidays: { type: Array },
},{
    timestamp:true
});

module.exports = mongoose.model('holidays', holidays);
