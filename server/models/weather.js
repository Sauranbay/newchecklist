var mongoose = require('mongoose')
var weatherSchemas = new mongoose.Schema({
    city: String,
    date: Number,
    dateString:String,
    wind_speed:Number,
    temp: Number,
    pressure: Number,
    temp_min:Number,
    temp_max: Number,
    humidity:String,
    main: String
})
module.exports = mongoose.model('Weather', weatherSchemas)