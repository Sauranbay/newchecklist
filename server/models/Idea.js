var mongoose = require("mongoose");


var ideaScheme = new mongoose.Schema({
  text: {type: String, unique: true},
  username: String,
  chat_name: String,
  chat_id: String,
  from_id: String,
  love: Number,
  like: Number,
  dislike: Number,
  voters: Array
});

module.exports = mongoose.model('Idea', ideaScheme);
