'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var Employee = new Schema({
    employee_id: { type: String, unique: true},
    botId: {type: String, unique: true},
    firstname: {type: String, default:'NoName'},
    lastname: {type: String, default: 'NoLastname'},
    fathername: {type: String, default: ''},
    disabled: {type: Boolean, default: false},
    competencies: [],
    type: {type: String, default: 'fixed'},
    fixT: {type: Number, default: 420},
    fixST: {type: Number, default: 300},
    bonusPercent: {type: Number, default: 75},
    registered_at: { type: Date, default: Date.now },
    checked: { type: Boolean, default:false },
    checkedIn: Date,
    checkedOut: Date,
    holidays: {type: Array},
    department: { type: mongoose.Schema.Types.ObjectId, ref: "Department" },
    department_id: String,
    u_region: { type: mongoose.Schema.Types.ObjectId, ref: "underRegion" },
    position: { type: mongoose.Schema.Types.ObjectId, ref: "vacancy" },
    vacancy: String,
    // company: String,
    // position: String,
    condition: String,
    // workType: String,
    // workPeriod: String,
    request: { type: mongoose.Schema.Types.ObjectId, ref: "Request" },
    admin: { type: Boolean, default: false},
    salary_fixed: {type: Number},
    salary: Number,
    // salaryFull: Number,
    confirmed: { type: Boolean, default:false },//new Property
    updated: Date,
    days: Array,
    salaryOfMonth: Array,
    tgname: String,
    gender: String,
    vacation: { type: Boolean, default:false }
},{
    timestamp:true
    // usePushEach: true
});

Employee.pre('save', function (next) {
    var currentDate = new Date();
    this.updated = currentDate;
    next();
});

Employee.methods.disableEmployee = function () {
  this.disabled = true;
};

Employee.methods.enableEmployee = function () {
  this.disabled = false;
};

Employee.methods.getName = function () {
  return this.firstname + ' ' + this.lastname;
};

Employee.methods.getSuffix = function () {
  if (!this.gender)
    return '(а)';
  else {
    return this.gender == 'female'? 'а' : '' ;
  }
};

Employee.methods.getSuffixElLa = function () {
  if (!this.gender)
    return 'ла(-ел)';
  else
    return this.gender == 'female'? 'ла' : 'ел' ;
};

Employee.methods.getSuffixSyaAs = function () {
  if (!this.gender)
    return 'ся(-ась)' ;
  else
    return this.gender == 'female'? 'ась' : 'ся' ;
};

Employee.methods.getSuffixHimHer = function () {
  if (!this.gender)
    return 'ее(его)' ;
  else
    return this.gender == 'female'? 'ее' : 'его' ;
};

Employee.methods.getId = function(){
    return this.id;
};

Employee.methods.getBotId = function(){
    return this.botId;
};

Employee.methods.generateCode = function(){
  var code = [];
  var index = Math.floor(Math.random() * (9 - 8) + 8);

  for(var i = 0; i < index; ++i){
    var number = Math.floor(Math.random() * (9000 - 1000) + 1000);
    code[i] = number;
  }
  return code;
};


module.exports = mongoose.model('Employee', Employee);
