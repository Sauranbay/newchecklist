'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var vacancy = new Schema({
    name: { type: String },
    active: {type: Boolean, default: true},
    department: {type: mongoose.Schema.ObjectId, ref:'department'}
},{
    timestamp:true
});

module.exports = mongoose.model('vacancy', vacancy);
