var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var Department = new Schema({
    name: String,
    short_name: {type: String, unique: true},
    u_region: {type: mongoose.Schema.Types.ObjectId, ref: 'underRegion'},
    employees: [{type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}]
},{
    timestamp:true
    // usePushEach: true
});

module.exports = mongoose.model('Department', Department);
