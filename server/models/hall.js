var mongoose = require('mongoose');
var hallSchemas = new mongoose.Schema({
    name:String,
    number:Number,
    level:Number,
    vip:Boolean
})
module.exports = mongoose.model('Hall', hallSchemas);