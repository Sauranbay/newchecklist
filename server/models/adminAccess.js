'use strict';

var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;
//var passportLocalMongoose = require('passport-local-mongoose');

var adminAccess = new Schema({
    id: { type: String, unique: true },
    hash: String,
    createdAt: { type: Date, expires: 86400, default: Date.now }
},{
    timestamp:true
});

module.exports = mongoose.model('adminAccess', adminAccess);
