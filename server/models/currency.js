var mongoose = require('mongoose')
var currencySchemas = new mongoose.Schema({
    date: Number,
    USDKZT:Number,
    USDEUR:Number,
    USDGBP:Number,
    USDKGS:Number,
    USDBTC:Number,
    USDRUB:Number,
    dateString:String
})
module.exports = mongoose.model('Currency', currencySchemas)