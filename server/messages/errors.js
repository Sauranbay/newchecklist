var error_messages = {
	error: 'Ошибка на сервере',
	profile_getting_error: 'Ошибка профайла. Попробуйте снова, либо обратитесь в поддержку',
	profile_update_error: 'Ошибка обновления профайла, попробуйте снова!',
	volume_deposit_error: 'Ошибка добавления обьемов',
	volume_withdraw_error: 'Ошибка вывода обьемов',
	volume_info_error: 'Ошибка. Информация не найдена',
	order_place_error: 'Ошибка. Невозможно совершить ордер!',
	check_error: 'Ошибка. Данные не верны!',
	performing_order_error: 'Ошибка исполнения ордера. Performing Order Error.'
}





module.exports = {
	error_messages
}