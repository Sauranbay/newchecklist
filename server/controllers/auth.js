const User = require('../models/User')
const Profile = require('../models/Profile')
const Volume = require('../models/Volume')
const Config = require('../configs/configs')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt-nodejs')

function signup (req, res) {
  User.findOne({email: req.body.email}, (err, user) => {
    if (err) {
      res.status(400).send({message: 'Неизвестная ошибка, попробуйте позже'})
    } else if (user && user._id) {
      res.status(400).send({message: 'Пользователь с таким электронным адресом уже существует'})
    } else {
      const newUser = new User({
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password),
        name: req.body.name
      })

      newUser.save((err, result) => {
        if (err) {
          res.status(400).send({message: 'Неизвестная ошибка, попробуйте позже'})
        } else {
          const newProfile = new Profile({
            user: result._id
          })

          newProfile.save((err, result1) => {
            if (err) {
              res.status(400).send({message: 'Неизвестная ошибка, попробуйте позже'})
            } else {
              const newVolume = new Volume({
                user: result._id
              })

              newVolume.save((err, result2) => {
                if (err) {
                  res.status(400).send({message: 'Неизвестная ошибка, попробуйте позже'})
                } else {
                  res.status(200).send({message: 'Вы успешно зарегистрировались!'})
                }
              })
            }
          })

        }
      })
    }
  })
}

function signin (req, res) {
  User.findOne({email: req.body.email}, (err, user) => {
    if (err) res.status(400).send({data: null, message: 'Неизвестная ошибка, попробуйте позже'})
    if (!user) res.status(400).send({data: null, message: 'Пользователь не найден'})
    else {
      if (!bcrypt.compareSync(req.body.password, user.password)) {
        res.status(400).send({data: null, message: 'Неверный пароль или почта'})
      } else {
        const payload = {
          _id: user._id,
          email: user.email
        }
        const token = jwt.sign(payload, Config.auth.JWT_SECRET, {
          expiresIn: Config.auth.tokenExpiry
        })
        res.status(200).send({data: {token: token}, message: ''}).end()
      }
    }
  })
}

module.exports = {
  signup,
  signin
}
