const Profile = require('../models/Profile')
const { error_messages } = require('../messages/errors')

function get_profile (req, res) {
  Profile.findOne({user: req.user._id}, (err, profile_result) => {
    if(err) {
      res.status(400).send({message: error_messages.profile_getting_error})
    } else {
      res.status(200).send({profile: profile_result})
    }
  })
}

function main_info (req, res, next) {
  Profile.updateOne({user: req.user._id}, {$set: {
    main_info_company_name: req.body.main_info_company_name ? req.body.main_info_company_name : '',
    main_info_company_address: req.body.main_info_company_address ? req.body.main_info_company_address : '',
    main_info_business_form: req.body.main_info_business_form ? req.body.main_info_business_form : '',
    requisites_checking_account: req.body.requisites_checking_account ? req.body.requisites_checking_account : '',
    requisites_bank_ben: req.body.requisites_bank_ben ? req.body.requisites_bank_ben : '',
    requisites_bik: req.body.requisites_bik ? req.body.requisites_bik: '',
    requisites_iin_bin: req.body.requisites_iin_bin ? req.body.requisites_iin_bin : ''
  }}, (err, newMainInfo) => {
    if(err){
      res.status(400).send({message: error_messages.profile_update_error})
    } else {
      next()
    }
  })
}

function contacts (req, res, next) {
  Profile.updateOne({user: req.user._id}, {$set: {
    contacts_fio: req.body.contacts_fio ? req.body.contacts_fio : '',
    contacts_phone: req.body.contacts_phone ? req.body.contacts_phone : ''
  }}, (err, newMainInfo) => {
    if(err){
      res.status(400).send({message: error_messages.profile_update_error})
    } else {
      next()
    }
  })
}

function logo (req, res, next) {
  Profile.updateOne({user: req.user._id}, {$set: {
    logo: req.body.logo ? req.body.logo : ''
  }}, (err, newMainInfo) => {
    if(err){
      res.status(400).send({message: error_messages.profile_update_error})
    } else {
      next()
    }
  })
}

function documents (req, res, next) {
  Profile.updateOne({user: req.user._id}, {$set: {
    documents: req.body.docs ? req.body.docs : ''
  }}, (err, newMainInfo) => {
    if (err) {
      res.status(400).send({message: error_messages.profile_update_error})
    } else {
      next()
    }
  })
}

module.exports = {
  get_profile,
  main_info,
  contacts,
  logo,
  documents
}
