const User = require('../models/User')
const Profile = require('../models/Profile')
const Volume = require('../models/Volume')
const { error_messages } = require('../messages/errors')

// Ввод обьемов
function depositVolume (req, res, next) {
  Volume.updateOne({user: req.user._id}, {$inc: {[req.body.volume_type]: parseInt(req.body.volume)}}, (err, result) => {
    if(err){
      res.status(404).send({message: error_messages.volume_deposit_error})
    } else {
      next()
    }
  })
}


// Проверка возможности вывести обьемы
function withdrawVolumeCheck(req, res, next) {
  Volume.findOne({user: req.user._id}, (err, result) => {
    if(err){
      res.status(404).send({message: error_messages.volume_withdraw_error})
    } else if(result && result[req.body.volume_type] < parseInt(req.body.volume)){
      res.status(404).send({message: error_messages.volume_withdraw_error})
    } else {
      next()
    }
  })
}


// Вывод обьемов
function withdrawVolume (req, res, next) {
  Volume.updateOne({user: req.user._id}, {$inc: {[req.body.volume_type]: parseInt(req.body.volume) * (-1)}}, (err, result) => {
    if(err){
      res.status(404).send({message: error_messages.volume_withdraw_error})
    } else {
      next()
    }
  })
}


// Информация о обьемах
function getInfo (req, res) {
  Volume.findOne({user: req.user._id}, (err, result) => {
    if(err) {
      res.status(404).send({message: error_messages.volume_info_error})
    } else {
      res.status(200).send([
        {
          volume_type: 'kzt_volume',
          name: 'Тенге',
          short_name: 'KZT',
          volume: result.kzt_volume
        }, {
          volume_type: 'pot_volume',
          name: 'Помидор',
          short_name: 'POT',
          volume: result.pot_volume
        }, {
          volume_type: 'apple_volume',
          name: 'Яблоко',
          short_name: 'APPLE',
          volume: result.apple_volume
        }
      ])
    }
  })
}


module.exports = {
  depositVolume,
  withdrawVolumeCheck,
  withdrawVolume,
  getInfo
}
