const User = require('../models/User')
const VolumeAction = require('../models/VolumeAction')
const { error_messages } = require('../messages/errors')

// Запись о вводе обьемов
function depositVolume (req, res) {
  const newAction = new VolumeAction({
  	user: req.user._id,
  	action_type: 1,
  	volume_type: req.body.volume_type,
  	volume: req.body.volume
  })

  newAction.save((err, result) => {
  	if(err) {
  		res.status(404).send({message: error_messages.volume_deposit_error})
  	} else {
  		res.status(200).send({ok: true})
  	}
  })
}

// Запись о выводе обьемов
function withdrawVolume (req, res) {
  const newAction = new VolumeAction({
  	user: req.user._id,
  	action_type: -1,
  	volume_type: req.body.volume_type,
  	volume: req.body.volume
  })

  newAction.save((err, result) => {
  	if(err) {
  		res.status(404).send({message: error_messages.volume_deposit_error})
  	} else {
  		res.status(200).send({ok: true})
  	}
  })
}

module.exports = {
  depositVolume,
  withdrawVolume
}
