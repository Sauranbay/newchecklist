const Order = require('../models/Order')
const Volume = require('../models/Volume')
const FrozenVolume = require('../models/FrozenVolume')
const { error_messages } = require('../messages/errors')


// ФУНКЦИИ ДЛЯ ПОКУПКИ


// Проверка входных данных на покупку
function check0LevelBuyOrder(req, res, next) {
	/**
	 * Проверка входных данных на соответствие по типу.
	 *
	 * req.body должен содержать:
	 * - currency_type - вид валюты, которая привязана к ордеру;
	 * - price - цена за единицу обьема;
	 * - volume_type - вид товара, который привязан к ордеру;
	 * - volume - обьем товара;
	 * 
	*/
	if(
		req.body.currency_type && typeof req.body.currency_type == 'string' &&
		req.body.price && Number(req.body.price) &&
		req.body.volume_type && typeof req.body.currency_type == 'string' &&
		req.body.volume && Number(req.body.volume)
	) {
		req.order = {}
		req.order.currency_type 	= req.body.currency_type
		req.order.price 			= Number(req.body.price)
		req.order.volume_type 		= req.body.volume_type
		req.order.volume 			= Number(req.body.volume)
		req.order.money 			= Number(req.order.volume) * Number(req.order.price)
		req.order.order_type 		= 'buy'		
		console.log('done check0LevelBuyOrder')
		next()
	} else {
		res.status(404).send({message: error_messages.check_error})
	}
}

// Проверка возможности купить обьемы 
function checkBuyOrderAbility(req, res, next) {
	/**
	 * Проверка наличия достаточного количества средств;
	 *
	 * req.user должен содердать:
	 * - _id - id пользователя, совершающего операцию;
	 *
	 * req.order должен содержать:
	 * - currency_type - вид валюты, которая привязана к ордеру;
	 * - money - сумма на которую производится покупка обьемов;
	 * 
	*/
	Volume.findOne({user: req.user._id}, (err, result) => {
		if(err){
		  	res.status(404).send({message: error_messages.order_place_error})
		} else if(result && result[req.order.currency_type] < req.order.money){
		  	res.status(404).send({message: error_messages.order_place_error})
		} else {
			console.log('done checkBuyOrderAbility')
		  	next()
		}
	})
}

// Замораживание средств, под ордер покупки.
function freezeVolumesBuyOrder(req, res, next) {
	/**
	 * Списание средств со счетов под ордер;
	 *
	 * req.user должен содердать:
	 * - _id - id пользователя, совершающего операцию;
	 *
	 * req.order должен содержать:
	 * - currency_type - вид валюты, которая привязана к ордеру;
	 * - money - сумма на которую производится покупка обьемов;
	 * - _id - id ордера
	 *
	*/
	Volume.findOneAndUpdate({user: req.user._id}, {$inc: {[req.order.currency_type]: req.order.money * (-1)}}, (err, result) => {
		if(err) {
			res.status(404).send({message: error_messages.order_place_error})
		} else {
			next()
		}
	})
}

function checkBuyOrderToPerform(req, res, next){
	// Проверяем есть ли подходящий ордер и закрываем его если нашли
	Order.findOneAndUpdate({
		order_type: 		'sell',
		volume_type: 		req.order.volume_type,
		volume: 			req.order.volume,
		currency_type: 		req.order.currency_type,
		price: 				req.order.price,
		status: 			1
	}, {$set: {
		status: 		2
	}}, (err, order_sell) => {
		if(order_sell && order_sell._id) {
			// Закрываем текущий ордер на покупку
			Order.findOneAndUpdate({_id: req.order._id}, {$set: {status: 2}}, (err, order_buy) => {
				if(err) res.status(404).send({message: error_messages.order_place_error})
				else {
					performingOrder(order_sell, order_buy, () => {
						res.status(200).end()
					})
				}
			})
		} else {
			Order.updateOne({_id: req.order._id}, {$set: {status: 1}}, (err, done) => {
				res.status(200).end()
			})	
		}
	})
}



// ФУНКЦИИ ДЛЯ ПРОДАЖИ


// Проверка входных данных на продажу
function check0LevelSellOrder(req, res, next) {
	/**
	 * Проверка входных данных на соответствие по типу.
	 *
	 * req.body должен содержать:
	 * - currency_type - вид валюты, которая привязана к ордеру;
	 * - price - цена за единицу обьема;
	 * - volume_type - вид товара, который привязан к ордеру;
	 * - volume - обьем товара;
	 * 
	*/
	if(
		req.body.currency_type && typeof req.body.currency_type == 'string' &&
		req.body.price && Number(req.body.price) &&
		req.body.volume_type && typeof req.body.currency_type == 'string' &&
		req.body.volume && Number(req.body.volume)
	) {
		req.order = {}
		req.order.currency_type 	= req.body.currency_type
		req.order.price 			= Number(req.body.price)
		req.order.volume_type 		= req.body.volume_type
		req.order.volume 			= Number(req.body.volume)
		req.order.money 			= Number(req.body.volume) * Number(req.body.price)
		req.order.order_type 		= 'sell'		
		console.log('done check0LevelBuyOrder')
		next()
	} else {
		res.status(404).send({message: error_messages.check_error})
	}
}

// Проверка возможности продать обьемы 
function checkSellOrderAbility(req, res, next) {
	/**
	 * Проверка наличия достаточного количества обьема на продажу;
	 *
	 * req.user должен содердать:
	 * - _id - id пользователя, совершающего операцию;
	 *
	 * req.order должен содержать:
	 * - volume_type - вид товара, который привязан к ордеру;
	 * - volume - обьем товара;
	 * 
	*/
	Volume.findOne({user: req.user._id}, (err, result) => {
		if(err){
		  	res.status(404).send({message: error_messages.order_place_error})
		} else if(result && result[req.order.volume_type] < req.order.volume){
			console.log(result[req.order.volume_type], result[req.order.volume])
		  	res.status(404).send({message: error_messages.order_place_error})
		} else {
			console.log(result[req.order.volume_type], req.order.volume)
			console.log('done checkSellOrderAbility')
		  	next()
		}
	})
}

// Замораживание средств, под ордер продажи.
function freezeVolumesSellOrder(req, res, next) {
	/**
	 * Списание средств со счетов и перевод средств на замороженные счета, под ордер покупки.
	 *
	 * req.user должен содердать:
	 * - _id - id пользователя, совершающего операцию;
	 *
	 * req.order должен содержать:
	 * - volume_type - вид товара, который привязан к ордеру;
	 * - volume - обьем товара;
	 * - _id - id ордера
	 *
	*/
	Volume.findOneAndUpdate({user: req.user._id}, {$inc: {[req.order.volume_type]: req.order.volume * (-1)}}, (err, result) => {
		if(err) {
			res.status(404).send({message: error_messages.order_place_error})
		} else {
			next()
		}
	})
}

function checkSellOrderToPerform(req, res, next){
	// Проверяем есть ли подходящий ордер и закрываем его если нашли
	Order.findOneAndUpdate({
		order_type: 		'buy',
		volume_type: 		req.order.volume_type,
		volume: 			req.order.volume,
		currency_type: 		req.order.currency_type,
		price: 				req.order.price,
		status: 			1
	}, {$set: {
		status: 		2
	}}, (err, order_buy) => {
		if(order_buy && order_buy._id) {
			// Закрываем текущий ордер на продажу
			Order.findOneAndUpdate({_id: req.order._id}, {$set: {status: 2}}, (err, order_sell) => {
				if(err) res.status(404).send({message: error_messages.order_place_error})
				else {
					performingOrder(order_sell, order_buy, () => {
						res.status(200).end()
					})
				}
			})
		} else {
			Order.updateOne({_id: req.order._id}, {$set: {status: 1}}, (err, done) => {
				res.status(200).end()
			})	
		}
	})
}



// ОБШИЕ ФУНКЦИИ


// Размещение ордеров
function placeOrder(req, res, next){
	/**
	 * Создание записи с ордером, для работы с order._id и дальнейшей его активации;
	 *
	 * req.user должен содердать:
	 * - _id - id пользователя, совершающего операцию;
	 *
	 * req.order должен содержать:
	 * - currency_type - вид валюты, которая привязана к ордеру;
	 * - price - цена за единицу обьема;
	 * - volume_type - вид товара, который привязан к ордеру;
	 * - volume - обьем товара;
	 * - order_type - тип ордера (buy или sell);
	*/
	var newOrder = new Order({
	  	user: 				req.user._id,
	  	order_type: 		req.order.order_type,
		volume_type: 		req.order.volume_type,
		volume: 			req.order.volume,
		currency_type: 		req.order.currency_type,
		price: 				req.order.price,
		currency_volume: 	req.order.money
	})

	newOrder.save((err, result) => {
		if(err) {
			res.status(404).send({message: error_messages.order_place_error})
		} else {
			req.order._id = result._id
			console.log('done placeOrder')
			next()
		}
	})	
}

function performingOrder (order_sell, order_buy, next) {
	/**
	 * Закрытие ордеров и.
	 *
	 * - order_sell - ордер, в котором заморожены обемы товара;
	 * - order_buy - ордер, в котором заморожены средства;
	 *
	*/

	// Отдаем деньги пользователю который выставил sell_order
	Volume.findOneAndUpdate({user: order_sell.user}, {$inc: {[order_sell.currency_type]: Number(order_sell.volume) * Number(order_sell.price)}}, (err, res1) => {
		//Отдаем обьемы пользователю который выставил buy_order
		Volume.findOneAndUpdate({user: order_buy.user}, {$inc: {[order_buy.volume_type]: Number(order_buy.volume)}}, (err, res2) => {
			if(err) {
				res.status(404).send({message: error_messages.performing_order_error})
			} else {
				next()
			}

		})
	})
}


function getMarketList(req, res) {
	let result = []
	req.market = {}
	console.log(req.params)
	if(req.params.currency_type == 'kzt'){
		req.market.currency_type = 'kzt_volume'


		getMarket(req, res, 'Картошка', 'POT', 'kzt_volume', 'pot_volume', (data1) => {

			result.push(data1)
			getMarket(req, res, 'Яблоки', 'APPLE', 'kzt_volume', 'apple_volume', (data2) => {
				result.push(data2)
				res.status(200).send(result);
			})
		})

		// if(req.body.volume_type == 'pot'){
		// 	req.market.name = 'Картошка'
		// 	req.market.short_name = 'POT'
		// 	req.market.volume_type = 'pot_volume'
		// 	next()
		// } else if(req.body.volume_type == 'apple') {
		// 	req.market.name = 'Яблоки'
		// 	req.market.short_name = 'APPLE'
		// 	req.market.volume_type = 'apple_volume'
		// 	next()
		// } else {
		// 	res.status(404).end()
		// }


	} else if(req.params.currency_type == 'usd'){
		res.status(404).end()
	} else if(req.params.currency_type == 'euro') {
		res.status(404).end()
	}
}


function getMarket(req, res, d_name, d_short_name, d_currency_type, d_volume_type, next) {
	let data = {
		total_volume: 0,
		name: d_name,
		short_name: d_short_name,
		price: 0,
		change_price: 0
	}

	Order.aggregate([
		{ 
			$match: {
		        currency_type: d_currency_type,
		        volume_type: d_volume_type,
		        order_type: 'sell',
		        status: 1
		    }
		},
	    { 
	    	$group: {
                _id: null,
                "volume": { $sum: "$volume" }
            }
        }
	]).exec((err, result) => {
		data.total_volume = result.length > 0 ? result[0].volume : 0

    	if(err) {
    		res.status(404).send(err)
    	} else {
    		Order.find({
    			currency_type: d_currency_type,
		        volume_type: d_volume_type,
		        order_type: 'sell',
		        status: 1
		    }).sort({
		    	price: 1
		    }).limit(1).exec((err, price_result) => {
		    	data.price = price_result.length > 0 ? price_result[0].price : 0


		    	Order.find({
	    			currency_type: d_currency_type,
			        volume_type: d_volume_type,
			        order_type: 'sell',
			        status: {$in: [1, 2]},
			        time: {$lt: (new Date()).getTime() - 11000000}//86400000}
			    }).sort({
			    	price: 1,
			    	time: -1
			    }).limit(1).exec((err, price_change_result) => {
			    	// тут косяк надо правильно высчитывать изменение
			    	if(price_change_result && price_change_result[0]) {
			    		data.change_price = data.price*100/price_change_result[0].price - 100
			    	}
			    	next(data)
			    })

		    })
    	}
    })
}



function getMyBuyOrders(req, res) {
	Order.find({
		user: req.user._id,
		currency_type: req.body.currency_type,
        volume_type: req.body.volume_type,
        order_type: 'buy',
        status: 1
    }).sort({
    	time: -1
    }).limit(10).exec((err, result) => {
    	if(err) {
    		res.status(404).send({message: error_messages.error})
    	} else {
    		res.status(200).send(result)
    	}
    })
}

function getMySellOrders(req, res) {
	Order.find({
		user: req.user._id,
		currency_type: req.body.currency_type,
        volume_type: req.body.volume_type,
        order_type: 'sell',
        status: 1
    }).sort({
    	time: -1
    }).limit(10).exec((err, result) => {
    	if(err) {
    		res.status(404).send({message: error_messages.error})
    	} else {
    		res.status(200).send(result)
    	}
    })
}

function getBuyOrders(req, res) {
	Order.find({
		currency_type: req.body.currency_type,
        volume_type: req.body.volume_type,
        order_type: 'buy',
        status: 1
    }).sort({
    	price: -1
    }).limit(10).exec((err, result1) => {
    	if(err) {
    		res.status(404).send({message: error_messages.error})
    	} else {
    		Order.aggregate([
				{ 
					$match: {
				        currency_type: req.body.currency_type,
				        volume_type: req.body.volume_type,
				        order_type: 'buy',
				        status: 1
				    }
				},
			    { 
			    	$group: {
		                _id: null,
		                "volume": { $sum: "$volume" }
		            }
		        }
			]).exec((err, result2) => {
				if(err) res.status(404).send({message: error_messages.error})
				else if(result2.length > 0) res.status(200).send({total_volume: result2[0].volume, data: result1})
				else res.status(200).send({total_volume: 0, data: result1})
			}) 
    	}
    })
}



function getSellOrders(req, res) {
	Order.find({
		currency_type: req.body.currency_type,
        volume_type: req.body.volume_type,
        order_type: 'sell',
        status: 1
    }).sort({
    	price: 1
    }).limit(10).exec((err, result1) => {
    	if(err) {
    		res.status(404).send({message: error_messages.error})
    	} else {
    		Order.aggregate([
				{ 
					$match: {
				        currency_type: req.body.currency_type,
				        volume_type: req.body.volume_type,
				        order_type: 'sell',
				        status: 1
				    }
				},
			    { 
			    	$group: {
		                _id: null,
		                "currency_volume": { $sum: "$currency_volume" }
		            }
		        }
			]).exec((err, result2) => {
				if(err) res.status(404).send({message: error_messages.error})
				else res.status(200).send({total_volume: result2.length > 0 ? result2[0].currency_volume : 0, data: result1})
			}) 
    	}
    })
}


function getAvailableBalance (req, res) {
	Volume.findOne({user: req.user._id}, (err, result) => {
		if(err) res.status(404).send({message: error_messages.error})
		else res.status(200).send({available_volume: result[req.body.volume_type], available_currency: result[req.body.currency_type]})
	})
}

function getMyActiveOrders (req, res) {
	Order.find({
		user: req.user._id,
		currency_type: req.body.currency_type,
        volume_type: req.body.volume_type,
		status: 1
	}).sort({time: 1}).exec((err, result) => {
		if(err) res.status(404).send({message: error_messages.error})
		else {
			Order.aggregate([
				{ 
					$match: {
				        currency_type: req.body.currency_type,
				        volume_type: req.body.volume_type,
				        order_type: 'buy',
				        status: 1
				    }
				},
			    { 
			    	$group: {
		                _id: null,
		                "currency_volume": { $sum: "$currency_volume" }
		            }
		        }
			]).exec((err, result1) => {
				if(err) res.status(404).send({message: error_messages.error})
				else {
					Order.aggregate([
						{ 
							$match: {
						        currency_type: req.body.currency_type,
						        volume_type: req.body.volume_type,
						        order_type: 'sell',
						        status: 1
						    }
						},
					    { 
					    	$group: {
				                _id: null,
				                "product_volume": { $sum: "$volume" }
				            }
				        }
					]).exec((err, result2) => {
						if(err) res.status(404).send({message: error_messages.error})
						else {
							res.status(200).send({
								data: result,
								currency_volume: result1.length > 0 ? result1[0].currency_volume : 0,
								product_volume: result2.length > 0 ? result2[0].product_volume: 0
							})
						}
					}) 
				}
			}) 
		}
	})
}


function cancellOrder(req, res) {
	Order.findOne({
		user: req.user._id,
		status: 1,
		_id: req.body.order_id
	}).exec((err, result) => {
		if(result) {
			if(result.order_type == 'sell') {
				Order.updateOne({_id: result._id}, {$set: {status: -1}}, (err, result2) => {
					Volume.findOneAndUpdate({user: req.user._id}, {$inc: {[result.volume_type]: result.volume}}, (err, result3) => {
						if(err) res.status(404).send({message: error_messages.error})
						else res.status(200).end()
					})
				})
			} else if(result.order_type == 'buy'){
				Order.updateOne({_id: result._id}, {$set: {status: -1}}, (err, result2) => {
					Volume.findOneAndUpdate({user: req.user._id}, {$inc: {[result.currency_type]: result.currency_volume}}, (err, result3) => {
						if(err) res.status(404).send({message: error_messages.error})
						else res.status(200).end()
					})
				})
			} else {
				res.status(404).send({message: error_messages.error})
			}
		}
	})
}


module.exports = {
	check0LevelBuyOrder,
	checkBuyOrderAbility,
  	placeOrder,
  	freezeVolumesBuyOrder,
  	checkBuyOrderToPerform,

  	check0LevelSellOrder,
  	checkSellOrderAbility,
  	freezeVolumesSellOrder,
  	checkSellOrderToPerform,

  	getMarketList,

  	getMyBuyOrders,
  	getMySellOrders,
  	getBuyOrders,
  	getSellOrders,

  	getAvailableBalance,
  	getMyActiveOrders,

  	cancellOrder
}