import {
    GETEMPLOYEE,
    GETEMPLOYEEINFO,
    GETEMPLOYEEINFOFORMONTH,
    EDITEMPLOYEE, 
    SAVEEMPLOYEE,
    DISABLEEMPLOYEE
} from '../constants/Employee'

const initialState = {
    employees: [],
    employeeInfo:[],
    employeeInfoForMonth:[],
    editedEmployee: []
}

export default function Employee (state=initialState, action){
    switch(action.type){
        case GETEMPLOYEE:
            return{
                ...state,
                employees: action.payload
            }
            case GETEMPLOYEEINFO:
            return{
                ...state,
                employeeInfo: action.payload,
                editedEmployee:action.payload
                
            }
            case GETEMPLOYEEINFOFORMONTH:
            return{
                ...state,
                employeeInfoForMonth: action.payload,
                
            }
            case EDITEMPLOYEE:
            return {
                ...state,
                editedEmployee:{
                    ...state.editedEmployee,
                    employee:{
                        ...state.editedEmployee.employee,
                        [action.payload.target.name]: action.payload.target.value
                    }
                    
                }
            }
            case SAVEEMPLOYEE:
            return {
                ...state,
                answer: action.payload,
            }
            case DISABLEEMPLOYEE:
            return {
                ...state,
                answer: action.payload,
            }
        default:
        return state
    }
}