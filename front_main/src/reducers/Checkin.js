import {CHECKIN, CHECKCODE,SENTIMAGE, GETQUESTIONS, CHECKOUT} from '../constants/Checkin'

const initialState = {
    currentUser:'',
    image:'',
    code:'',
    questions:[],
    checkout:''
}

export default function Checkin (state=initialState, action){
    switch(action.type){
        case CHECKIN:
            return{
                ...state,
                currentUser:action.payload
            }
        case CHECKCODE:
            return{
                ...state,
                code:action.payload
            }
        case SENTIMAGE:
            return{
                ...state,
                image:action.payload
            }
        case GETQUESTIONS:
            return{
                ...state,
                questions:action.payload
            }
        case CHECKOUT:
        return{
            ...state,
            checkout:action.payload
        }
        default:
        return state
    }
}
