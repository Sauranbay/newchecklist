import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import ModalShow from './ModalShow'
import DepartmentList from './DepartmentList'
import webcam from './webcam'
import Employee from './Employee'
import Checkin from './Checkin'
import AdminEmployeeInfo from './AdminEmployeeInfo'


export default combineReducers({
	routing: routerReducer,
	ModalShow,
	DepartmentList,
	webcam,
	Employee,
	Checkin,
	AdminEmployeeInfo,
})
