import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import Header from '../components/Header/Header.js'
import MainContainer from '../components/Main/MainContainer.js'

class Main extends Component {
  constructor(props){
    super(props)
    this.state = {
 
    }
  }
  
  render () {
    return (
      <section>
        <Header />
        <MainContainer />
      </section>
    )
  }
}

export default Main

