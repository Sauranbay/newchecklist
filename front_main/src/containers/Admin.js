import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Switch, Route, Redirect } from 'react-router'
import AdminHeader from '../components/Header/AdminHeader.js'
import DepAndPos from '../components/Department/DepAndPos.js'
import AddDepartment from '../components/Modals/AddDepartment.js';
import DepartmentList from '../components/Modals/DepartmentList.js';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getEmployees from '../actions/Employees'
import * as getEmployeeAdmin from '../actions/AdminEmployeeInfoAction'

import AddPosition from '../components/Modals/AddPosition.js';
import SearchEmployee from '../components/Atoms/SearchEmployee'
import AdminEmployeeInfo from '../components/Atoms/AdminEmployeeInfo'
import AdminTimeCounter from '../components/Atoms/AdminTimeCounter'
import Tasks from '../components/Atoms/Tasks.js';
import Sidebar from '../components/Atoms/Sidebar.js';
import EditEmployee from '../components/Modals/EditEmployee.js';

class Admin extends Component {
    constructor(props){
      super(props)
      this.state = {
        selectedEmployee:'',
        objectId:''
      }
      this.selected=this.selected.bind(this)
    }
  async  selected(employeeId,objectId){
    await  this.setState({
        selectedEmployee:employeeId,
        objectId:objectId
      })
      this.props.getEmployees.getEmployeeInfo(this.state.objectId)
    }
    componentDidMount() {
      this.props.getEmployees.getEmployee()
      this.props.getEmployeeAdmin.getEmployeeForAdmin()
    }
    
    render () {
      return (
         <section className='h-100vh overflowyhidden'>
        {this.props.match.params.parameter=='employee'?
        <div>
          <AdminHeader />
          {this.props.ModalShow.showEditEmployee ?
             <EditEmployee 
             employee={this.props.Employee.employeeInfo.employee}
             departmentName={this.props.Employee.employeeInfo.department_name}
             objectId={this.state.objectId}
             />
            : null
            }
          <div className='flex'>
              <SearchEmployee 
              selected={this.selected}/>
              <div className='flex fl-dc'>
                  <AdminTimeCounter 
                    selectedEmployee={this.state.selectedEmployee}
                    checkedIn={this.props.Employee.employeeInfo.checkedIn}
                    averageTime={this.props.Employee.employeeInfo.averagetime}
                    remainedTime={this.props.Employee.employeeInfo.remainedtime}
                    // employee={this.props.}
                  />
                  <div className='flex'>
                  <AdminEmployeeInfo
                    selectedEmployee={this.state.selectedEmployee}
                    employee={this.props.Employee.employees}
                    fullSalary={this.props.Employee.employeeInfo.employee ? (this.props.Employee.employeeInfo.employee.salaryFull? this.props.Employee.employeeInfo.employee.salaryFull : "0") : '0'} 
                    fixedSalary={this.props.Employee.employeeInfo.employee ? this.props.Employee.employeeInfo.employee.salary_fixed : null}
                    hoursInMonth={this.props.Employee.employeeInfo.employee ? this.props.Employee.employeeInfo.salary.monthHrs : null}
                    departmentName={this.props.Employee.employeeInfo.department_name}
                  />
                  <Tasks/>
                  </div>
              </div>
              <Sidebar 
                employee={this.props.Employee.employeeInfo}
                className='z-index-1'
                employeeId={this.props.Employee.employeeInfo.employee?this.props.Employee.employeeInfo.employee._id :null}
              />
        </div> 
      </div>
      : null}
      {this.props.match.params.parameter=='main' ?
      <div>
          <AdminHeader />
            <DepAndPos 
              employee={this.props.Employee.employees}
              adminEmployee={this.props.AdminEmployeeInfo}
              hour={this.props.AdminEmployeeInfo.employee.hours}
              minutes={this.props.AdminEmployeeInfo.employee.min}
            />
            {this.props.ModalShow.showAddDepartment ?
            <AddDepartment/>
            : null
            }
            {this.props.ModalShow.showDepartmentList ?
            <DepartmentList/>
            : null
            }
            {this.props.ModalShow.showAddPosition ?
            <AddPosition/>
            : null
            }
      </div>
      :null
    
    }
        </section>
      )
    }
  }
  
  function mapStateToProps (state) {
    return {
        Employee: state.Employee,
        ModalShow: state.ModalShow,
        AdminEmployeeInfo: state.AdminEmployeeInfo
    }
  }
  function mapDispatchToProps (dispatch) {
    return {
        getEmployees: bindActionCreators(getEmployees, dispatch),
        getEmployeeAdmin: bindActionCreators(getEmployeeAdmin,dispatch)
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(Admin)
