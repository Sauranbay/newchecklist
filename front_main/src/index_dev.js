import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router' // react-router v4
import { ConnectedRouter } from 'connected-react-router'
import '../../assets/styles/index.css'

import createHistory from 'history/createBrowserHistory'
const history = createHistory()

import configureStore from './store/configureStore'
const store = configureStore(true)
// import store from './store/store'

import Signup from './containers/Signup'
import Approve from './containers/Approve'
import Main from './containers/Main'


import axios from 'axios'
import Admin from './containers/Admin';
import DepartmentAndPosition from './containers/DepartmentAndPosition';
import BigCalendar from './containers/BigCalendar';
import Draft from './containers/Draft';
import EditEmployee from './components/Modals/EditEmployee';

// axios.defaults.baseURL =  'http://192.168.1.155:3000'

// const PrivateRoute = ({ component: Component, ...rest }) => (
//   <Route {...rest} render={(props) => (
//     localStorage.getItem("userId")=== null
//       ?  <Switch><Redirect to='/approvelogin' /></Switch>
//       :<Component {...props}/>
//   )} />
// )

// const PublicRoute = ({ component: Component, ...rest }) => (
//   <Route {...rest} render={(props) => (
//     localStorage.getItem("userId")!= null
//       ? <Component {...props} />
//       : <Redirect to='/approvelogin' />
//   )} />
// )

render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<Switch>
        <Route exact path='/' render={() => (
		  <Redirect to='/approvelogin'/>
        )}/>

        <Route path='/approve:params' component={Approve} />
		<Route path='/main' component={Main} />
		<Route path='/admin:parameter' component={Admin} />
		<Route path='/dep' component={DepartmentAndPosition} />
		<Route path='/calendar' component={BigCalendar} />
			</Switch>
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root')
)
