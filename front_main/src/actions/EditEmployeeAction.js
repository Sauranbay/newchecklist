import {
    EDITEMPLOYEE, SAVEEMPLOYEE
} from '../constants/EditEmployee'
import axios from 'axios'

export function editEmployee(e){
    return function (dispatch) {
		dispatch({
			type: EDITEMPLOYEE,
			payload: e
		})
	}
}
export function saveEmployee (firstname,lastname,fathername,salaryFull,salary_fixed,vacancy,department,fixT,fixST,gender) {
    return function(dispatch){
        axios.post('/employees/editemployee', {
            editedEmployee:{
                firstname,
                lastname,
                fathername,
                salaryFull,
                salary_fixed,
                vacancy,
                department,
                fixT,
                fixST,
                gender
            }
          })
        .then((response)=>{
            dispatch({
                type: SAVEEMPLOYEE,
                payload: response.data
            })
            // if(response.data.succes==true){
            //     window.location.reload()}
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}