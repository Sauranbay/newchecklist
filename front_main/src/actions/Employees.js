import {
    GETEMPLOYEE,
    GETEMPLOYEEINFO,
    GETEMPLOYEEINFOFORMONTH,
    EDITEMPLOYEE, 
    SAVEEMPLOYEE,
    DISABLEEMPLOYEE
} from '../constants/Employee'
import axios from 'axios'
import moment from 'moment'
export function getEmployee(){
    return function(dispatch){
        axios.get('/employees/getemployees')
        .then((response)=>{
            dispatch({
                type: GETEMPLOYEE,
                payload: response.data.employees
            })
    }
    )
    .catch((err)=>
        console.log(err)
    )
    }
}
function transformate(min){
    var h = Math.floor(min/60);
    var m = min - h*60;
    if(m<10){
      return h+":0"+m;
    }else {
      return h+":"+m;
    }
  }
export function getEmployeeInfo(id){
    return function(dispatch){
        axios.get('/employees/getemployee',{
            params: {
                id: id
            }
        }
    )
        .then((response)=>{
            const employee= response.data.employee;
            const checkedIn = moment(employee.checkedIn).format('HH:mm');
            const checkedOut = moment(employee.checkedOut).format('HH:mm');
            const department_name = response.data.department_name;
            const reports = response.data.reports;
            const salary = response.data.salary;
            const hours = salary!=null ?  Math.floor(salary.totalMonthMin/60): '0';
            const minutes = salary!=null ? salary.totalMonthMin - hours*60: '0';
            const remainedtime = salary!=null ? transformate(salary.nedorabotal) : '0';
            const round = salary!=null ? (salary.totalMonthMin*10)/(salary.monthHrs*6): '0';
            const averageMin = salary!=null ? Math.floor(salary.totalMonthMin/salary.byDay.length): '0';
            const reportMonth=reports.month+1;
            const averagetime = transformate(averageMin);
            dispatch({
                type:GETEMPLOYEEINFO,
                payload: {
                    employee, checkedIn, checkedOut, department_name, reports, salary ,hours, minutes, remainedtime, round, averageMin, averagetime, reportMonth
                }
            })
    }
    )
    .catch((err)=>
        console.log(err)
    )
    }
}

export function getEmployeeInfoForMonth(id,date){
    return function(dispatch){
        axios.get('/employees/getemployee',{
            params: {
                id: id,
                selectedMonth: date
            }
        }
    )
        .then((response)=>{
            const employee=response.data.employee;
            const checkedIn = moment(employee.checkedIn).format('HH:mm');
            const checkedOut = moment(employee.checkedOut).format('HH:mm');
            const department_name = response.data.department_name;
            const reports = response.data.reports;
            const salary = response.data.salary;
            const hours = salary!=null ?  Math.floor(salary.totalMonthMin/60): '0';
            const minutes = salary!=null ? salary.totalMonthMin - hours*60: '0';
            const remainedtime = salary!=null ? transformate(salary.nedorabotal) : '0';
            const round = salary!=null ? (salary.totalMonthMin*10)/(salary.monthHrs*6): '0';
            const averageMin = salary!=null ? Math.floor(salary.totalMonthMin/salary.byDay.length): '0';
            const reportMonth=reports.month+1;
            const averagetime = transformate(averageMin);
            dispatch({
                type:GETEMPLOYEEINFOFORMONTH,
                payload: {
                    employee, checkedIn, checkedOut, department_name, reports, salary ,hours, minutes, remainedtime, round, averageMin, averagetime, reportMonth
                }
            })
           
    }
    )
    .catch((err)=>
        console.log(err)
    )
    }
}

export function editEmployee(e){
    return function (dispatch) {
		dispatch({
			type: EDITEMPLOYEE,
			payload: e
		})
	}
}
export function saveEmployee (editedEmployee) {
    return function(dispatch){
        axios.post('/employees/editemployee', {
            editedEmployee: editedEmployee 
          })
        .then((response)=>{
            dispatch({
                type: SAVEEMPLOYEE,
                payload: response.data
            })
            // if(response.data.succes==true){
            //     window.location.reload()}
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

export function disableEmployee (id) {
    return function(dispatch){
        axios.post('/employees/disabledemployee', {
            id: id
          })
        .then((response)=>{
            dispatch({
                type: DISABLEEMPLOYEE,
                payload: response.data
            })
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}