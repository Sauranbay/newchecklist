import {
    ADMINEMPLOYEEINFO
} from '../constants/AdminEmployeeInfo'
import axios from 'axios'
import moment from 'moment'

export function getEmployeeForAdmin(){
    return function(dispatch){
        axios.get('/employees/statisticofmonth',{
        }
    )
        .then((response)=>{
            const averageMin = Math.floor(response.data.minutes/response.data.i);
            const hours = Math.floor(averageMin/60);
            const min = averageMin-hours*60;
            const salaryPerDay = response.data.salary;
            dispatch({
                type:ADMINEMPLOYEEINFO,
                payload: {
                    averageMin, hours, min, salaryPerDay
                }
            })
    }
    )
    .catch((err)=>
        console.log(err)
    )
    }
}
