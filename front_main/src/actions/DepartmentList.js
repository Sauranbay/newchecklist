import {
    DEPARTMENTLIST,
    ADDDEPARTMENT,
    DELETEDEPARTMENT,
    CHECKDEPARTMENT
} from '../constants/DepartmentList'
import axios from 'axios'
import createHistory from 'history/createBrowserHistory'
const history = createHistory({forceRefresh: true})
const checkedDep=[];
//function for getting departments from db
export function getDepartment () {
    return function(dispatch){
        axios.get('/departments/departments')
        .then((response)=>{
            console.log(response.data.departments)
            dispatch({
                type: DEPARTMENTLIST,
                payload: response.data.departments
            })
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}
//check if department is selected
export function checkDepartment(department){
    if (checkedDep.indexOf(department) === -1) {
        checkedDep.push(department);
    } else {
        checkedDep.splice(checkedDep.indexOf(department), 1);
    }
    return function(dispatch){
        dispatch({
            type: CHECKDEPARTMENT,
            payload:checkedDep
        })
    }
}
//function for deleting department
export function deleteDepartment () {
    return function(dispatch){
        axios.post('/departments/deletedepartments', {
            short_name: checkedDep
          })
        .then((response)=>{
            dispatch({
                type: DELETEDEPARTMENT,
                payload: response.data
            })
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

export function addDepartment (departName, shortName) {
    return function(dispatch){
        axios.post('/departments/adddepartment', {
            department_name: departName,
            short_name: shortName
          })
        .then((response)=>{
            dispatch({
                type: ADDDEPARTMENT,
                payload: response.data
            })
            console.log(response.data,'hello')
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

