import axios from 'axios'

import createHistory from 'history/createBrowserHistory'
const history = createHistory({forceRefresh: true})

export function signIn (email, password) {
	return new Promise((resolve, reject) => {
		axios.post('/api/auth/signin', {
			email: email.toLowerCase(),
			password: password
		}).then((response) => {
				localStorage.setItem('token', response.data.data.token)
				resolve(response)
				history.push('/main')
		}).catch((err) => {
				reject(err)
			}
		)
	})
}

// регистрирует нового пользователя
export function signUp (email, password, name) {
  return new Promise((resolve, reject) => {
    axios.post('/api/auth/signup', {
      name: name,
      email: email.toLowerCase(),
      password: password
    }).then((response) => {
			resolve(response)
			history.push('/login')
			// history.push('/inner/v')
		}).catch((err) => {
			reject(err)
			}
		)
	})
}

export function logout () {
	return function (dispatch) {
		localStorage.removeItem('token')
		history.push('/auth/signin')
	}
}

export function isLoggedIn () {
  const token = window.localStorage.getItem('token')
  let payload
  if (token) {
    payload = token.split('.')[1]
    payload = window.atob(payload)
    payload = JSON.parse(payload)
    return payload.exp > Date.now() / 1000
  } else {
    return false
  }
}

export function currentUser () {
  if (isLoggedIn()) {
    const token = window.localStorage.getItem('token')
    let payload = token.split('.')[1]
    payload = window.atob(payload)
    payload = JSON.parse(payload)
    return {
      email: payload.email,
      name: payload.name,
      id: payload.id
    }
  }
}

// export function me () {
// 	return function (dispatch) {
// 		axios.get('/api/auth/me')
// 		.then((response) => {
// 			dispatch({
// 				type: LOGIN_SUCCESS,
// 				payload: response.data
// 			})
// 		}).catch((err) => {
// 				history.push('/auth/signin')
// 			}
// 		)
// 	}
// }
