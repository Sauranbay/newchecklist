import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router' // react-router v4
import { ConnectedRouter } from 'connected-react-router'
import '../../assets/styles/index.css'

import createHistory from 'history/createBrowserHistory'
const history = createHistory()

import configureStore from './store/configureStore'
const store = configureStore(true)

import Signup from './containers/Signup'
import Approve from './containers/Approve'

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path='/' render={() => (
          <Redirect to='/signup' />
        )}/>
        <PublicRoute path='/signup' component={Signup} />
        <PublicRoute path='/approve:params' component={Approve} />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
