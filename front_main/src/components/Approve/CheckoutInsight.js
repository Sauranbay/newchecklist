import React, { Component } from 'react'



class CheckoutInsight extends Component {
  
  constructor (props) {
    super(props)
    this.state = {
      user:JSON.parse(localStorage.getItem('currentUser')),
      insight:'',
      showError:false,
      userId : JSON.parse(localStorage.getItem('userId'))
    }
    this.handleChange=this.handleChange.bind(this)
    this.showError=this.showError.bind(this)
  }
  handleChange(event) {
    this.setState({
        insight: event.target.value
    });
  }
  showError(){
      this.setState({
          showError:true
      })
  }
  render () {
    const reg=/(facebook\.com)\/automato.me(.+)\/([0-9]{3,20})/i;
    return (
      <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' className='ml-16px h-2rem' />
        <div className="w-37rem h-20rem  bgc202d45 mt-5rem br-8px flex fl-dc fl-aic ">
        <div className="pos-rel top-15 flex fl-dc fl-aic">
            <img src={'/userimages/'+this.state.userId+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='br-50 h-5rem w-5rem'/>
            <p className="mt-1rem colorfff fs-14w200 opacity051"> {this.state.user.firstname}!</p>
        </div>
        <div className='flex fl-sb w-30rem'>
            <p className='fs-16w200 colorfff mb-1rem pos-rel top-2rem'>Введите ссылку на инсайт</p>
             <img src='images/next.svg' className="pos-rel w-4rem h-3rem top-2rem left-16px cursor" onClick={reg.test(this.state.insight) ?  ()=> this.props.checkout(this.state.insight) : this.showError}/>
        </div>
          <input type='text' className='signInput w-30rem h-2rem' onChange={this.handleChange}/>
          <div className={this.state.showError? "colorf0475f fs-12w200 mt-1rem" : "hidden"}>вы ввели не правильную ссылку</div>
        </div>
        {/* <div className="flex fl-dr mt-1rem">
            <img src='images/dot.png' className="h-10px pr-5px" />
            <img src='images/dot.png' className="h-10px pr-5px" />
            <img src='images/dot.png' className="h-10px pr-5px" />
            <img src='images/active-dot.png' className="h-10px pr-5px" />
        </div> */}
      </section>
    )
  }
}


export default CheckoutInsight
