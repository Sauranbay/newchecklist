import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as showModalActions from '../../actions/ShowModal'
 
import 'react-datepicker/dist/react-datepicker.css';

class AddPosition extends Component {
  constructor (props) {
    super(props)
    this.state = {
     
    };
  }

  render () {
    return (
      <div className="w-100 h-100vh flex fl-dc fl-c fl-aic bg_161f29_08 pos-abs top-0">
       <div className='h-3rem w-480px bgc354054 mt-2rem br-t-8px flex fl-aic fl-sb text-up pdl-1rem colorfff fs-14w700'>
          <p>добавить должность</p>
          <img src='images/close1.svg' className='w-50px cursor' onClick={() => this.props.showModalActions.showAddPosition(false)}/>
      </div>
      <div className='h-10rem w-480px opacity02 bgc26364f br-b-8px pdx-1rem mb-7rem'>
        <div className='flex fl-dc fl-fs fl-aifs mt-2rem'>
            <p className='colorfff fs-10w600 text-cap'>должность</p>
            <input className='border-bottom-input'></input>
        </div>
        <div className='flex fl-fe mt-2rem'>
            <button type='submit' className='bgc146aa4 w-150px h-2rem text-up colorfff br-8px fs-10w600'>добавить</button>
        </div>
      </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
   
  }
}
function mapDispatchToProps (dispatch) {
  return {
    showModalActions: bindActionCreators(showModalActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPosition)
