import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as showModalActions from '../../actions/ShowModal'
import * as getDepartment from '../../actions/DepartmentList'
 
import 'react-datepicker/dist/react-datepicker.css';

class AddDepartment extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name:'',
      shortName:''
    };
    this.name = this.name.bind(this);
    this.shortName = this.shortName.bind(this);
    this.addDep = this.addDep.bind(this);
  }
 name(e) {
    this.setState({
      name: e.target.value
    });
  }
  shortName(e) {
    this.setState({
      shortName: e.target.value
    });
  }
  componentDidMount() {
    this.props.getDepartment.getDepartment()
  }
  addDep(){
    this.props.getDepartment.addDepartment(this.state.name,this.state.shortName)
    // this.props.showModalActions.showDepartmentModal(false)
  }
  render () {
    return (
      <div className="w-100 h-100vh flex fl-dc fl-c fl-aic bg_161f29_08 pos-abs top-0">
       <div className='h-3rem w-480px bgc354054 mt-2rem br-t-8px flex fl-aic fl-sb text-up pdl-1rem colorfff fs-14w700'>
          <p>Добавить Отдел</p>
          <img src='images/close1.svg' className='w-50px cursor' onClick={() => this.props.showModalActions.showDepartmentModal(false)}/>
      </div>
      <div className='h-17rem w-480px opacity02 bgc26364f br-b-8px pdx-1rem mb-7rem'>
        <div className='flex fl-dc fl-fs fl-aifs mt-2rem'>
            <p className='colorfff fs-10w600'>Название отдела</p>
            <input className='border-bottom-input' onChange={this.name}></input>
        </div>
        <div className='flex fl-dc fl-fs fl-aifs mt-2rem'>
            <p className='colorfff fs-10w600'>Краткое название отдела</p>
            <input className='border-bottom-input' onChange={this.shortName}></input>
        </div>
        <div className='flex fl-fe mt-2rem'>
            <button type='submit' className='bgc146aa4 w-150px h-2rem text-up colorfff br-8px fs-10w600' onClick={this.state.name.length>3 && this.state.shortName.length>1 ?  this.addDep : null}>Добавить</button>
        </div>
        <div className={this.props.DepartmentList.addDep.success !=false ? "hidden" : "colorf0475f fs-12w200 mt-1rem"}>Такой отдел уже существует</div>
        <div className={this.props.DepartmentList.addDep.success !=true ? "hidden" : "color9DE86F fs-12w200 mt-1rem"}>Отдел успешно сохранен</div>
      </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    showAddDepartment: state.showAddDepartment,
  	DepartmentList: state.DepartmentList

  }
}
function mapDispatchToProps (dispatch) {
  return {
    showModalActions: bindActionCreators(showModalActions, dispatch),
    getDepartment: bindActionCreators(getDepartment, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDepartment)
