import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as showModalActions from '../../actions/ShowModal'
import * as getDepartment from '../../actions/DepartmentList' 
import * as getEmployees from '../../actions/Employees'
class EditEmployee extends Component {
  constructor (props) {
    super(props)
    this.state = {

    };
}
async componentDidMount() {
    await this.props.getDepartment.getDepartment()
    setTimeout(() => {
        this.props.DepartmentList.departments.map((department)=>(
            this.state.departments.push(department.name)
        ));
      }, 1000)
      this.props.getEmployees.getEmployeeInfo(this.props.objectId)
  }
  render () {
    return (
      <div className="w-100 h-100vh flex fl-dc fl-c fl-aic bg_161f29_08 pos-abs top-0 z-index-100">
       <div className='h-3rem w-430px bgc354054 mt-1-5rem br-t-8px flex fl-aic fl-sb text-up pdl-1rem colorfff fs-14w700'>
          <p onClick={this.createDepartment}>Редактирование информации о сотруднике</p>
          <img src='images/close1.svg' className='w-50px cursor' onClick={() => this.props.showModalActions.showEditEmployee(false)} />
      </div>
      <div className='h-31rem w-430px opacity02 bgc26364f br-b-8px pdx-1rem mb-7rem'>
        <div className='flex fl-dc fl-fs fl-aifs mt-1-5rem'>
            <p className='colorfff fs-10w600 text-cap'>имя</p>
            <input className='pt-4px pb-2px  border-bottom-input' type='text'   defaultValue={this.props.Employee.editedEmployee.employee.firstname}  name='firstname' onChange={(e) => this.props.getEmployees.editEmployee(e)} ></input>
        </div>
        <div className='flex fl-dc fl-fs fl-aifs mt-1-5rem'>
            <p className='colorfff fs-10w600 text-cap'>фамилия</p>
            <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.lastname}  name='lastname' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
        </div>
        <div className='flex fl-dc fl-fs fl-aifs mt-1-5rem'>
            <p className='colorfff fs-10w600 text-cap'>отчество</p>
            <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.fathername} name='fathername' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
        </div>
        <div className='flex fl-dc fl-fs fl-aifs mt-1-5rem'>
            <p className='colorfff fs-10w600 text-cap'>Должность</p>
            <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.vacancy} name='vacancy' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
        </div>
        <div className='flex w-100 fl-sb mt-1-5rem'>
            <div className='w-48'>
                <p className='colorfff fs-10w600 text-cap text-al-s'>Зарплата</p>
                <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.salaryFull} name='salaryFull' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
            </div>
            <div className='w-48'>
                <p className='colorfff fs-10w600 text-cap text-al-s'>Фиксированная зарплата</p>
                <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.salary_fixed} name='salary_fixed' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
            </div>
        </div>
        <div className='flex w-100 fl-sb mt-1-5rem'>
            <div className='w-48'>
                <p className='colorfff fs-10w600 text-cap text-al-s'>Время за будний день(мин.)</p>
                <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.fixT} name='fixT' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
            </div>
            <div className='w-48'>
                <p className='colorfff fs-10w600 text-cap text-al-s'>Время за субботний день(мин.)</p>
                <input className='pt-4px pb-2px border-bottom-input' defaultValue={this.props.Employee.editedEmployee.employee.fixST} name='fixST' onChange={(e) => this.props.getEmployees.editEmployee(e)}></input>
            </div>
        </div>
        <div className='flex fl-dc fl-fs fl-aifs mt-1-5rem w-100'>
            <p className='colorfff fs-10w600 text-cap'>Пол</p>
            <select className='intro_input mb_10 bgc26364f colorfff bnone w-100 bbfff'  type='text' name='gender' defaultValue={this.props.Employee.editedEmployee.employee.gender} onChange={(e) => this.props.getEmployees.editEmployee(e)}  required >
                    <option>{this.props.Employee.editedEmployee.employee.gender=='female' ? 'женский' : 'мужской'}</option>
                    <option>{this.props.Employee.editedEmployee.employee.gender!='female' ? 'женский' : 'мужской'}</option>
            </select>
        </div>
        <div className='flex fl-c mt-1-5rem'>
            <button type='submit' className='bgc146aa4 w-150px h-2rem text-up colorfff br-8px fs-10w600' onClick={() => this.props.getEmployees.disableEmployee(
                  this.props.objectId)}>Уволить</button>
            <button type='submit' className='bgc146aa4 w-150px h-2rem text-up colorfff br-8px fs-10w600 ml-16px' onClick={() => this.props.getEmployees.saveEmployee(
                  this.props.Employee.editedEmployee.employee)}>Добавить</button>
        </div>
      </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    // showAddDepartment: state.showAddDepartment,
    DepartmentList: state.DepartmentList,
    Employee: state.Employee
  }
}
function mapDispatchToProps (dispatch) {
  return {
    showModalActions: bindActionCreators(showModalActions, dispatch),
    getDepartment: bindActionCreators(getDepartment, dispatch),
    getEmployees: bindActionCreators(getEmployees, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEmployee)
