import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import TimeCounter from '../Atoms/TimeCounter.js'
import EmployeeInfo from '../Atoms/EmployeeInfo.js'
import SimpleAreaChart from '../Atoms/Chart.js'
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

import Speedometer from '../Atoms/ReactSpeedometer.js';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getEmployees from '../../actions/Employees'


class MainContainer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment(),
      userId: JSON.parse(localStorage.getItem('currentUser')),
      employeeInfoForMonth:false,
      month:''
    }
    this.handleChange = this.handleChange.bind(this);
    this.getEmployee-this.getEmployee.bind(this)
  }
 async handleChange(date) {
  const date1= await new Date(date.year(),date.month()+1,0, 0, 0, 0)
    this.setState({
      startDate: date,
      employeeInfoForMonth:true,
      month: date1.getMonth()
    });
    this.props.getEmployees.getEmployeeInfoForMonth(this.state.userId.userId, date1.toISOString())
  }
  toggleF = (e) =>{
    e.preventDefault();
    document.querySelector('.sidebar').classList.toggle("toggled");
   
  }
componentDidMount() {
  this.props.getEmployees.getEmployee()
  this.props.getEmployees.getEmployeeInfo(this.state.userId.userId)
}
getEmployee(){
  this.props.getEmployees.getEmployee()
}

  render () {
    const hour=this.props.Employee.employeeInfo.hours;
    const salPerMin=this.props.Employee.employeeInfo.salary? this.props.Employee.employeeInfo.salary.salPerMin :null
    return (
      <section className=' mt-1rem ml-16px mr-16px'>
      <div className="sidebar right-0">
            <div className="sidebar_headline">
              <div className='sidebarMonthpicker h-3rem bgc3A455D flex fl-aic pdx-1rem fl-sb'>
              <DatePicker
                    className ='sidebardatepicker'
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    showMonthDropdown
                    locale="ru" />
                    <img src='images/close.svg' className='h-3rem pos-rel left-16px' onClick={this.toggleF}/>
              </div>
            </div>
            {this.props.Employee.employeeInfo.employee && !this.state.employeeInfoForMonth ? this.props.Employee.employeeInfo.reports.map((employee)=>
            employee.month==moment().format('M')-1 ?
            <section>
            <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic'>
            {moment(employee).format("dddd").toUpperCase() + ' '+ employee.day}
            </div>
            <div className='h-9rem bgc3A455D pdx-1rem text-up colorfff fs-14w700 flex fl-dc fl-aic fl-c'>
              <div className='h-4rem bb979797 w-100 flex'>
                <div className='h-4rem w-50 flex fl-aic fl-c br979797'>
                  <img src='images/upward.svg' className='h-1-5rem pdr-1rem'/>
                  <p className='colorfff fs-24w400 '>{moment(employee.check[0][0]).format('HH:mm')}</p>
                </div>
                <div className='h-4rem w-50 flex fl-aic fl-c'>
                  <img src='images/downward.svg' className='h-1-5rem pdr-1rem'/>
                  <p className='colorfff fs-20w700 fs-24w400'>{moment(employee.check[0][1]).isValid() ? moment(employee.check[0][1]).format('HH:mm') :"00:00" }</p>
                </div>
              </div>
              <div className='h-4rem w-100 flex'>
                <div className='h-4rem w-50 flex fl-dc fl-c ml-16px fl-aifs'>
                  <p className='colorfff text-up fs-14w200'>РАБОЧЕЕ ВРЕМЯ:</p>
                  <p className='colorfff text-up fs-14w200 mt-3px'>количество денег:</p>
                </div>
                <div className='h-4rem w-50 flex fl-dc  fl-c fl-aifs ml-16px'>
                  <p className='color92cbff fs-14w200 text-low'>{(employee.mins==null ? '0' : employee.mins)  + " мин"}</p>
                  <p className='color92cbff fs-14w200 mt-3px'>{(employee.salDay==null ? '0' : employee.salDay )+ ' тг'}</p>
                </div>
              
              </div>
            </div>
            </section> : null
       ) : null} 
       {(this.state.employeeInfoForMonth && this.props.Employee.employeeInfoForMonth.employee)  ? this.props.Employee.employeeInfoForMonth.reports.map((employee)=>
        employee.month==this.state.month?
            <section>
            <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic'>
            {moment(employee).format("dddd").toUpperCase() + ' '+ employee.day}
            </div>
            <div className='h-9rem bgc3A455D pdx-1rem text-up colorfff fs-14w700 flex fl-dc fl-aic fl-c'>
              <div className='h-4rem bb979797 w-100 flex'>
                <div className='h-4rem w-50 flex fl-aic fl-c br979797'>
                  <img src='images/upward.svg' className='h-1-5rem pdr-1rem'/>
                  <p className='colorfff fs-24w400 '>{moment(employee.check[0][0]).format('HH:mm')}</p>
                </div>
                <div className='h-4rem w-50 flex fl-aic fl-c'>
                  <img src='images/downward.svg' className='h-1-5rem pdr-1rem'/>
                  <p className='colorfff fs-20w700 fs-24w400'>{moment(employee.check[0][1]).isValid() ? moment(employee.check[0][1]).format('HH:mm') :"00:00" }</p>
                </div>
              </div>
              <div className='h-4rem w-100 flex'>
                <div className='h-4rem w-50 flex fl-dc fl-c ml-16px fl-aifs'>
                  <p className='colorfff text-up fs-14w200'>РАБОЧЕЕ ВРЕМЯ:</p>
                  <p className='colorfff text-up fs-14w200 mt-3px'>количество денег:</p>
                </div>
                <div className='h-4rem w-50 flex fl-dc  fl-c fl-aifs ml-16px'>
                  <p className='color92cbff fs-14w200 text-low'>{(employee.mins==null ? '0' : employee.mins)  + " мин"}</p>
                  <p className='color92cbff fs-14w200 mt-3px'>{(employee.salDay==null ? '0' : employee.salDay )+ ' тг'}</p>
                </div>
              
              </div>
            </div>
            </section>
            : null
       ) : null}  

        </div>
          <div className='h-4rem flex fl-sb'>
            <div>
            <div className='flex'>
              <img src='images/calendar.png' className='h-7px'/>
              <p className='fs-8w400 colorfff ml-3px'>Сегодня</p>
            </div>
            <p className='text-cap colorfff fs-18w400 mt-3px'>{moment().locale('ru').format("dddd, D MMMM")}</p>
            </div>
            <div className='bgc000 h-2rem flex fl-aic fl-c w-14rem br-8px opacity03 cursor' onClick={this.toggleF}>
              <p className='text-up fs-14w700 color8bc3ff'>просмотреть отчет о чекине</p>
            </div>
          </div>
          <TimeCounter 
          employee={this.props.Employee.employees}
          checkedIn={this.props.Employee.employeeInfo.checkedIn}
          averageTime={this.props.Employee.employeeInfo.averagetime}
          remainedTime={this.props.Employee.employeeInfo.remainedtime}
          />
          <div className='w-100 flex fl-dr fl-sb'>
            <EmployeeInfo 
              employee={this.props.Employee.employees}
              fullSalary={this.props.Employee.employeeInfo.employee ? (this.props.Employee.employeeInfo.employee.salaryFull? this.props.Employee.employeeInfo.employee.salaryFull : "0") : '0'} 
              fixedSalary={this.props.Employee.employeeInfo.employee ? this.props.Employee.employeeInfo.employee.salary_fixed : null}
              hoursInMonth={this.props.Employee.employeeInfo.employee ? this.props.Employee.employeeInfo.salary.monthHrs : null}
              depName={this.props.Employee.employeeInfo.department_name}
            />
            <div className='w-49'>
            <div className='w-100'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            количество ежедневных зарплат
            </div>
            <div className='h-105rem bgc26364f br-b-8px flex fl-aic'>
              <div className='w-60  pos-rel top-130px'>
                <Speedometer 
                hours={this.props.Employee.employeeInfo.hours}
                />
              </div>
              <div className='bl979797 h-8rem flex fl-aic w-50'>
                <div className='flex fl-dc fl-fs w-100'>
                  <p className='text-up colorfff fs-33w200'>{hour*salPerMin*60}</p>
                  <p className='flex fl-c text-up fs-10w600 colorfff mt-1rem ml-16px'>обязательное время: <span className='color6dc1ce ml-3px '> 1000 KZT / мин </span></p>
                </div>
                <div>
                  
                </div>
              </div>
            </div>
          </div>
          <div className='w-100'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            Статистика заработной платы <span className='text-low ml-3px'> (в тенге)</span>
            </div>
            <div className='h-105rem bgc26364f br-b-8px flex fl-aie'>
              <SimpleAreaChart 
                reports={this.props.Employee.employeeInfo.reports}
              />
            </div>
          </div>
          </div>
          </div>
          
        </section>
    )
  }
}

function mapStateToProps (state) {
  return {
      Employee: state.Employee
  }
}
function mapDispatchToProps (dispatch) {
  return {
      getEmployees: bindActionCreators(getEmployees, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)

