import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as checkin  from '../../actions/CheckinAction'
import { Redirect } from 'react-router-dom'

class Question1 extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dropdown:false,
      index:0,
      negativeQuestions:''
    }
    this.dropdown=this.dropdown.bind(this)
    this.nextQuestion=this.nextQuestion.bind(this)
    this.negQuestion=this.negQuestion.bind(this)
    this.sendingQuestions=this.sendingQuestions.bind(this)
  }
  dropdown(){
    this.setState({
        dropdown: true
      });
  }
  //show the next questions
  nextQuestion(){
    if(this.state.index<this.props.Checkin.questions.length-1){
      this.setState({
        index: this.state.index+1
      }) 
    }
     if(this.state.index==this.props.Checkin.questions.length-1){
      this.sendingQuestions()
    }
  }
  //collect the string of negative questions and show the next question
  async negQuestion(question){
    if(this.state.index<this.props.Checkin.questions.length-1){
      this.setState({
        index: this.state.index+1,
        negativeQuestions: this.state.negativeQuestions + '\n' + question
      })
    }
   if(this.state.index==this.props.Checkin.questions.length-1){
    await this.setState({
      negativeQuestions: this.state.negativeQuestions + '\n' + question
    })
      this.sendingQuestions()
    }
  }
  sendingQuestions(){
    const noProblem='\nНа рабочем месте никаких проблем нет.'
    if(this.state.negativeQuestions.length!=0){
      this.props.checkin.sentQuestions(this.state.negativeQuestions,this.props.id)
    }
    else this.props.checkin.sentQuestions(noProblem,this.props.id) 
  }
 componentDidMount(){
   this.props.checkin.getQuestions()
 }
//283
  render () {
    const question=this.props.Checkin.questions[this.state.index]
    return (
      
      <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' alt='vatafak' className="h-35px fl-dc" />
        { question ?
        <div className='mt-5rem '>
          <div className='h-3rem w-480px bgc354054 br-t-8px flex fl-aic fl-sb pdl-1rem colorfff fs-14w700'>
            <p>{this.props.Checkin.questions[this.state.index].question}</p>
        </div>
        <div className='h-10rem w-480px opacity02 bgc26364f br-b-8px pdx-1rem mb-3rem'>
          <div className='flex fl-dc fl-fs fl-aifs' onClick={this.dropdown}>
              <p className='border-bottom-input mt-4rem'></p>
              <img src='images/dropdown.svg' className='pos-rel h-6rem top--59 flex fl-fe left-380px' />
              <div className={this.state.dropdown? 'w-100 h-3rem bgc354054 flex fl-dc fl-aifs pos-rel top--93' : 'hidden'}>
                  <p className='bb303C53 h-1-5rem colorfff fs-14w200 pdx-1rem flex fl-aic w-100' onClick={this.nextQuestion}>{this.props.Checkin.questions[this.state.index].posAnswer}</p>
                  <p className='bb303C53 h-1-5rem colorfff fs-14w200 pdx-1rem flex fl-aic w-100' onClick={()=>this.negQuestion(this.props.Checkin.questions[this.state.index].negAnswer)}> {this.props.Checkin.questions[this.state.index].negAnswer}</p>
              </div>
          </div>
        </div>
       </div>
      : null       }
        

        {/* <div className="flex fl-dr">
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/active-dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
            <img src='images/dot.png' alt='vatafak' className="h-10px pr-5px" />
        </div> */}
      </section>
       
    )
  }
}


function mapStateToProps (state) {
  return {
    Checkin: state.Checkin
  }
}
function mapDispatchToProps (dispatch) {
  return {
    checkin: bindActionCreators(checkin, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Question1)