import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import SimpleAreaChartForDep from '../Atoms/ChartForDep.js'
import RadialChart from '../Atoms/RadialChart.js'
import CircularProgressbar from 'react-circular-progressbar';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as showModalActions from '../../actions/ShowModal'
import * as getDepartment from '../../actions/DepartmentList'

import 'react-datepicker/dist/react-datepicker.css';


class DepAndPos extends Component {
  constructor (props) {
    super(props)
    this.state = {
      donutval:55,
      showallIndoor:false,
      showallOutdoor:false
    }
    this.showAllIndoor=this.showAllIndoor.bind(this);
    this.showAllOutdoor=this.showAllOutdoor.bind(this);
  }
  updateVal(e){
    this.setState({donutval:e.target.value})
  }
  showAllIndoor(){
    this.state.showallIndoor ?
      this.setState({
        showallIndoor:false
      })
    :
      this.setState({
        showallIndoor:true
      })
  }
  showAllOutdoor(){
    this.state.showallOutdoor ?
      this.setState({
        showallOutdoor:false
      })
    :
      this.setState({
        showallOutdoor:true
      })
  }
  componentDidMount() {
    this.props.getDepartment.getDepartment()
  }

  render () {
    return (
      <section className='w-100  ml-16px flex '>
        <div className='w-79 mt-1rem'>
         <div className='flex '>
         <div className='h-5rem color2bbcd4 text-up fs-14w400'><span className='fs-75w300'>{this.props.employee.length}</span><br/>Сотрудников</div>
          <div className='w-100 flex fl-fe'>
            <div className='h-6rem h-100 w-300px mt-8px br-8px bgcimgpink flex mr-2rem'>
              <div className='w-230px flex fl-aic fl-c cursor' onClick={() => this.props.showModalActions.showDepartmentList(true)}>
                <img src='images/moleculas.svg' className='h-3rem'/>
                <p className='text-up colorfff fs-10w200 ml-16px flex fl-dc fl-aifs'>
                  <span className='fs-20w400 lh1-3 '>Отдел</span><br/>
                 {this.props.DepartmentList.departments.length +' Зарегистрированных'}
                  </p>
              </div>
              <div className='w-70px h-100 bgcD66380 brl-8px opacity0i5 flex fl-aic fl-c' onClick={() => this.props.showModalActions.showDepartmentModal(true)}>
                <img src='images/pen.svg' className='h-2rem cursor'/>
              </div>
            </div>
            {/* <div className='h-6rem h-100 w-300px mt-8px br-8px bgcimgblue flex mr-2rem'>
              <div className='w-230px flex fl-aic fl-c cursor' >
                <img src='images/people.svg' className='h-3rem'/>
                <p className='text-up colorfff fs-10w200 ml-16px flex fl-dc fl-aifs'>
                  <span className='fs-20w400 lh1-3 '>Должности</span><br/>
                 12 Зарегистрированных
                  </p>
              </div>
              <div className='w-70px h-100 bgc7AC4DD brl-8px opacity0i5 flex fl-aic fl-c cursor' onClick={() => this.props.showModalActions.showAddPosition(true)}>
                <img src='images/pen.svg' className='h-2rem cursor'/>
              </div>
            </div> */}
          </div>
         </div>
         <div>
         <div className='w-100 flex'>
            <div className='w-50 mr-2rem'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            Средняя время работы
            </div>
            <div className='h-105rem bgc26364f br-b-8px'>
            <CircularProgressbar
                initialAnimation={true}
                percentage={(this.props.hour*100)/24}
                styles={{
                path: { stroke: `#C04968` },
                }}
              />
              <p className='pos-rel top--60px fs-18w400 colorfff mr-1-8rem'>{this.props.hour + 'ч.' + this.props.minutes +'мин'}</p>
            </div>

          </div>
          <div className='w-50 mr-2rem'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            Карма
            </div>
            <div className='h-105rem bgc26364f br-b-8px'>
              <RadialChart
              />
            </div>
          </div>
          </div>
          <div className='w-100 mt-2rem mr-2rem'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700 mr-2rem'>
            Статистика заработной платы  <span className='text-low ml-3px'> (в тенге)</span>
            </div>
            <div className='h-135rem bgc26364f br-b-8px flex fl-aie mr-2rem'>
              <SimpleAreaChartForDep
                salary={this.props.adminEmployee}
              />
            </div>
          </div>
         </div>

        </div>
        <div className=' bgc3A455D'> 
        <div className="sidebarFixed right-0">
            <div className="sidebar_headline">
              <div className='h-3rem bgc3A455D flex fl-aic pdx-1rem fl-sb'>
              <div>
            <div className='flex'>
              <img src='images/calendar.png' className='h-7px'/>
              <p className='fs-8w400 colorfff ml-3px'>Сегодня</p>
            </div>
            <p className='text-cap colorfff fs-18w400 mt-3px'>{moment().locale('ru').format("dddd, D MMMM")}</p>
            </div>
              </div>
            </div>
            <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic fl-sb'>
             <p> Сотрудники в офисе</p>
             <p className='fs-8w400 color3aa2ef cursor' onClick={this.showAllIndoor}> {this.state.showallIndoor ? 'Скрыть'  : 'Показать все'}</p>
            </div>
            <div className={(this.state.showallIndoor ? ' showall ' : ' overflowyhidden ') +'h-13rem bgc3A455D pdx-1rem text-up colorfff fs-14w700'}>
            { this.props.employee.map((employee,index)=>(
              employee.checked ?
              <div className="flex pt-1-5rem">
                <img src={'/userimages/'+employee.employee_id.toLowerCase()+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='h-3rem w-3rem br-50'/>
                <div className='ml-16px mt-8px'>
                  <p className='text-cap'>{employee.firstname +" "+  employee.lastname}</p>
                  <div className='flex fl-aic mt-8px'>
                    <img src='images/upward.svg' className='h-10px'/>
                    <p className='fs-10w600 color0df751'> {moment(employee.checkedIn).format('hh:mm')}</p>
                  </div>
                </div>
              </div>
              :
              null
            )  )}
            </div>
            <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic fl-sb'>
              <p> Сотрудники вне офисa</p>
              <p className='fs-8w400 color3aa2ef cursor' onClick={this.showAllOutdoor}> {this.state.showallOutdoor ? 'Скрыть'  : 'Показать все'}</p>
            </div>
            <div className={(this.state.showallOutdoor ? ' showall ' : ' overflowyhidden ') +'h-13rem bgc3A455D pdx-1rem text-up colorfff fs-14w700'}>
            { this.props.employee.map((employee,index)=>(
              !employee.checked ?
              <div className="flex pt-1-5rem">
                <img src={'/userimages/'+employee.employee_id.toLowerCase()+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='h-3rem w-3rem br-50'/>
                <div className='ml-16px mt-8px'>
                  <p className='text-cap'>{employee.firstname +" "+  employee.lastname}</p>
                  <div className='flex fl-aic mt-8px'>
                    <img src='images/downward.svg' className='h-10px'/>
                    <p className='fs-10w600 colorf0475f'>{moment(employee.checkedIn).format('hh:mm')}</p>
                  </div>
                </div>
              </div>
              :
              null
            )  )}
            </div>

        </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps (state) {
  return {
    modalShow: state.modalShow,
    DepartmentList: state.DepartmentList
  }
}
function mapDispatchToProps (dispatch) {
  return {
    getDepartment: bindActionCreators(getDepartment, dispatch),
    showModalActions: bindActionCreators(showModalActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DepAndPos)
