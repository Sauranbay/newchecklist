import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Tasks extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
  render () {
    return (
        <div className='w-27rem ml-16px ml-2rem'>
            <div className='h-3rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            Задачи
            </div>
            <div className='h-40rem bgc26364f br-b-8px'>
              <div  className=' flex fl-fs'>
                <div className='bgc2E425E h-auto w-100 colorfff  flex fl-dc fl-fs text-al-s pdl-1rem fs-14w200 pb-1rem pt-1rem '>
              <div className='flex'>
              <p> НАЗВАНИЕ ПРОЕКТА:</p>
              <p className='pdl-1rem'> Checklist</p>
              </div>
               <div className='flex'>
                  <p className='mt-8px'>
                    ОПИСАНИЕ: </p> 
                    <p className='pdl-1rem mt-8px'> A checklist is a type of job aid used to reduce failure by compensating for potential limits of human memory and attention. It helps to ensure consistency and completeness in carrying out a task
                  </p>
                </div>
                </div>
                </div>
              <p className=' flex fl-fs fs-12w200 colorfff pdl-1rem mt-3px'>Назначено: <span className='text-cap ml-3px'>Рустам Жуаспаев</span></p>
              <p className=' flex fl-fs fs-12w200 colorf0475f pdl-1rem mt-3px'>Дедлайн: <span className='text-cap ml-3px'>08.08.2018</span></p>
              <p className=' flex fl-fs fs-12w200 colorfff pdl-1rem mt-3px'>Дата назначения: <span className='text-cap ml-3px'>01.08.2018</span></p>
            </div>
          </div>

        )
    }
}



export default Tasks
