import React, { Component }  from 'react'
import { RadialBarChart, RadialBar, Legend } from 'recharts';


const data = [
      {name: 'John ', uv: 31.47, pv: 2400, fill: '#ccf6de'},
      {name: 'Emily', uv: 26.69, pv: 4567, fill: '#6ff8a9'},
      {name: 'Sarah', uv: 15.69, pv: 1398, fill: '#2cbe6a'},

    ];
    
  const style = {
  	top: 0,
  	left: 350,
  	lineHeight: '24px'
  };

  class RadialChart extends Component {
	render () {
  	return (
    	<RadialBarChart width={500} height={300} cx={150} cy={150} innerRadius={40} outerRadius={160} barSize={20} data={data}>
        <RadialBar minAngle={15}  label={{ position: 'insideStart', fill: '#26364f' }} clockWise={true}  dataKey='uv'/>
        <Legend iconSize={10} width={120} height={140} layout='vertical' verticalAlign='middle' wrapperStyle={style} />
        </RadialBarChart>
    );
  }
}

export default RadialChart
