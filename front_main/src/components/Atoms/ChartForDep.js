import React, { Component }  from 'react'
import { AreaChart, CartesianGrid, XAxis, YAxis, Tooltip, Area } from 'recharts';
import { isMoment } from '../../../../node_modules/moment';
import moment from 'moment'


class SimpleAreaChartForDep extends Component {
    render () {
      const data = []
      this.props.salary.employee.salaryPerDay ? this.props.salary.employee.salaryPerDay.map((salary) =>
        data.push({name: salary.day + '.'+ moment().format('MM'), зп: salary.sal, pv: 1398, amt: 2210})
    ) : null
  	return (
    	<AreaChart width={1000} height={150} data={data}
            margin={{top: 10, right: 30, left: 0, bottom: 0}} className='w-100'>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="name"/>
        <YAxis/>
        <Tooltip/>
        <Area type='monotone' dataKey='зп' fill='#3A5179' />
      </AreaChart>
    )
}
}

export default SimpleAreaChartForDep