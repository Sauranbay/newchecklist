import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as showModalActions from '../../actions/ShowModal'

class AdminEmployeeInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }
  render () {
    return (
        <div className='w-27rem ml-16px'>
            <div className='h-3rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700 flex fl-sb'>
            <p>Информация о работнике</p>
            <img src="images/pen.svg" className='h-1rem mr-16px' onClick={() => this.props.showModalActions.showEditEmployee(true)}/>
            </div>
            { this.props.employee.map((employee,index)=>(
              employee.employee_id.toLowerCase()==this.props.selectedEmployee.toLowerCase() ? 
            <div className='h-18rem bgc26364f br-b-8px'>
              <div className='m-x-2rem pt-2rem pb-3rem flex fl-dr fl-sb bb303C53'>
                <div>
                  <p className='fs-14w200 colorfff'>{employee.firstname}</p>
                  <img src={'/userimages/'+employee.employee_id.toLowerCase()+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='h-6rem w-6rem br-50 mt-1rem'/> 
                </div>
                <div className='flex fl-dc fl-aifs '>
                  <p className='fs-10w200 colorfff'>{employee.employee_id}</p>
                  <p className='fs-10w200 colorfff mt-2rem text-cap'>Должность:</p>
                  <p className='fs-10w200 colorfff mt-1rem text-cap'>Отдел:</p>
                  {/* <p className='fs-10w200 colorfff mt-1rem text-cap'>Номер телефона:</p>
                  <p className='fs-10w200 colorfff mt-1rem text-cap'>Email:</p>
                  <p className='fs-10w200 colorfff mt-1rem text-cap'>Адрес:</p> */}
                </div>
                <div className='flex fl-dc fl-aifs '>
                  <p className='fs-10w200 colorfff mt-2-7rem text-cap'>{employee.vacancy}</p>
                  <p className='fs-10w200 colorfff mt-1rem text-cap'>{this.props.departmentName}</p>
                  {/* <p className='fs-10w200 colorfff mt-1rem'>+7 (747)  867 89  90</p>
                  <p className='fs-10w200 colorfff mt-1rem'>khalilu@gmail.com</p>
                  <p className='flex fl-fs text-al-s fs-10w200 colorfff mt-1rem'>Алматы, Казахстан <br/>Ул. Алфараби 23/1</p> */}
                </div>
              </div>
              <div className="flex fl-dr fl-sb m-x-2rem h-4-4rem fl-aic">
                <div className='flex fl-dc fl-aic'>
                  <img src="images/money.svg" className='w-32px' />
                  <p className='colorfff fs-10w200'>{this.props.fullSalary + ' kzt'}</p>
                </div>
                <div className='flex fl-dc fl-aic'>
                  <img src="images/cent.svg" className='w-32px' />
                  <p className='colorfff fs-10w200'>{this.props.fixedSalary + ' kzt'}</p>
                </div>
                <div className='flex fl-dc fl-aic'>
                  <img src="images/workmoney.svg" className='w-32px' />
                  <p className='colorfff fs-10w200'>{this.props.hoursInMonth + ' час.'}</p>
                </div>
              </div>
            </div>
            : null
          ) 
        
            )
            }
          </div>

        )
    }
}
// function mapStateToProps (state) {
//   return {
//     webcam: state.webcam
//   }
// }


// export default connect(mapStateToProps)(AdminEmployeeInfo)
function mapStateToProps (state) {
  return {
    modalShow: state.modalShow
  }
}
function mapDispatchToProps (dispatch) {
  return {
    showModalActions: bindActionCreators(showModalActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminEmployeeInfo)