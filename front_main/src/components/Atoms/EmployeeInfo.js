import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class EmployeeInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userId: JSON.parse(localStorage.getItem('userId'))
    }
  }
  componentDidMount(){
    this.props.getUsers
  }
  
  render () {
    return (
        <div className='w-49'>
            <div className='h-4rem bgc354054 mt-2rem br-t-8px flex fl-aic text-up pdl-1rem colorfff fs-14w700'>
            Информация о работнике
            </div>
            { this.props.employee.map((employee,index)=>(
              employee.employee_id.toLowerCase()==this.state.userId.toLowerCase()) ?
           <div className='h-27rem bgc26364f br-b-8px' key={index}>
              <div className='m-x-2rem pt-2rem pb-3rem flex fl-dr fl-sb bb303C53'>
                <div>
                  <p className='fs-20w700 colorfff'>{employee.firstname + ' ' + employee.lastname}</p>
                  <img src={'/userimages/'+employee.employee_id.toLowerCase()+'.jpg'} onError={(e)=>{e.target.src="images/userphoto.png"}} className='mt-3rem h-9rem br-50 w-9rem'/>
                </div>
                <div className='flex fl-dc fl-aifs '>
                  <p className='fs-18w400 colorfff'>{employee.employee_id}</p>
                  <p className='fs-16w600 colorfff mt-2rem fs-14w600'>Должность:</p>
                  <p className='fs-16w600 colorfff mt-1rem'>Отдел:</p>
                </div>
                <div className='flex fl-dc fl-aifs '>
                  <p className='fs-14w400 colorfff mt-32rem fs-14w600'>{employee.vacancy}</p>
                  <p className='fs-14w400 colorfff mt-12rem text-cap'>{this.props.depName}</p>
                </div>
              </div>
              <div className="flex fl-dr fl-sb m-x-2rem h-8rem fl-aic">
                <div className='flex fl-dc fl-aic'>
                  <img src="images/money.svg" className='h-3rem' />
                  <p className='colorfff fs-18w400 text-up'>{this.props.fullSalary + ' kzt'}</p>
                </div>
                <div className='flex fl-dc fl-aic'>
                  <img src="images/cent.svg" className='h-3rem' />
                  <p className='colorfff fs-18w400 text-up'>{this.props.fixedSalary + ' kzt'}</p>
                </div>
                <div className='flex fl-dc fl-aic'>
                  <img src="images/workmoney.svg" className='h-3rem' />
                  <p className='colorfff fs-18w400 text-up'>{this.props.hoursInMonth + ' час.'}</p>
                </div>
              </div>
            </div>
          : null
        ) }
          </div>
        
        )
    }
}
function mapStateToProps (state) {
  return {
    webcam: state.webcam
  }
}


export default connect(mapStateToProps)(EmployeeInfo)
