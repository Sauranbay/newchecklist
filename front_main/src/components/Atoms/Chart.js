import React, { Component }  from 'react'
import { AreaChart, CartesianGrid, XAxis, YAxis, Tooltip, Area } from 'recharts';

// const data =this.props.reports ? this.props.reports.map((report)=>[
//     {name: report.day + '.'+report.month, зп: report.salDay, pv: 1398, amt: 2210}
//   ]
// ) : null
// const data = [
//   {name: 'hello', зп: 3000, pv: 1398, amt: 2210}
// ]
// const rep=this.props.reports
class SimpleAreaChart extends Component {
    render () {
      // const data =this.props.reports ? this.props.reports.forEach(report => 
      //   [
      //     {name: report.day + '.'+report.month, зп: report.salDay, pv: 1398, amt: 2210}
      //   ]
      // ) : null
      const data = []
      this.props.reports ? this.props.reports.map((report) =>
        data.push({name: report.day + '.'+report.month, зп: report.salDay, pv: 1398, amt: 2210})
    ) : null

  	return (
    	<AreaChart width={700} height={150} data={data}
            margin={{top: 10, right: 30, left: 0, bottom: 0}} className='w-100'>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="name"/>
        <YAxis/>
        <Tooltip/>
        <Area type='monotone' dataKey={'зп'} fill='#4596A0' fill='#4596A0' />
      </AreaChart>
    )
}
}

export default SimpleAreaChart