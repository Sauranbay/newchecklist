import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getEmployees from '../../actions/Employees'


class Sidebar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment(),
      employeeInfoForMonth:false,
      month:''
    }
    this.handleChange = this.handleChange.bind(this);
  }
  // handleChange(date) {
  //   this.setState({
  //     startDate: date,
  //     employeeInfoForMonth:true
  //   });
  // }
  async handleChange(date) {
    const date1= await new Date(date.year(),date.month()+1,0, 0, 0, 0)
      this.setState({
        startDate: date,
        employeeInfoForMonth:true,
        month: date1.getMonth()
      });
      this.props.getEmployees.getEmployeeInfoForMonth(this.props.employeeId, date1.toISOString())
    }
  render () {
    return (
      // <section className=' mt-1rem ml-16px mr-16px'>
      // <div className="sidebarFixed right-0">
      //       <div className="sidebar_headline">
      //         <div className='sidebarMonthpicker h-3rem bgc3A455D flex fl-aic pdx-1rem fl-sb'>
      //         <DatePicker
      //               className ='sidebardatepicker'
      //               selected={this.state.startDate}
      //               onChange={this.handleChange}
      //               showMonthDropdown
      //               locale="ru" />
      //         </div>
      //       </div>
      //       {this.props.employee && !this.state.employeeInfoForMonth ? this.props.employee.days.map((employee)=>
      //       <section>
      //       <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic'>
      //       {moment(employee).format("dddd").toUpperCase() + ' '+ employee.day}
      //       </div>
      //       <div className='h-9rem bgc3A455D pdx-1rem text-up colorfff fs-14w700 flex fl-dc fl-aic fl-c'>
      //         <div className='h-4rem bb979797 w-100 flex'>
      //           <div className='h-4rem w-50 flex fl-aic fl-c br979797'>
      //             <img src='images/upward.svg' className='h-1-5rem pdr-1rem'/>
      //             <p className='colorfff fs-24w400 '>{moment(employee.check[0][0]).format('HH:mm')}</p>
      //           </div>
      //           <div className='h-4rem w-50 flex fl-aic fl-c'>
      //             <img src='images/downward.svg' className='h-1-5rem pdr-1rem'/>
      //             <p className='colorfff fs-20w700 fs-24w400'>{moment(employee.check[0][1]).isValid() ? moment(employee.check[0][1]).format('HH:mm') :"00:00" }</p>
      //           </div>
      //         </div>
      //         <div className='h-4rem w-100 flex'>
      //           <div className='h-4rem w-50 flex fl-dc fl-c ml-16px fl-aifs'>
      //             <p className='colorfff text-up fs-10w200'>РАБОЧЕЕ ВРЕМЯ:</p>
      //             <p className='colorfff text-up fs-10w200 mt-3px'>количество денег:</p>
      //           </div>
      //           <div className='h-4rem w-50 flex fl-dc  fl-c fl-aifs ml-16px'>
      //             <p className='color92cbff fs-10w200 text-low'>{(employee.mins==null ? '0' : employee.mins)  + " мин"}</p>
      //             <p className='color92cbff fs-10w200 mt-3px'>{(employee.salDay==null ? '0' : employee.salDay )+ ' тг'}</p>
      //           </div>
              
      //         </div>
      //       </div>
      //       </section>
      //  ) : null} 

      //   </div>
      //     </section>
          <div className="sidebarFixed right-0">
          <div className="sidebar_headline">
            <div className='sidebarMonthpicker h-3rem bgc3A455D flex fl-aic pdx-1rem fl-sb'>
            <DatePicker
                  className ='sidebardatepicker'
                  selected={this.state.startDate}
                  onChange={this.handleChange}
                  showMonthDropdown
                  locale="ru" />
            </div>
          </div>
          {this.props.employee.reports && !this.state.employeeInfoForMonth ? this.props.employee.reports.map((employee)=>
          employee.month==moment().format('M')-1 ?
          <section>
          <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic'>
          {moment(employee).format("dddd").toUpperCase() + ' '+ employee.day}
          </div>
          <div className='h-9rem bgc3A455D pdx-1rem text-up colorfff fs-14w700 flex fl-dc fl-aic fl-c'>
            <div className='h-4rem bb979797 w-100 flex'>
              <div className='h-4rem w-50 flex fl-aic fl-c br979797'>
                <img src='images/upward.svg' className='h-1-5rem pdr-1rem'/>
                <p className='colorfff fs-24w400 '>{moment(employee.check[0][0]).format('HH:mm')}</p>
              </div>
              <div className='h-4rem w-50 flex fl-aic fl-c'>
                <img src='images/downward.svg' className='h-1-5rem pdr-1rem'/>
                <p className='colorfff fs-20w700 fs-24w400'>{moment(employee.check[0][1]).isValid() ? moment(employee.check[0][1]).format('HH:mm') :"00:00" }</p>
              </div>
            </div>
            <div className='h-4rem w-100 flex'>
              <div className='h-4rem w-50 flex fl-dc fl-c ml-16px fl-aifs'>
                <p className='colorfff text-up fs-14w200'>РАБОЧЕЕ ВРЕМЯ:</p>
                <p className='colorfff text-up fs-14w200 mt-3px'>количество денег:</p>
              </div>
              <div className='h-4rem w-50 flex fl-dc  fl-c fl-aifs ml-16px'>
                <p className='color92cbff fs-14w200 text-low'>{(employee.mins==null ? '0' : employee.mins)  + " мин"}</p>
                <p className='color92cbff fs-14w200 mt-3px'>{(employee.salDay==null ? '0' : employee.salDay )+ ' тг'}</p>
              </div>
            
            </div>
          </div>
          </section> : null
     ) : null} 
     {(this.state.employeeInfoForMonth && this.props.Employee.employeeInfoForMonth.employee)  ? this.props.Employee.employeeInfoForMonth.reports.map((employee)=>
      employee.month==this.state.month?
          <section>
          <div className='h-2rem pdx-1rem text-up colorfff fs-14w700 flex fl-aic'>
          {moment(employee).format("dddd").toUpperCase() + ' '+ employee.day}
          </div>
          <div className='h-9rem bgc3A455D pdx-1rem text-up colorfff fs-14w700 flex fl-dc fl-aic fl-c'>
            <div className='h-4rem bb979797 w-100 flex'>
              <div className='h-4rem w-50 flex fl-aic fl-c br979797'>
                <img src='images/upward.svg' className='h-1-5rem pdr-1rem'/>
                <p className='colorfff fs-24w400 '>{moment(employee.check[0][0]).format('HH:mm')}</p>
              </div>
              <div className='h-4rem w-50 flex fl-aic fl-c'>
                <img src='images/downward.svg' className='h-1-5rem pdr-1rem'/>
                <p className='colorfff fs-20w700 fs-24w400'>{moment(employee.check[0][1]).isValid() ? moment(employee.check[0][1]).format('HH:mm') :"00:00" }</p>
              </div>
            </div>
            <div className='h-4rem w-100 flex'>
              <div className='h-4rem w-50 flex fl-dc fl-c ml-16px fl-aifs'>
                <p className='colorfff text-up fs-14w200'>РАБОЧЕЕ ВРЕМЯ:</p>
                <p className='colorfff text-up fs-14w200 mt-3px'>количество денег:</p>
              </div>
              <div className='h-4rem w-50 flex fl-dc  fl-c fl-aifs ml-16px'>
                <p className='color92cbff fs-14w200 text-low'>{(employee.mins==null ? '0' : employee.mins)  + " мин"}</p>
                <p className='color92cbff fs-14w200 mt-3px'>{(employee.salDay==null ? '0' : employee.salDay )+ ' тг'}</p>
              </div>
            
            </div>
          </div>
          </section>
          : null
     ) : null}  

      </div>
    )
  }
}
function mapStateToProps (state) {
  return {
      Employee: state.Employee
  }
}
function mapDispatchToProps (dispatch) {
  return {
      getEmployees: bindActionCreators(getEmployees, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)

