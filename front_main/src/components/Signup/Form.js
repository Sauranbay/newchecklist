import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as checkin from '../../actions/CheckinAction'


class Form extends Component {
  render () {
    return (
      <section className="h-90vh flex  fl-dc fl-c fl-aic">
        <img src='images/Automato.svg' className='ml-16px h-2rem' />
        <div className="w-30rem h-12rem  bgc202d45 mt-2rem br-8px flex fl-dc fl-aic fl-c">
          <div className='flex fl-sb w-27rem'>
            <p className='fs-16w200 colorfff mb-1rem top-2rem pos-rel'>Введите свой ID</p>
            <Link to={this.props.currentUser.next=='in' ? "/approve1" :"/approvecheckout"} >
             <img src='images/next.svg' alt='vatafak' className="h-3rem w-4rem pos-rel top-2rem left-16px"   onClick={this.props.onClick}/>
            </Link>
          </div>
          <input type='text' className='signInput w-27rem h-2rem'  onChange={this.props.checkId}/>
          <div className={this.props.success !=false ? "hidden" : "colorf0475f fs-12w200 mt-1rem"}>Работник с таким ID не найден!</div>
        </div>
      </section>
    )
  }
}
function mapStateToProps (state) {
  return {
    Checkin: state.Checkin
  }
}
function mapDispatchToProps (dispatch) {
  return {
    checkin: bindActionCreators(checkin, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
